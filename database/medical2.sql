-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2019 at 10:25 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `medical`
--

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id_artikel` int(50) NOT NULL,
  `penulis` varchar(50) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `isi_artikel` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `id_komentar` int(50) NOT NULL,
  `nama_komentar` varchar(50) NOT NULL,
  `email_komentar` varchar(50) NOT NULL,
  `isi_komentar` varchar(200) NOT NULL,
  `id_artikel` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `laporan_transaksi`
--

CREATE TABLE `laporan_transaksi` (
  `id_laporan` int(50) NOT NULL,
  `id_transaksi` int(50) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `medis`
--

CREATE TABLE `medis` (
  `id_medis` int(50) NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `usia` int(3) NOT NULL,
  `alamat_lengkap` text NOT NULL,
  `jenis_kelamin` enum('Laki-laki','Perempuan') NOT NULL,
  `no_hp` varchar(12) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `bagian_tugas` enum('UGD','puskesmas','klinik','poliklinik') NOT NULL,
  `sip` varchar(100) NOT NULL,
  `str` varchar(100) NOT NULL,
  `stb` varchar(100) NOT NULL,
  `ijazah` varchar(100) NOT NULL,
  `ktp` varchar(100) NOT NULL,
  `status` enum('','online','offline') NOT NULL,
  `status_medis` enum('dokter','perawat','bidan') NOT NULL,
  `status_register` enum('terverifikasi','belum terverifikasi') NOT NULL,
  `is_login` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medis`
--

INSERT INTO `medis` (`id_medis`, `nama_lengkap`, `username`, `password`, `email`, `usia`, `alamat_lengkap`, `jenis_kelamin`, `no_hp`, `foto`, `bagian_tugas`, `sip`, `str`, `stb`, `ijazah`, `ktp`, `status`, `status_medis`, `status_register`, `is_login`) VALUES
(7, 'Chika', 'chikajessica', '1d7139f34ccd35a2e5790991eb09dd96', 'chikajessica97@gmail.com', 25, 'Perumnas Subang', 'Perempuan', '089812823475', 'gritte.png', 'UGD', 'LAPORAN PRAKTIK KERJA LAPANGAN.docx', 'LAPORAN PRAKTIK KERJA LAPANGAN.docx', 'LAPORAN PRAKTIK KERJA LAPANGAN.docx', 'LAPORAN PRAKTIK KERJA LAPANGAN.docx', '', '', 'dokter', 'terverifikasi', '1'),
(8, 'Vivi Ratu', 'vivi', 'c3bb6f719742fd1e5768d6d1361cfb49', 'viviratu@gmail.com', 0, '', 'Perempuan', '085612938475', '', 'UGD', '10103026_Ricka Zakia Drajat_Laporan UID 2.docx', 'Form PKLRicka Baru.docx', 'Form PKLRicka Baru.docx', 'Form PKLRicka Baru.docx', '', '', 'perawat', 'belum terverifikasi', '0'),
(9, 'Chika', 'rickazakia', '674040b55a3e0e52afa976b5f005d448', 'chikajessica97@gmail.com', 0, '', 'Perempuan', '089812823475', '', 'UGD', 'LAPORAN PRAKTIK KERJA LAPANGAN.docx', 'LAPORAN PRAKTIK KERJA LAPANGAN.docx', 'LAPORAN PRAKTIK KERJA LAPANGAN.docx', 'LAPORAN PRAKTIK KERJA LAPANGAN.docx', '', '', 'dokter', 'terverifikasi', '0');

-- --------------------------------------------------------

--
-- Table structure for table `obat`
--

CREATE TABLE `obat` (
  `id_obat` int(50) NOT NULL,
  `nama_obat` varchar(50) NOT NULL,
  `harga_satuan_obat` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `obat`
--

INSERT INTO `obat` (`id_obat`, `nama_obat`, `harga_satuan_obat`) VALUES
(1, 'paramex', 2000),
(2, 'bodrex', 2000);

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id_user` int(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `jenis_kelamin` enum('Laki-laki','Perempuan') NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(12) NOT NULL,
  `usia` int(3) NOT NULL,
  `status` enum('user','admin') NOT NULL,
  `foto` varchar(50) NOT NULL,
  `is_login` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id_user`, `username`, `password`, `nama_lengkap`, `jenis_kelamin`, `email`, `alamat`, `no_hp`, `usia`, `status`, `foto`, `is_login`) VALUES
(5, 'rickazakia', '674040b55a3e0e52afa976b5f005d448', 'Ricka Zakia Drajat', 'Perempuan', 'rickazakia97@gmail.com', 'Jl. Sawo 3 no 11 Perumnas Subang', '089872653419', 21, 'user', 'zulfa.png', '1'),
(6, 'admin', '21232f297a57a5a743894a0e4a801fc3', '0', '', '', '', '', 0, 'admin', '', '0'),
(7, 'ada', '8c8d357b5e872bbacd45197626bd5759', '0', 'Laki-laki', 'ada@gmail.com', '', '085612938475', 0, 'user', '', '0'),
(8, 'boby', 'c83e4046a7c5d3c4bf4c292e1e6ec681', 'boby', 'Laki-laki', 'boby@gmail.com', '', '085791882731', 0, 'user', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id_review` int(50) NOT NULL,
  `id_medis` int(50) NOT NULL,
  `id_user` int(50) NOT NULL,
  `jumlah_review` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tambah_layanan`
--

CREATE TABLE `tambah_layanan` (
  `id_tambah` int(50) NOT NULL,
  `nama_tambahan` varchar(250) NOT NULL,
  `harga` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tambah_layanan`
--

INSERT INTO `tambah_layanan` (`id_tambah`, `nama_tambahan`, `harga`) VALUES
(2, 'cek darah', 150000),
(3, 'cek gula', 150000);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(50) NOT NULL,
  `id_user` int(50) NOT NULL,
  `id_medis` int(50) NOT NULL,
  `des_gejala` text NOT NULL,
  `id_tambah` int(50) DEFAULT NULL,
  `id_obat` int(50) DEFAULT NULL,
  `jumlah_obat` int(30) NOT NULL,
  `total_obat` int(30) NOT NULL,
  `alamat` text NOT NULL,
  `alamat_detail` text NOT NULL,
  `jenis_medis` enum('dokter','perawat','bidan') NOT NULL,
  `status` enum('sedang mencari medis','diproses','selesai') NOT NULL,
  `harga_bidan` int(30) NOT NULL,
  `harga_dokter` int(30) NOT NULL,
  `harga_perawat` int(30) NOT NULL,
  `total_bayar` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_user`, `id_medis`, `des_gejala`, `id_tambah`, `id_obat`, `jumlah_obat`, `total_obat`, `alamat`, `alamat_detail`, `jenis_medis`, `status`, `harga_bidan`, `harga_dokter`, `harga_perawat`, `total_bayar`) VALUES
(6, 8, 8, 'dnbfhdfjd', 3, 2, 1, 6000, 'befgeygfhefje', 'msncjhsuyheb', 'perawat', 'selesai', 0, 0, 200000, 209000),
(7, 8, 7, 'bdvhdfhie', 2, 1, 2, 6000, 'njdhfe', 'nvejfiej', 'dokter', 'selesai', 0, 200000, 0, 206000),
(11, 5, 7, 'panas, dingin, pusing, flu', NULL, 2, 2, 6000, 'perumnas subang', 'no 11 jl. sawo 3 blok 1 perumnas subang', 'dokter', 'selesai', 0, 200000, 0, 206000),
(19, 5, 7, 'sfbhdfg h', 2, 1, 3, 9000, 'shbdgfhdg', 'ndchfbgfhegfey', 'dokter', 'selesai', 0, 200000, 0, 209000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id_artikel`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id_komentar`);

--
-- Indexes for table `laporan_transaksi`
--
ALTER TABLE `laporan_transaksi`
  ADD PRIMARY KEY (`id_laporan`),
  ADD UNIQUE KEY `id_transaksi` (`id_transaksi`) USING BTREE;

--
-- Indexes for table `medis`
--
ALTER TABLE `medis`
  ADD PRIMARY KEY (`id_medis`);

--
-- Indexes for table `obat`
--
ALTER TABLE `obat`
  ADD PRIMARY KEY (`id_obat`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id_review`),
  ADD UNIQUE KEY `id_user` (`id_medis`,`id_user`) USING BTREE,
  ADD KEY `id_user_2` (`id_user`);

--
-- Indexes for table `tambah_layanan`
--
ALTER TABLE `tambah_layanan`
  ADD PRIMARY KEY (`id_tambah`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_medis` (`id_medis`,`id_user`),
  ADD KEY `id_obat` (`id_obat`),
  ADD KEY `FK_tambah` (`id_tambah`),
  ADD KEY `id_gejala` (`id_transaksi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id_artikel` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id_komentar` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laporan_transaksi`
--
ALTER TABLE `laporan_transaksi`
  MODIFY `id_laporan` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `medis`
--
ALTER TABLE `medis`
  MODIFY `id_medis` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `obat`
--
ALTER TABLE `obat`
  MODIFY `id_obat` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id_user` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tambah_layanan`
--
ALTER TABLE `tambah_layanan`
  MODIFY `id_tambah` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `laporan_transaksi`
--
ALTER TABLE `laporan_transaksi`
  ADD CONSTRAINT `laporan_transaksi_ibfk_1` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id_transaksi`);

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `pengguna` (`id_user`),
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`id_medis`) REFERENCES `medis` (`id_medis`);

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `FK_tambah` FOREIGN KEY (`id_tambah`) REFERENCES `tambah_layanan` (`id_tambah`) ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`id_medis`) REFERENCES `medis` (`id_medis`),
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `pengguna` (`id_user`),
  ADD CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
