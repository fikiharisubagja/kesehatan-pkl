-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 30, 2019 at 11:36 PM
-- Server version: 10.3.14-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id8560625_arkamayamedical`
--

-- --------------------------------------------------------

--
-- Table structure for table `laporan_transaksi`
--

CREATE TABLE `laporan_transaksi` (
  `id_laporan` int(50) NOT NULL,
  `id_transaksi` int(50) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `medis`
--

CREATE TABLE `medis` (
  `id_medis` int(50) NOT NULL,
  `photo_medis` text NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `usia` int(3) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(12) NOT NULL,
  `jenis_kelamin` varchar(100) NOT NULL,
  `bagian_tugas` enum('UGD','puskesmas','klinik','poliklinik') NOT NULL,
  `sip` varchar(100) NOT NULL,
  `str` varchar(100) NOT NULL,
  `stb` varchar(100) NOT NULL,
  `ijazah` varchar(100) NOT NULL,
  `ktp` varchar(100) NOT NULL,
  `status_medis` enum('dokter','perawat','bidan') NOT NULL,
  `status_register` enum('terverifikasi','belum terverifikasi') NOT NULL,
  `is_login` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medis`
--

INSERT INTO `medis` (`id_medis`, `photo_medis`, `nama_lengkap`, `username`, `password`, `email`, `usia`, `alamat`, `no_hp`, `jenis_kelamin`, `bagian_tugas`, `sip`, `str`, `stb`, `ijazah`, `ktp`, `status_medis`, `status_register`, `is_login`) VALUES
(1, '', 'fery suyatna', 'fery', '$2y$10$l5OakkHAb4djy42sQ04LfeXI9fccaiGP94qO636WMnHsXXXlavt.y', 'ferysuyatna08@gmail.com', 20, 'Perumahan D\'amerta blok d6-3', '0873232343', '', 'UGD', '435.jpg', '435.jpg', '435.jpg', '435.jpg', '46465474767', 'dokter', 'terverifikasi', '0'),
(2, '', 'Wayan Agus', 'agus', '123', 'agus@gmail.com', 20, 'Bali', '087323234356', '', 'klinik', 'fgh.jpg', 'fgh.jpg', 'fgh.jpg', 'fgh.jpg', 'fgh.jpg', 'bidan', 'terverifikasi', '1');

-- --------------------------------------------------------

--
-- Table structure for table `obat`
--

CREATE TABLE `obat` (
  `id_obat` int(50) NOT NULL,
  `nama_obat` varchar(50) NOT NULL,
  `harga_satuan_obat` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `obat`
--

INSERT INTO `obat` (`id_obat`, `nama_obat`, `harga_satuan_obat`) VALUES
(1, 'Alkohol 70%', 5000),
(2, 'Antimo Dewasa', 5000),
(3, 'Balsem Geliga 10 gr', 5000),
(4, 'Balsem Geliga 20 gr', 8),
(5, 'Balsem Lang 10 gr', 5000),
(6, 'Balsem Lang 20 gr', 8000);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id_dorder` int(11) NOT NULL,
  `dalamat` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dgejala` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dbarang` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id_dorder`, `dalamat`, `dgejala`, `dbarang`) VALUES
(1, 'Jl. Pantai Lebih', 'Batuk berdahak', '-'),
(2, 'bali', '-', '-'),
(3, '$bali', '$-', '$-');

-- --------------------------------------------------------

--
-- Table structure for table `panggil`
--

CREATE TABLE `panggil` (
  `alamat_pemesan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_panggil` int(11) NOT NULL,
  `id_obat` int(50) NOT NULL,
  `id_user` int(50) NOT NULL,
  `gejala` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `barang` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status_order` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `panggil`
--

INSERT INTO `panggil` (`alamat_pemesan`, `id_panggil`, `id_obat`, `id_user`, `gejala`, `barang`, `status_order`) VALUES
('Jl. Pandanus VII Blok D6 No.8, Lengkong, Bojongsoang, Bandung, Jawa Barat 40287, Indonesia', 24, 2, 7, '_', '_', '1'),
('Jl. Pandanus VII Blok D6 No.8, Lengkong, Bojongsoang, Bandung, Jawa Barat 40287, Indonesia', 25, 1, 8, 'j', 'j', '1'),
('Jl. Pandanus VII Blok D6 No.8, Lengkong, Bojongsoang, Bandung, Jawa Barat 40287, Indonesia', 26, 4, 15, '-', '-', '1'),
('Jl. Pandanus VII Blok D6 No.8, Lengkong, Bojongsoang, Bandung, Jawa Barat 40287, Indonesia', 27, 5, 5, '-', '-', '1'),
('', 28, 1, 15, '_', '_', '1'),
('Jl. Pandanus VII Blok D6 No.8, Lengkong, Bojongsoang, Bandung, Jawa Barat 40287, Indonesia', 29, 2, 15, '_', '_', '1'),
('Jl. Pandanus VII Blok D6 No.8, Lengkong, Bojongsoang, Bandung, Jawa Barat 40287, Indonesia', 30, 3, 15, '_', '_', '1'),
('$alamat', 31, 1, 15, '$gejala', '$barang', '1'),
('Jl. Pandanus VII Blok D6 No.8, Lengkong, Bojongsoang, Bandung, Jawa Barat 40287, Indonesia', 32, 5, 15, '_', 'kecil', '1');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id_user` int(50) NOT NULL,
  `photo` text NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(70) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(12) NOT NULL,
  `usia` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(100) NOT NULL,
  `status` enum('user','admin') NOT NULL,
  `is_login` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id_user`, `photo`, `username`, `password`, `nama_lengkap`, `email`, `alamat`, `no_hp`, `usia`, `jenis_kelamin`, `status`, `is_login`) VALUES
(1, '', 'fery', '$2y$10$ofESk1K7Aa1aR/iH9.0UcuuV4CBfbABIjsAUKYYmjm0M2xsfOMJJa', '', 'ferysuyatna08@gmail.com', 'Jalan pantai lebih, Gianyar', '0873232343', '0', '', 'user', '0'),
(2, '', '', '$2y$10$1p8lwQDg/5aiR/P2j0pPf.SFVXTJB3wrp85vYs7Mh8o', '', '', '', '', '0', '', 'user', '0'),
(5, '', 'agus', '$2y$10$ofESk1K7Aa1aR/iH9.0UcuuV4CBfbABIjsAUKYYmjm0M2xsfOMJJa', '', 'pondokcode@gmail.com', 'Baliii', '9098879787', '0', '', 'user', '0'),
(6, '', 'gg', 'gg', '', 'gg@gmail.com', 'ciganitri', '9039543', '0', '', 'user', '0'),
(7, '', 'kk', 'kk', '', 'kk@gmail.com', 'ciganitri', '9039543', '0', '', 'user', '0'),
(8, '', 'bima', 'bima', '', 'bima@gmai.com', 'ciganitri', '9039543', '0', '', 'user', '0'),
(9, '', '$username', '$password', '', '$email', '$alamat', '57547', '0', '', 'user', '0'),
(10, '', 'fury', '$2y$10$H4my0WBT71ZKME3EJ.GPAuF.wfL7j78PcLcvT0eifAveq49vehgUa', '', 'fury@gmail.com', '', '', '0', '', 'user', '0'),
(11, '', 'dede', '$2y$10$JJWv56R.z.6B392s1.3U5.R.xL9k9irDp2Rp98aKIddAU3SAy5KpS', '', 'dede@gmail.com', '', '', '0', '', 'user', '0'),
(12, '', 'gun', '$2y$10$kuzlcQ/pXxut9W3Q4K4tvewvDKJHfShkwLhLk8Q41HMQVPnmg52HG', '', 'gun@gmail.col', '', '', '', '', 'user', '0'),
(13, '', 'gas', '$2y$10$NCnTKI9aKBCkO79KTrPbTOu9K3SFtRuqhrPsdpuXa7tyX7mjLol.G', '', 'gas@gmail.com', '', '', '', '', 'user', '0'),
(14, '', 'ferys', '$2y$10$P9LpzyqCR2eKG.iHgMU2qezFEjHd8.kKXD2zZMjTVTsBoNcPi54MK', 'fery suyatna', 'ferya@gmail.com', 'bali', '08766551', '20', '', 'user', '0'),
(15, 'http://https://apiarkamedical.000webhostapp.com/profile_image/15.jpeg', 'bali', '$2y$10$y1q0RdbBwP4q4A/q4xQIWeDdekU4aP5j2bx08dACdfXxU37ePkG5e', 'bali united', 'bali@gmail.com', 'bali', '0986552', '10', '', 'user', '0'),
(22, 'http://https://apiarkamedical.000webhostapp.com/profile_image/22.jpeg', 'kadek', '$2y$10$/.5WiJIv9cWHZLlpE3XMDOTbtv0aObKTcGcC7v6qsNDQp0WKiO1AS', 'kadek herik', 'kadek@gmail.com', 'br.  kesian', '087759872', '20', '', 'user', '0'),
(23, '20', 'kun', '$2y$10$3cOlAtqLWaCoTnsfIZvv8ugDJzpozQ2uwum9kQAOPVPKIH2wq4PHu', 'kun', 'kun@gmail.com', 'bali', '08775982550', '20', '', 'user', '0');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id_review` int(50) NOT NULL,
  `id_medis` int(50) NOT NULL,
  `id_user` int(50) NOT NULL,
  `jumlah_review` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tambah_layanan`
--

CREATE TABLE `tambah_layanan` (
  `id_tambah` int(50) NOT NULL,
  `nama_tambahan` varchar(250) NOT NULL,
  `harga` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tes`
--

CREATE TABLE `tes` (
  `id_tes` int(11) NOT NULL,
  `deskripsi_barang` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi_gejala` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `timmedis`
--

CREATE TABLE `timmedis` (
  `id_tim` int(11) NOT NULL,
  `nama_lengkap` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `usia` int(50) NOT NULL,
  `alamat` text COLLATE utf8_unicode_ci NOT NULL,
  `no_hp` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `timmedis`
--

INSERT INTO `timmedis` (`id_tim`, `nama_lengkap`, `email`, `password`, `usia`, `alamat`, `no_hp`) VALUES
(1, 'agus', 'agus@gmail.com', '123', 20, 'Bali', 546546546);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(50) NOT NULL,
  `id_user` int(50) NOT NULL,
  `id_medis` int(50) NOT NULL,
  `id_tambah` int(50) DEFAULT NULL,
  `id_obat` int(50) DEFAULT NULL,
  `jumlah_obat` int(30) NOT NULL,
  `total_obat` int(30) NOT NULL,
  `alamat` text NOT NULL,
  `alamat_detail` text NOT NULL,
  `jenis_medis` enum('dokter','perawat','bidan') NOT NULL,
  `status` enum('sedang mencari medis','diproses','selesai') NOT NULL,
  `harga_bidan` int(30) NOT NULL,
  `harga_dokter` int(30) NOT NULL,
  `harga_perawat` int(30) NOT NULL,
  `des_gejala` varchar(500) NOT NULL,
  `total_bayar` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users_table`
--

CREATE TABLE `users_table` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_table`
--

INSERT INTO `users_table` (`id`, `name`, `email`, `password`) VALUES
(1, 'gh', 'gh@gmail.com', '$2y$10$ZepRMmVbC1shH2P1DwsaNOG/0oyavhUUygDAA51y4yd5nJ7KcghOO'),
(2, 'herik', 'herik@gmali.com', '$2y$10$6HAHlDoZynoZBhd06EwXCOhbL6tdbDBa.CjQ06BZN0lR.W/XUwkJ.'),
(3, 'heeik', 'suyatna08@gmail.com', '$2y$10$.v9zC3/ClTFXkeOGreWugO5e.6DIwAzpqFlrIFS9U6s4QnhcDGF/a'),
(4, 'feru', 'feru@gmail.com', '$2y$10$ucjT6I3NgKI0Gz.LiGwTd.5FbbZUNxy3f872svIuEH0NdbZ6nxwS.'),
(5, 'ere', 'ere08@gmail.com', '$2y$10$M6zyox9vXheRSp80KWBdCey.vcseUjG5GiyjLeoBRhhKFv25RlthG'),
(6, 'ferysuyatna', 'ferys@gmail.com', '$2y$10$441ICVUq.JRQwOGbkEaeKO5WPGnCEMLUO79gw388V1kwp17FqlMnC'),
(7, 'gh', 'gh@gmail.com', '$2y$10$l5OakkHAb4djy42sQ04LfeXI9fccaiGP94qO636WMnHsXXXlavt.y');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `laporan_transaksi`
--
ALTER TABLE `laporan_transaksi`
  ADD PRIMARY KEY (`id_laporan`),
  ADD UNIQUE KEY `id_transaksi` (`id_transaksi`) USING BTREE;

--
-- Indexes for table `medis`
--
ALTER TABLE `medis`
  ADD PRIMARY KEY (`id_medis`);

--
-- Indexes for table `obat`
--
ALTER TABLE `obat`
  ADD PRIMARY KEY (`id_obat`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id_dorder`);

--
-- Indexes for table `panggil`
--
ALTER TABLE `panggil`
  ADD PRIMARY KEY (`id_panggil`),
  ADD KEY `id_obat` (`id_obat`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id_review`),
  ADD UNIQUE KEY `id_user` (`id_medis`,`id_user`) USING BTREE,
  ADD KEY `id_user_2` (`id_user`);

--
-- Indexes for table `tambah_layanan`
--
ALTER TABLE `tambah_layanan`
  ADD PRIMARY KEY (`id_tambah`);

--
-- Indexes for table `tes`
--
ALTER TABLE `tes`
  ADD PRIMARY KEY (`id_tes`);

--
-- Indexes for table `timmedis`
--
ALTER TABLE `timmedis`
  ADD PRIMARY KEY (`id_tim`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_medis` (`id_medis`,`id_user`),
  ADD KEY `id_obat` (`id_obat`),
  ADD KEY `FK_tambah` (`id_tambah`),
  ADD KEY `id_gejala` (`id_transaksi`);

--
-- Indexes for table `users_table`
--
ALTER TABLE `users_table`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `laporan_transaksi`
--
ALTER TABLE `laporan_transaksi`
  MODIFY `id_laporan` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `medis`
--
ALTER TABLE `medis`
  MODIFY `id_medis` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `obat`
--
ALTER TABLE `obat`
  MODIFY `id_obat` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id_dorder` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `panggil`
--
ALTER TABLE `panggil`
  MODIFY `id_panggil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id_user` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tambah_layanan`
--
ALTER TABLE `tambah_layanan`
  MODIFY `id_tambah` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tes`
--
ALTER TABLE `tes`
  MODIFY `id_tes` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `timmedis`
--
ALTER TABLE `timmedis`
  MODIFY `id_tim` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_table`
--
ALTER TABLE `users_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `laporan_transaksi`
--
ALTER TABLE `laporan_transaksi`
  ADD CONSTRAINT `laporan_transaksi_ibfk_1` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id_transaksi`);

--
-- Constraints for table `panggil`
--
ALTER TABLE `panggil`
  ADD CONSTRAINT `panggil_ibfk_1` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `panggil_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `pengguna` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `pengguna` (`id_user`),
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`id_medis`) REFERENCES `medis` (`id_medis`);

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `FK_tambah` FOREIGN KEY (`id_tambah`) REFERENCES `tambah_layanan` (`id_tambah`) ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`id_medis`) REFERENCES `medis` (`id_medis`),
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `pengguna` (`id_user`),
  ADD CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
