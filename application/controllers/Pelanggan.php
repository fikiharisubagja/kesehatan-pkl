	<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan extends CI_Controller {

	function __construct() {
	parent::__construct();

	$this->general->checkPasien(); // Pengecekan Hak Akses Admin, jika bukan Admin maka akan diredirect ke form Loginn
	}

	public function signin()
	{
		$this->load->view('user/login.php');
	}

	public function index()
	{
		$this->load->view('user/pelanggan/index');
	}

	public function pesan_dokter()
	{
		$data['poli'] = $this->db->user_enum('medis', 'poli');
		$data['jenis_obat']= $this->db->user_enum('obat', 'jenis_obat');
		$this->load->view('user/pelanggan/pesan_dokter', $data);
	}

	public function pesan_bidan()
	{
		$data['jenis_obat']= $this->db->user_enum('obat', 'jenis_obat');
		$this->load->view('user/pelanggan/pesan_bidan', $data);
	}

			public function kritiksaran()
	{
		$nama_lengkap = $_POST['nama_lengkap'];
		$email = $_POST['email'];
		$isi = $_POST['isi'];

        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = 'medicalarkamaya@gmail.com';
        $config['smtp_pass'] = 'arkamaya123';
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

		$ci->email->initialize($config);
        $ci->email->from($email, $nama_lengkap);
        $ci->email->to('rickazakia97@gmail.com');
        $ci->email->subject('');
        $ci->email->message($isi);
        if ($this->email->send()) {
            redirect('pelanggan/kontak');
        } else {
            show_error($this->email->print_debugger());
        }
    }

	public function pesan_perawat()
	{
		$data['jenis_obat']= $this->db->user_enum('obat', 'jenis_obat');
		$this->load->view('user/pelanggan/pesan_perawat', $data);
	}

	public function profile()
	{
		$username = $this->session->userdata['username'];
		$data['profile'] = $this->db->query("select * from pengguna where username = '$username'");
		$this->load->view('user/pelanggan/profile', $data);
	}

	public function riwayat()
	{	
		$id_user = $this->session->userdata['id_user'];
		
		$data['pesanan'] = $this->db->query("select transaksi.id_transaksi, transaksi.jenis_medis, medis.nama_lengkap, transaksi.des_gejala, obat.nama_obat, transaksi.jumlah_obat, tambah_layanan.nama_tambahan, transaksi.alamat, transaksi.alamat_detail, transaksi.total_bayar from transaksi left join obat using(id_obat) left join pengguna using(id_user) left join medis using(id_medis) left join tambah_layanan using(id_tambah) where transaksi.id_user=$id_user");
		$this->load->view('user/pelanggan/riwayat', $data);
	}

	public function do_delete_pesanan($id_pemesanan){//parameter $nim didapat dari teknik redirect slash $d['nim'] ketika link ke controller dari view
	$where = array('id_pemesanan' => $id_pemesanan);
	$res = $this->mymodel->DeleteData('pemesanan',$where);
			if($res >= 1){
			$this->session->set_flashdata('pesan','Delete Data '.$id_pemesanan.' Berhasil');
			redirect('Pelanggan/data_pesanan');
			}
			else{
			echo "<h2>Delete Data Gagal</h2>";		
			}	
	}

	public function pesan_medis()
	{
		$id_user = $this->session->userdata['id_user'];
		$data['status'] = $this->db->user_enum('transaksi', 'status');
		$data['profile'] = $this->db->query("select * from pengguna where id_user = '$id_user'");
		$this->load->view('user/pelanggan/pesan_medis', $data);
	}

	public function gabung()
	{	
		$data['poli'] = $this->db->user_enum('medis', 'poli');
		$data['kendaraan'] = $this->db->user_enum('medis', 'kendaraan');
		$this->load->view('user/pelanggan/v_gabung', $data);
	}


	public function persyaratan()
	{	
		
		$this->load->view('user/pelanggan/input_syarat');
	}

	public function kontak()
	{	
		
		$this->load->view('user/pelanggan/kontak');
	}

	public function tentang_kami()
	{	
		
		$this->load->view('user/pelanggan/tentang_kami');
	}

	public function maps_dokter()
	{	
		
		$this->load->view('user/pelanggan/maps_dokter');
	}

	public function maps_bidan()
	{	
		
		$this->load->view('user/pelanggan/maps_bidan');
	}

	public function maps_perawat()
	{	
		
		$this->load->view('user/pelanggan/maps_perawat');
	}

	public function melahirkan()
	{	
		
		$this->load->view('user/pelanggan/melahirkan');
	}

	public function lansia()
	{
		$this->load->view('user/pelanggan/lansia');
	}

	public function hidupsehat()
	{
		$this->load->view('user/pelanggan/hidupsehat');
	}

	public function makansehat()
	{
		$this->load->view('user/pelanggan/makansehat');
	}

	public function bayibatuk()
	{
		$this->load->view('user/pelanggan/bayibatuk');
	}

	public function nyamuk()
	{
		$this->load->view('user/pelanggan/nyamuk');
	}

public function do_edit_profile(){

				$id_user = $_POST['id_user'];
				$nama_lengkap = $_POST['nama_lengkap'];
				$usia = $_POST['usia'];
				$alamat = $_POST['alamat'];
				$email = $_POST['email'];
				$no_hp = $_POST['no_hp'];
				$photo = $_POST['photo'];
				$data = array('id_user' => $id_user, 'nama_lengkap' => $nama_lengkap, 'usia' => $usia, 'alamat' => $alamat, 'email' => $email, 'no_hp'=> $no_hp, 'photo' => $photo);
				$where = array('id_user'=>$id_user);
			
				$res=$this->mymodel->UpdateData('pengguna',$data,$where);

				if($res >= 1){
				$this->session->set_flashdata('success','<div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="icon fa fa-check"></i>
                Edit data sukses.
              </div>');	
				header('location:'.base_url().'index.php/Pelanggan/profile');
				}else{
				echo "<h2>Gagal Edit Data</h2>";	
				}
		
	}

	public function do_upload_profile(){
		$this->load->helper(array('form','url'));
		$photo->$photo;
		$this->load->library('upload');
		$config['upload_path'] = './assets/images/';
		$config['allowe_types'] = 'gif|jpg|png|jpeg|bmp';
		$config['max_size'] = '2048';
		$config['max_width'] = '1288';
		$config['max_height'] = '768';
		$id_user = $_POST['id_user'];

		$this->upload->initialize($config);
		if(!$this->upload->do_upload_profile('photo')){
			$photo="";
		}else{
			$photo=$this->upload->file_name;
		}
		$data = array(
			'photo' => $photo,
		);
		$where = array('id_user'=>$id_user);
		$this->mymodel->UpdateData('pengguna',$data,$where);
	}

	public function insert_persyaratan(){

	$nama_lengkap = $_POST['nama_lengkap'];// data nim dari textfield di passing ke controller menjadi $nim dengan teknik $_POST 
	//data nama dari textfield di passing ke controller menjadi $nama dengan teknik $_POST
	$tgl_lahir = $_POST['tanggal_lahir'];
	$alamat_lengkap = $_POST['alamat_lengkap'];
	$kendaraan = $_POST['kendaraan'];
	$email = $_POST['email'];
	$lulusan = $_POST['lulusan'];
	$poli = $_POST['poli'];
	$password = md5($_POST['password']);
			
	$data = array('nama_lengkap'=>$nama_lengkap, 'password'=>$password, 'email'=>$email, 'tanggal_lahir' => $tgl_lahir,'alamat_lengkap' => $alamat_lengkap, 'kendaraan' => $kendaraan, 'lulusan' => $lulusan, 'poli' => $poli);

		//$res = $this->db->insert('tbl_user',$data_masuk);
	$res=$this->mymodel->InsertData('dokter',$data);

	 // $this->session->set_userdata('id_user', $re['id_user']);
	 // $id_user = $this->session->userdata['id_user'];

		if($res >= 1){
		echo "<script>
		alert('Data berhasil diinput');
		window.location.href='signin';
		</script>";
		}else{
		echo "<script>
		alert('Data Gagal diinput');
		window.location.href='signin';
		</script>";	
	    }

		
	}


	public function input_syarat()
	{
		
		$this->load->view('user/pelanggan/input_syarat');
	}

	public function do_insert_pesanan(){
		$id_user = $this->session->userdata['id_user'];
		$user = $id_user;
		$nama_pasien = $_POST['nama_pasien'];// data nim dari textfield di passing ke controller menjadi $nim dengan teknik $_POST 
		$alamat = $_POST['alamat'];//data nama dari textfield di passing ke controller menjadi $nama dengan teknik $_POST
		$gejala = $_POST['gejala'];
		$usia = $_POST['usia'];
		$des_gejala = $_POST['des_gejala'];
		$status = 'sedang mencari medis';
		$jenis_medis = $_POST['jenis_medis'];
		$tarif = (($jenis_medis=='dokter')? "100000" : (($jenis_medis=='perawat')? "50000" : (($jenis_medis=='bidan')? "200000" : "0")));

		// "'if ($jenis_medis == 'dokter') {
		// 	100000;
		// }elseif ($jenis_medis == 'perawat') {
		// 	50000;
		// };'";

		$data = array('id_user'=>$user,'nama_pasien'=>$nama_pasien, 'alamat' => $alamat, 'usia' => $usia, 'gejala' => $gejala,'des_gejala' => $des_gejala, 'jenis_medis' => $jenis_medis, 'status' => $status, 'tarif' => $tarif);
		
		//$res = $this->db->insert('tbl_user',$data_masuk);
		$res=$this->mymodel->InsertData('transaksi',$data);
		if($res >= 1){
		echo "<script>
		alert('Tambah Pasien Berhasil');
		window.location.href='riwayat';
		</script>";
		}else{
		echo "<script>
		alert('Tambah Pasien Gagal');
		window.location.href='riwayat';
		</script>";	
	    }
		
	}


}