<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tamu extends CI_Controller {

	public function aa()
	{
		$x['data']=$this->mymodel->get_data_jumlah();
   
		$this->load->view('admin/aa');	
	}


	function keamanan(){
	$username = $this->session->userdata('username');
	if(empty($username)){
		$this->session->sess_destroy();
		redirect('signin');
		}
	}

	public function index()
	{	
		
		//jika user dengan status = user dan is_login=1 maka masuk ke C_Pelanggan->index
		//jika medis dengan is_login=1 dan status_medis = dokter/perawat/bidan maka masuk ke C_Medis->index
		$this->load->view('user/tamu/index.php');
	}

	public function signin()
	{	

		$this->load->view('user/login.php');
	}

	public function pesan_medis()
	{
		$this->load->view('user/tamu/pesan_medis');
	}

	public function tentang_kami()
	{
		$this->load->view('user/tamu/tentang_kami');
	}

	public function kontak()
	{
		$this->load->view('user/tamu/kontak');
	}

	public function lansia()
	{
		$this->load->view('user/tamu/lansia');
	}

	public function nyamuk()
	{
		$this->load->view('user/tamu/nyamuk');
	}

	public function melahirkan()
	{
		$this->load->view('user/tamu/melahirkan');
	}

	public function bayibatuk()
	{
		$this->load->view('user/tamu/bayibatuk');
	}

	public function hidupsehat()
	{
		$this->load->view('user/tamu/hidupsehat');
	}

	public function makansehat()
	{
		$this->load->view('user/tamu/makansehat');
	}
	public function registrasi_pasien()
	{
		$this->load->view('user/tamu/registrasi_pasien');
	}

	public function kritiksaran()
	{
		$nama_lengkap = $_POST['nama_lengkap'];
		$email = $_POST['email'];
		$isi = $_POST['isi'];

        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = 'medicalarkamaya@gmail.com';
        $config['smtp_pass'] = 'arkamaya123';
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

		$ci->email->initialize($config);
        $ci->email->from($email, $nama_lengkap);
        $ci->email->to('rickazakia97@gmail.com');
        $ci->email->subject('');
        $ci->email->message($isi);
        if ($this->email->send()) {
            redirect('tamu/kontak');
        } else {
            show_error($this->email->print_debugger());
        }
    }

	public function registrasi_medis()
	{	
		$data['status_medis'] = $this->db->user_enum('medis', 'status_medis');
		$this->load->view('user/tamu/registrasi_medis', $data);
	}

	public function insert_persyaratan(){

		$this->load->library('upload');

		$config['upload_path'] = './libs/medis';
			// Tambah Allowed type
		$config['allowed_types'] = 'gif|jpg|png|pdf|mp3|exe|jpeg|';
		$config['max_size'] = '1000';
		$config['max_width'] = '3000';
		$config['max_height'] = '3000';
		$this->upload->initialize($config);
		$this->upload->do_upload('sip') ;
		$sip = $this->upload->file_name;

		$this->upload->do_upload('str');
		$str = $this->upload->file_name;

		$this->upload->do_upload('stb');
		$stb = $this->upload->file_name;

		$this->upload->do_upload('ijazah');
		$ijazah = $this->upload->file_name;

		$nama_lengkap = $_POST['nama_lengkap'];
		$username = $_POST['username'];
		$email = $_POST['email'];
		$password = password_hash($_POST['password'], PASSWORD_DEFAULT);
		$no_hp = $_POST['no_hp'];
		$status_medis = $_POST['status_medis'];
		$status_register = 'belum terverifikasi';
		$data = array('nama_lengkap'=>$nama_lengkap, 'username' => $username,'email'=>$email,'password'=>$password, 'no_hp' => $no_hp, 'status_medis'=>$status_medis,'sip' => $sip, 'str' => $str, 'stb' => $stb, 'ijazah' => $ijazah, 'status_register' => $status_register);
		//$res = $this->db->insert('tbl_user',$data_masuk);

		$res=$this->mymodel->InsertData('medis',$data);
			if($res >= 1){
			echo "<script>
			alert('Registrasi Medis Berhasil Data sedang diverifikasi harap tunggu kurang dari 24 jam kami akan mengirim via email jika data sudah diverifikasi');
			window.location.href='signin';
			</script>";
			}else{
			echo "<script>
			alert('Gagal registrasi');
			window.location.href='signin';
			</script>";	
		    }
			
		 }

	public function do_registration(){
	//$id_user = $_POST['id_user'];
	$nama_lengkap = $_POST['nama_lengkap'];
	$username = $_POST['username'];
	$email = $_POST['email'];
	$password = password_hash($_POST['password'], PASSWORD_DEFAULT);
	$no_hp = $_POST['no_hp'];
	$status = 'user';

	$data = array('nama_lengkap' => $nama_lengkap, 'username' => $username,'email' =>$email,'password' => $password, 'no_hp' => $no_hp, 'status' => $status);
	// print_r($data);
	// die();
	$res=$this->mymodel->RegistData('pengguna',$data);

		if($res >= 1){
		echo "<script>
		alert('Registrasi User Berhasil');
		window.location.href='signin';
		</script>";
		}else{
		echo "<script>
		alert('Registrasi User Gagal');
		window.location.href='signin';
		</script>";	
	    }
	}
	
	
	//untuk login
	public function login()
	{
	
	    $username = $this->input->post("username");
        $password = password_verify($this->input->post("password"));
        $cnt = $this->db->get_where('pengguna', array('username' => $username, 'password' => $password))->num_rows();
        $cntM = $this->db->get_where('medis', array('username' => $username, 'password' => $password))->num_rows();

        $pengguna = $this->db->get_where('pengguna', array('username' => $username))->row_array();

        $medis = $this->db->get_where('medis', array('username' => $username))->row_array();
        // $this->username = $username;

            if ($cnt > 0 && $pengguna['status'] == 'admin') 
			{
			      $this->db->update("pengguna", 
                    array(
                        'is_login' => '1'
                    )
                   );

			    $this->session->set_flashdata('login','<script>window.alert("Login sukses");</script>');
                $this->session->set_userdata('data', $pengguna);  
                $this->session->set_userdata('status', $pengguna['status']);
                $this->session->set_userdata('is_login', $pengguna['is_login']);
                header('location:'.base_url().'index.php/admin/Adminn');
           		
            } else if ($cnt > 0 && $pengguna['status'] == 'user'){
				$this->db->update("pengguna", 
                    array(
                        'is_login' => '1'
                    )
                   );
				 //Session seluruh data				
                $this->session->set_userdata('data', $pengguna);
                //session satu data
                $this->session->set_userdata('id_user', $pengguna['id_user']);
                $this->session->set_userdata('status', $pengguna['status']);
                $this->session->set_userdata('is_login', $pengguna['is_login']);
				$this->session->set_userdata('username', $pengguna['username']);
				header('location:'.base_url().'index.php/Pelanggan');

			} else if ($cntM > 0 && $medis['status_medis'] == 'dokter' || $medis['status_medis'] == 'bidan' || $medis['status_medis'] == 'perawat'){
				$this->db->update("medis", 
						array(
	                    'is_login' => '1'
	                )
	               );
				//Session seluruh data
				$this->session->set_userdata('medis', $medis);
				//session satu data
				$this->session->set_userdata('id_medis', $medis['id_medis']);
				$this->session->set_userdata('is_login', $medis['is_login']);
				$this->session->set_userdata('status_medis', $medis['status_medis']);
				$this->session->set_userdata('username', $medis['username']);
				header('location:'.base_url().'index.php/Medis');
				}
				else if (($cnt || $cntM) == 0) {
				$this->session->set_flashdata('salah','<div style="color: red;">Maaf username atau password salah</div>');
				redirect('Tamu/signin');
				}
			
			else 
			{
            $this->session->set_flashdata('other','<div style="color: red;">Maaf gagal login</div>');
			redirect('Tamu/signin');
            }
	}

	public function logout()
 	{	
 		$username = $this->session->userdata['username'];		
 		$pengguna = $this->db->get_where('pengguna', array('username' => $username))-> row_array();
 		$user = $this->db->get_where('medis', array('username' => $username))-> row_array();

 		if ($pengguna['status'] == 'admin') 
			{
			      $this->db->update("pengguna", 
                    array(
                        'is_login' => '0'
                    )
                   );
			$this->session->set_flashdata('logout','<script>window.alert("Logout sukses");</script>');
			$this->session->unset_userdata('username');
			$this->session->unset_userdata('pengguna');
			session_destroy();
            header('location:'.base_url().'index.php/Tamu');
           
            } else if ($pengguna['status'] == 'user'){
				$this->db->update("pengguna", 
                    array(
                        'is_login' => '0'
                    )
                   );
			$this->session->unset_userdata('username');
			session_destroy();
			header('location:'.base_url().'index.php/Tamu');
			
			} else if ($user['status_medis'] == 'dokter' || $user['status_medis'] == 'perawat' || $user['status_medis'] == 'bidan'){
				$this->db->update("medis", 
 					array(
                        'is_login' => '0'
                    )
                   );
			$this->session->unset_userdata('username');
			session_destroy();
			header('location:'.base_url().'index.php/Tamu');
				}
			
			else 
			{
               echo "<script>window.alert('Maaf, gagal Logout');
                     </script>";
                     redirect('Tamu/signin');
               
            }

 	}
}
