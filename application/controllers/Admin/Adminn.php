<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminn extends CI_Controller {

//construct adalah method atau fungsi yang dijalankan pertama kali
//sedangkan desruct sebaliknya
function __construct() {
parent::__construct();

$this->general->checkAdmin(); // Pengecekan Hak Akses Admin, jika bukan Admin maka akan diredirect ke form Loginn
$this->load->library('pdf');
}

public function index()
{
    $a = $this->session->userdata['data'];
    $username = $a['username'];

	$x['profile'] = $this->db->query("select * from pengguna where status='admin'");

	$x['bln'] = $this->db->query("
    	select 
    	(select count(*) from laporan_transaksi where month(tanggal)=1) as jan, 
    	(SELECT count(*) from laporan_transaksi where month(tanggal)=2) as feb,
    	(SELECT count(*) from laporan_transaksi where month(tanggal)=3) as mar,
    	(SELECT count(*) from laporan_transaksi where month(tanggal)=4) as apr,
    	(SELECT count(*) from laporan_transaksi where month(tanggal)=5) as mei,
    	(SELECT count(*) from laporan_transaksi where month(tanggal)=6) as jun,
    	(SELECT count(*) from laporan_transaksi where month(tanggal)=7) as jul,
    	(SELECT count(*) from laporan_transaksi where month(tanggal)=8) as agu,
    	(SELECT count(*) from laporan_transaksi where month(tanggal)=9) as sep,
    	(SELECT count(*) from laporan_transaksi where month(tanggal)=10) as okt,
    	(SELECT count(*) from laporan_transaksi where month(tanggal)=11) as nov,
    	(SELECT count(*) from laporan_transaksi where month(tanggal)=12) as des
    	");

	$x['jml']=$this->mymodel->get_data_jumlah();
	$x['jumlah_user'] = $this->db->query("select count(*) as jumlah from pengguna where status = 'user' ");
	$x['jumlah_medis'] = $this->db->query("select count(*) as jumlah from medis");
	$x['jumlah_order'] = $this->db->query("select *, count(*) as jumlah from laporan_transaksi where date(tanggal)=date(now())");
	$x['tampil_order'] = $this->db->query("select * from laporan_transaksi where date(tanggal)=date(now())");
	$x['jumlah_pemasukan'] = $this->db->query("select coalesce(sum(total_bayar), 0) as jumlah from transaksi natural join laporan_transaksi where date(tanggal)=date(now())");

//grapik medis
	// $x['onD'] = $this->db->query("select count(*) as jumlah from medis where status_medis = 'dokter' and status = 'online'");
	// $x['onP'] = $this->db->query("select count(*) as jumlah from medis where status_medis = 'perawat' and status = 'online'");
	// $x['onB'] = $this->db->query("select count(*) as jumlah from medis where status_medis = 'bidan' and status = 'online'");
	$this->load->view('admin/index',$x);	
}

public function totalTransaksi()
{

	$this->load->view('admin/filter_total_transaksi');
	
}

public function printTotal()
{
$start_date = $_POST['start_date'];
$end_date = $_POST['end_date'];

	$a['data_penghasilan'] = $this->db->query("
		select laporan_transaksi.id_laporan, pengguna.username, medis.nama_lengkap, laporan_transaksi.tanggal, transaksi.total_bayar 
		from laporan_transaksi, transaksi, pengguna, medis 
		where laporan_transaksi.id_transaksi=transaksi.id_transaksi and transaksi.id_user=pengguna.id_user and transaksi.id_medis=medis.id_medis and tanggal between '$start_date' and '$end_date';
		");

	$a['subtotal'] = $this->db->query("select sum(total_bayar) as subtot 
		from laporan_transaksi, transaksi
		where laporan_transaksi.id_transaksi = transaksi.id_transaksi and tanggal between '$start_date' and '$end_date'");
	$a['admin'] = $this->db->query("select sum(total_bayar)*0.2 as keuntungan from transaksi where status='selesai'");
	$this->load->view('admin/penghasilan',$a);
	
}

public function print_penghasilan(){
	$a['data_penghasilan'] = $this->db->query("select laporan_transaksi.id_laporan, pengguna.username, medis.nama_lengkap, laporan_transaksi.tanggal, transaksi.total_bayar from laporan_transaksi, transaksi, pengguna, medis where laporan_transaksi.id_transaksi=transaksi.id_transaksi and transaksi.id_user=pengguna.id_user and transaksi.id_medis=medis.id_medis;");
	$a['subtotal'] = $this->db->query("select sum(total_bayar) as subtot from transaksi");
	$a['admin'] = $this->db->query("select sum(total_bayar)*0.2 as keuntungan from transaksi where status='selesai'");
	$this->load->view('admin/print_penghasilan', $a);
}


public function profile_admin(){
	$a['pf'] = $this->db->query("select * from pengguna where status='admin'");
	$this->load->view('admin/profile',$a);
}

function do_edit_profile()
{
	$id_user = $_POST['id_user'];		
	$nama_lengkap = $_POST['nama_lengkap'];
	$password = md5($_POST['password']);
	$email = $_POST['email'];
	$no_hp = $_POST['no_hp'];
	
	$data = array('nama_lengkap' => $nama_lengkap, 'password' => $password,'email' => $email, 'no_hp' => $no_hp);
	$where = array('id_user'=> $id_user);

	$res=$this->mymodel->UpdateData('pengguna',$data,$where);
	
	if($res >= 1){
	$this->session->set_flashdata('success','<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <i class="icon fa fa-check"></i>
    Ubah Password Sukses.
  </div>');	
	header('location:'.base_url().'index.php/admin/Adminn/profile_admin');
	}else{
	echo "<h2>Gagal Edit data</h2>";	
	}
}

public function data_pasien()
{

	$d['data_pasien'] = $this->db->query("select * from pengguna where status = 'user'");
		
	$this->load->view('admin/halaman/data_pasien',$d);
}

// halaman tabel transaksi dan laporan ======================================================================================================

public function data_transaksi()
{

	$d['data_transaksi'] = $this->db->query("select * from transaksi ");
		
	$this->load->view('admin/halaman/data_transaksi',$d);
}

public function data_laporan()
{

	$d['data_laporan'] = $this->db->query("select * from laporan_transaksi ");
		
	$this->load->view('admin/halaman_laporan/data_laporan',$d);
}

public function excel_laporan()
{
		$where = array ('id_laporan' => $id_laporan);
		$d['laporan'] = $this->db->query("select * from laporan_transaksi");
		$this->load->view('admin/halaman_laporan/excel_laporan',$d);

}		

public function do_delete_user($id_user){
$where = array('id_user' => $id_user);
$res = $this->mymodel->DeleteData('pengguna',$where);
if($res >= 1){
$this->session->set_flashdata('pesan','<div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="icon fa fa-check"></i>
            Delete data berhasil.
          </div>');
redirect('admin/Adminn/data_pasien');
}
else{
echo "<h2>Delete Data Gagal</h2>";		
}	
}

public function excel_pemesanan()
{
		$d['data_pemesanan'] = $this->db->query("select * from pemesanan");
		$this->load->view('admin/halaman/excel_pemesanan',$d);

}

public function data_review()
{
	$data['review'] = $this->db->query("SELECT *,pengguna.nama_lengkap as user, medis.nama_lengkap as medis, review.jumlah_review FROM review, medis, pengguna where review.id_medis=medis.id_medis and review.id_user=pengguna.id_user");
	
	$this->load->view('admin/halaman/data_review',$data);
}

//All Medis

public function detail_medis($id_medis)
{
	$a['detail'] = $this->db->query("select * from medis where id_medis = $id_medis");
	$this->load->view('admin/halaman/detail_medis',$a);
}

public function do_verifikasi_medis($id_medis)
{
	$data = $this->session->userdata['data'];
	$username = $data['username'];
	$email_Adm = $data['email'];

	$ci = get_instance();
	$ci->load->library('email');
	$config['protocol'] = "smtp";
    $config['smtp_host'] = "ssl://smtp.gmail.com";
    $config['smtp_port'] = "465";
    $config['smtp_user'] = "fikiharisubagja@gmail.com";
    $config['smtp_pass'] = "makankonate10103011";
    $config['charset'] = "utf-8";
    $config['mailtype'] = "html";
    $config['newline'] = "\r\n";

	$query = $this->db->query("select email, nama_lengkap from medis where id_medis = $id_medis");
	$ab = $query->row();
	$email_U = $ab->email;
	$nama = $ab->nama_lengkap;

	$status_register = 'terverifikasi';
	$d = array('status_register' => $status_register);	
	$where = array('id_medis' => $id_medis);
	//$res = $this->db->insert('tbl_user',$data_masuk);
	$res=$this->mymodel->UpdateData('medis',$d,$where);
	if($res >= 1){  

		$ci->email->initialize($config);

		$ci->email->from($email_Adm, $username);
		$ci->email->to($email_U, $nama);
		$ci->email->subject('Verifikasi Berhasil');
		$ci->email->message('
			<h3> VERIFIKASI BERHASIL. </h3> <br>
			Selamat, anda sekarang sudah bergabung menjadi partner kami dan bisa melakukan transaksi. <br>
			Silahkan install aplikasi Arkamaya Partner Medis di Playstore untuk melakukan transaksi layanan medis, atau dengan klik link di bawah ini. <br>
			<a href="#">Link</a> <br>
			Terima kasih <br>
			CS : No.hp 08938389283

			');
		if ($this->email->send()) {
			$this->session->set_flashdata('notif','<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<i class="icon fa fa-check"></i>
			Medis berhasil diverifikasi. Pemberitahuan kepada user akan dikirim melalui email
			</div>');
			redirect($_SERVER['HTTP_REFERER']);
		} else {
			echo "<h2>Verifikasi medis gagal</h2>";	
		}
	}
}

function ubah(){
	$id_medis = $this->input->post('id_medis');
	$data = array(

		'nama_lengkap'		=> $this->input->post('nama_lengkap')

		);

	$this->mymodel->ubah($data,$id_medis);
	$this->session->set_flashdata('notif','<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<i class="icon fa fa-check"></i>
		Ubah data berhasil.
	</div>');
	redirect($_SERVER['HTTP_REFERER']);
}

public function do_delete_medis($id_medis){
	$where = array('id_medis' => $id_medis);
	$res = $this->mymodel->DeleteData('medis',$where);
	if($res >= 1){
	$this->session->set_flashdata('notif','<div class="alert alert-success">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    <i class="icon fa fa-check"></i>
	    Hapus data berhasil.
	  </div>');
	redirect($_SERVER['HTTP_REFERER']);
	}
	else{
	echo "<h2>Delete Data Gagal</h2>";		
	}	
}

// halaman tabel perawat ============================================================================================================
public function data_perawat()
{	
	$where = 'where status_medis = "perawat"';
	$data['data_perawat'] = $this->mymodel->GetMedis($where);

	$this->load->view('admin/halaman_perawat/data_perawat',$data);
}

public function do_edit_perawat(){

	$id_medis = $_POST['id_medis'];
	$nama_lengkap = $_POST['nama_lengkap'];
	$alamat_lengkap = $_POST['alamat_lengkap'];
	$no_hp= $_POST['no_hp'];
	$data = array('id_medis' => $id_medis,'nama_lengkap' => $nama_lengkap,'alamat_lengkap' => $alamat_lengkap, 'no_hp'=> $no_hp);
	$where = array('id_medis'=>$id_medis);
	$res=$this->mymodel->UpdateData('medis',$data,$where);

	if($res >= 1){
	$this->session->set_flashdata('success','<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<i class="icon fa fa-check"></i>
	Edit data sukses.
	</div>');	
	header('location:'.base_url().'index.php/admin/Adminn/data_perawat');
	}else{
	echo "<h2>Gagal Edit Data</h2>";	
	}
	
}


public function excel_perawat()
{
		$where = array ('status' => $status);
		$d['data_perawat'] = $this->db->query("select * from medis");
		$this->load->view('admin/halaman_perawat/excel_perawat',$d);

}




// Data Dokter          //Data Dokter          //Data Dokter          //Data Dokter          //Data Dokter
public function data_dokter()
{
	$where = 'where status_medis = "dokter"';
	$data['data_dokter'] = $this->mymodel->GetMedis($where);
	
	$this->load->view('admin/halaman_dokter/data_dokter',$data);
}

public function do_edit_dokter(){	

			$id_medis = $_POST['id_medis'];
			$nama_lengkap = $_POST['nama_lengkap'];
			$alamat_lengkap = $_POST['alamat_lengkap'];
			$no_hp= $_POST['no_hp'];
			$data = array('id_medis' => $id_medis,'nama_lengkap' => $nama_lengkap,'alamat_lengkap' => $alamat_lengkap, 'no_hp'=> $no_hp);
			$where = array('id_medis'=>$id_medis);
			$res=$this->mymodel->UpdateData('medis',$data,$where);

			if($res >= 1){
			$this->session->set_flashdata('pesan','<div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="icon fa fa-check"></i>
            Ubah data berhasil.
          	</div>');	
			header('location:'.base_url().'index.php/admin/Adminn/data_dokter');
			}else{
			echo "<h2>Gagal Edit Data</h2>";	
			}		
	
	}

public function excel_dokter()
{
		$where = array ('status' => $status);
		$d['data_dokter'] = $this->db->query("select * from medis");
		$this->load->view('admin/halaman_dokter/excel_dokter',$d);

}

public function pdf_dokter()
{
	
    $pdf = new FPDF('l','mm','A5');
    // membuat halaman baru
    $pdf->AddPage();
    // setting jenis font yang akan digunakan
    $pdf->SetFont('Arial','B',16);
    // mencetak string 
    $pdf->Cell(190,7,'ARKAMAYA MEDICAL',0,1,'C');
    $pdf->SetFont('Arial','B',12);
    $pdf->Cell(190,7,'DAFTAR DOKTER',0,1,'C');
    // Memberikan space kebawah agar tidak terlalu rapat
    $pdf->Cell(10,7,'',0,1,'C');
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(25,6,'ID DOKTER',1,0,'C');
    $pdf->Cell(30,6,'NAMA DOKTER',1,0);
    $pdf->Cell(60,6,'ALAMAT LENGKAP',1,0);
	$pdf->Cell(30,6,'NO TELEPON',1,1);
    $pdf->SetFont('Arial','',10);
    $medis = $this->db->get('medis')->result();
    foreach ($medis as $row){
        $pdf->Cell(25,6,$row->id_medis,1,0,'C');
        $pdf->Cell(30,6,$row->nama_lengkap,1,0);
		$pdf->Cell(60,6,$row->alamat,1,0);
		$pdf->Cell(30,6,$row->no_hp,1,1);
    }
    $pdf->Output();


}		

//Data bidan        //Data bidan         //Data bidan         //Data bidan         //Data bidan         //Data bidan

public function data_bidan()
{
	$where = 'where status_medis = "bidan"';
	$data['data_bidan'] = $this->mymodel->GetMedis($where);
		
	$this->load->view('admin/halaman_bidan/data_bidan',$data);
}

public function do_edit_bidan(){

			$id_medis = $_POST['id_medis'];
			$nama_lengkap = $_POST['nama_lengkap'];
			$alamat_lengkap = $_POST['alamat_lengkap'];
			$no_hp= $_POST['no_hp'];
			$data = array('id_medis' => $id_medis,'nama_lengkap' => $nama_lengkap,'alamat_lengkap' => $alamat_lengkap, 'no_hp'=> $no_hp);
			$where = array('id_medis'=>$id_medis);
			$res=$this->mymodel->UpdateData('medis',$data,$where);

			if($res >= 1){
			$this->session->set_flashdata('pesan','<div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="icon fa fa-check"></i>
            Edit data sukses.
          </div>');	
			header('location:'.base_url().'index.php/admin/Adminn/data_bidan');
			}else{
			echo "<h2>Gagal Edit Data</h2>";	
			}
	
}
	
public function excel_bidan()
{
		$where = array ('status' => $status);
		$d['data_bidan'] = $this->db->query("select * from medis");
		$this->load->view('admin/halaman_bidan/excel_bidan',$d);

}

public function pdf_bidan()
{
	
    $pdf = new FPDF('l','mm','A5');
    // membuat halaman baru
    $pdf->AddPage();
    // setting jenis font yang akan digunakan
    $pdf->SetFont('Arial','B',16);
    // mencetak string 
    $pdf->Cell(190,7,'ARKAMAYA MEDICAL',0,1,'C');
    $pdf->SetFont('Arial','B',12);
    $pdf->Cell(190,7,'DAFTAR BIDAN',0,1,'C');
    // Memberikan space kebawah agar tidak terlalu rapat
    $pdf->Cell(10,7,'',0,1);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(20,6,'ID Bidan',1,0);
    $pdf->Cell(30,6,'NAMA BIDAN',1,0);
    $pdf->Cell(55,6,'TEMPAT TANGGAL LAHIR',1,0);
    $pdf->Cell(35,6,'ALAMAT',1,0);
    $pdf->Cell(30,6,'NO HP',1,1);
    $pdf->SetFont('Arial','',10);
    $medis = $this->db->get('medis')->result();
    foreach ($medis as $row){
        $pdf->Cell(20,6,$row->id_medis,1,0);
        $pdf->Cell(30,6,$row->nama_lengkap,1,0);
        $pdf->Cell(55,6,$row->tanggal_lahir,1,0);
        $pdf->Cell(35,6,$row->alamat_lengkap,1,0);
        $pdf->Cell(30,6,$row->no_hp,1,1); 
    }
    $pdf->Output();


}

public function data_tambahan()
{

		$d['data_tambahan'] = $this->db->query("select * from tambah_layanan");
		
	$this->load->view('admin/halaman_tambahan/data_tambahan',$d);
}

public function do_insert_tambahan(){

	// Tambah Allowed type
			$nama_tambahan = $_POST['nama_tambahan'];
			
			$harga = $_POST['harga'];

			$data = array('nama_tambahan'=>$nama_tambahan, 'harga'=>$harga);
	
	$res=$this->mymodel->InsertData('tambah_layanan',$data);
	if($res >= 1){
	$this->session->set_flashdata('pesan','<div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="icon fa fa-check"></i>
            Tambah data sukses.
          </div>');	
	header('location:'.base_url().'index.php/admin/Adminn/data_tambahan');
	}else{
	echo "<h2>Gagal Simpan Data</h2>";	
    }
	
}

public function do_edit_tambahan(){

			$id_tambah = $_POST['id_tambah'];
			$nama_tambahan = $_POST['nama_tambahan'];
			$harga = $_POST['harga'];

			$data = array('id_tambah' => $id_tambah,'nama_tambahan' => $nama_tambahan,'harga
				' => $harga
			);
			$where = array('id_tambah'=>$id_tambah);
			
			$res=$this->mymodel->UpdateData('tambah_layanan',$data,$where);

			if($res >= 1){
			$this->session->set_flashdata('pesan','<div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="icon fa fa-check"></i>
            Ubah data Berhasil.
          </div>');	
			header('location:'.base_url().'index.php/admin/Adminn/data_tambahan');
			}else{
			echo "<h2>Gagal Edit Data</h2>";	
			}		
	
	}
	

public function do_delete_tambahan($id_tambah){
$where = array('id_tambah' => $id_tambah);
$res = $this->mymodel->DeleteData('tambah_layanan',$where);
		if($res >= 1){
		$this->session->set_flashdata('pesan','<div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="icon fa fa-check"></i>
            Hapus data Berhasil.
          </div>');
		redirect('admin/Adminn/data_tambahan');
		}
		else{
		echo "<h2>Delete Data Gagal</h2>";		
		}	
}

//DATA OBAT
public function data_obat()
{
	$data['data_obat'] = $this->mymodel->GetObat();
		
	$this->load->view('admin/halaman_obat/data_obat',$data);
}

public function do_insert_obat(){

	// Tambah Allowed type
	$nama_obat = $_POST['nama_obat'];
	$harga_satuan_obat = $_POST['harga_satuan_obat'];

	$data = array('nama_obat'=>$nama_obat, 'harga_satuan_obat'=>$harga_satuan_obat);
	
	$res=$this->mymodel->InsertData('obat',$data);
	if($res >= 1){
	$this->session->set_flashdata('pesan','<div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="icon fa fa-check"></i>
            Tambah data Berhasil.
          </div>');	
	header('location:'.base_url().'index.php/admin/Adminn/data_obat');
	}else{
	echo "<h2>Gagal Simpan Data</h2>";	
    }
	
}

public function do_edit_obat(){

	$id_obat = $_POST['id_obat'];
	$nama_obat = $_POST['nama_obat'];
	$harga_satuan_obat = $_POST['harga_satuan_obat'];
	
	$data = array('id_obat' => $id_obat,'nama_obat' => $nama_obat, 'harga_satuan_obat' => $harga_satuan_obat);
	$where = array('id_obat'=>$id_obat);
	
	$res=$this->mymodel->UpdateData('obat',$data,$where);

	if($res >= 1){
	$this->session->set_flashdata('pesan','<div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="icon fa fa-check"></i>
            Ubah data Berhasil.
          </div>');	
	header('location:'.base_url().'index.php/admin/Adminn/data_obat');
	}else{
	echo "<h2>Gagal Edit Data</h2>";	
	}		
	
	}
	

public function do_delete_obat($id_obat){
$where = array('id_obat' => $id_obat);
$res = $this->mymodel->DeleteData('obat',$where);
		if($res >= 1){
		$this->session->set_flashdata('pesan','<div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="icon fa fa-check"></i>
            Hapus data Berhasil.
          </div>');
		redirect('admin/Adminn/data_obat');
		}
		else{
		echo "<h2>Delete Data Gagal</h2>";		
		}	
}

public function excel_obat()
{
		$d['data_obat'] = $this->db->query("select * from obat");
		$this->load->view('admin/halaman_obat/excel_obat',$d);

}

public function pdf_obat()
{
	
    $pdf = new FPDF('l','mm','A5');
    // membuat halaman baru
    $pdf->AddPage();
    // setting jenis font yang akan digunakan
    $pdf->SetFont('Arial','B',16);
    // mencetak string 
    $pdf->Cell(190,7,'ARKAMAYA MEDICAL',0,1,'C');
    $pdf->SetFont('Arial','B',12);
    $pdf->Cell(190,7,'DAFTAR OBAT',0,1,'C');
    // Memberikan space kebawah agar tidak terlalu rapat
    $pdf->Cell(10,7,'',0,1);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(20,6,'ID OBAT',1,0);
    $pdf->Cell(30,6,'NAMA OBAT',1,0);
    $pdf->Cell(20,6,'KATEGORI OBAT',1,0);
    $pdf->Cell(30,6,'KANDUNGAN',1,0);
    $pdf->Cell(35,6,'INDIKASI',1,0);
    $pdf->Cell(30,6,'DOSIS',1,0);
    $pdf->Cell(30,6,'STOCK',1,0);
    $pdf->Cell(20,6,'HARGA SATUAN',1,1);
    $pdf->SetFont('Arial','',10);
    $obat = $this->db->get('obat')->result();
    foreach ($obat as $row){
        $pdf->Cell(20,6,$row->id_obat,1,0);
        $pdf->Cell(30,6,$row->nama_obat,1,0);
        $pdf->Cell(30,6,$row->kategori_obat,1,0);
        $pdf->Cell(55,6,$row->kandungan,1,0);
        $pdf->Cell(35,6,$row->indikasi,1,0);
        $pdf->Cell(30,6,$row->dosis,1,0);
        $pdf->Cell(30,6,$row->stock,1,0);
        $pdf->Cell(30,6,$row->harga_satuan,1,1);
    }
    $pdf->Output();


}

}