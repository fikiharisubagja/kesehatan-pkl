	<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Medis extends CI_Controller {

	function __construct() {
	parent::__construct();

	$this->general->checkMedis(); // Pengecekan Hak Akses Admin, jika bukan Admin maka akan diredirect ke form Loginn
	}

	function foto()
	{
		echo '<input name="gambar" type="file" id="img" size="30" maxlength="30" >';
	}


	public function index()
	{
		$this->load->view('user/medis/index');
	}

	public function pesanan_pasien()
	{
		$data['pesanan'] = $this->db->query("select * from pemesanan where jenis_medis = 'dokter'");
		$this->load->view('user/medis/pesanan_pasien', $data);
	}

	public function profile()
	{
		$username = $this->session->userdata['username'];
		$data['profile'] = $this->db->query("select * from medis where username = '$username'");
		$this->load->view('user/medis/profile', $data);
	}

	public function ambil_pesanan()
	{
		$id_medis = $this->session->userdata['id_medis'];
		$data['pesanan'] = $this->db->query("select * from transaksi natural join pemesanan where id_medis = '$id_medis' ");
		$this->load->view('user/medis/ambil_pesanan', $data);
	}

		public function kontak()
	{
		$this->load->view('user/medis/kontak');
	}

	public function kritiksaran()
	{
		$nama_lengkap = $_POST['nama_lengkap'];
		$email = $_POST['email'];
		$isi = $_POST['isi'];

        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = 'medicalarkamaya@gmail.com';
        $config['smtp_pass'] = 'arkamaya123';
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

		$ci->email->initialize($config);
        $ci->email->from($email, $nama_lengkap);
        $ci->email->to('rickazakia97@gmail.com');
        $ci->email->subject('');
        $ci->email->message($isi);
        if ($this->email->send()) {
            redirect('medis/kontak');
        } else {
            show_error($this->email->print_debugger());
        }
    }

	public function tentang_kami()
	{
		$this->load->view('user/medis/tentang_kami');
	}

	public function riwayat()
	{
		$id_medis = $this->session->userdata['id_medis'];
		$data['pesanan'] = $this->db->query("select transaksi.id_transaksi, pengguna.nama_lengkap, pengguna.usia, transaksi.alamat, transaksi.des_gejala, obat.nama_obat, tambah_layanan.nama_tambahan, transaksi.total_bayar from transaksi left join obat using(id_obat) left join pengguna using(id_user) left join tambah_layanan using(id_tambah) where transaksi.id_medis=$id_medis");
		$this->load->view('user/medis/riwayat', $data);
	}

	public function do_edit_profile(){

		        
				$id_medis = $_POST['id_medis'];
				$nama_lengkap = $_POST['nama_lengkap'];
				$usia = $_POST['usia'];
				$alamat = $_POST['alamat'];
				$email = $_POST['email'];
				$no_hp= $_POST['no_hp'];
				$photo_medis = $_POST['photo_medis'];
				$data = array('id_medis' => $id_medis, 'nama_lengkap' => $nama_lengkap, 'usia' => $usia, 'alamat' => $alamat, 'email' => $email,'no_hp'=> $no_hp, 'photo_medis' => $photo_medis);
				$where = array('id_medis'=>$id_medis);
				$res=$this->mymodel->UpdateData('medis',$data,$where);
	
				if($res >= 1){
				$this->session->set_flashdata('success','<div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="icon fa fa-check"></i>
                Edit data sukses.
              </div>');	
				header('location:'.base_url().'index.php/medis/profile');
				}else{
				echo "<h2>Gagal Edit Data</h2>";	
				}
		
	}

	public function do_ambil_pesanan($id_pemesanan)
	{

	$id_medis = $this->session->userdata['id_medis'];
	$status = 'diproses';
	$d = array('status' => $status);
	$data = array('id_pemesanan' => $id_pemesanan, 'id_medis' => $id_medis);	
	
		//$res = $this->db->insert('tbl_user',$data_masuk);
	$res=$this->mymodel->UpdateData('pemesanan',$d);
	$res=$this->mymodel->InsertData('transaksi',$data);
	

	 // $this->session->set_userdata('id_user', $re['id_user']);
	 // $id_user = $this->session->userdata['id_user'];

		redirect('Medis/ambil_pesanan');
	}


}