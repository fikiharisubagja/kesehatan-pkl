<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mymodel extends CI_Model {

	function keamanan(){
		$username = $this->session->userdata('username');
		if(empty($username)){
			$this->session->sess_destroy();
			redirect('Tamu/signin');
		}
	}


	function get_data_jumlah(){
		$query = $this->db->query("select date(tanggal) as tanggal,count(*)as jumlah from laporan_transaksi where date(tanggal)=date(now()) or date(tanggal)=date_sub(curdate(), interval 1 day) or date(tanggal)=date_sub(curdate(), interval 2 day) group by tanggal desc");
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$hasil[] = $data;
			}
			return $hasil;
		}
	}

	public function GetPengguna($where)
	{
		$this->db->select('*');
		$this->db->from('pengguna');
		$this->db->where($where);
		$data=$this->db->get();
		return $data;

	}

	function GetP($id_user)
	{
		$data = array();
		$options = array('id_user' => $id_user, 'username' => $username);
		$Q = $this->db->get_where('pengguna',$options,1);
		if ($Q->num_rows() > 0){
			$data = $Q->row_array();
		}
		$Q->free_result();
		return $data;
	}
	
	public function GetMedis($where="")
	{
		return $this->db->query('select * from medis '.$where);
	}

	public function GetObat()
	{
		return $this->db->query('select * from obat ');
	
	}
	
	public function RegistData($tableName,$data){
		$res = $this->db->insert($tableName,$data);
		return $res;
	}
	
	public function InsertData($tableName,$data){
		$res = $this->db->insert($tableName,$data);
		return $res;
	}
	
	public function UpdateData($tableName,$data,$where){
		$res = $this->db->update($tableName,$data,$where);
		return $res;
	}
	
	public function DeleteData($tableName,$where){
		$res = $this->db->delete($tableName,$where);
		return $res;
	}
	
	//pop upp
	function ubah($data, $id_medis){
		$this->db->where('id_medis',$id_medis);
		$this->db->update('medis', $data);
		return TRUE;
	}
}