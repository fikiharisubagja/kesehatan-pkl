<?php

/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/

/**
* Description of General
*
* @author gieart
*/
class General {

//put your code here
var $ci;

function __construct() {
$this->ci = &get_instance();
$this->ci->load->library('session');
}

function isLoginP() {

if ($this->ci->session->userdata('data') == TRUE) {
return TRUE;
} else {
return FALSE;
}
}

function isLoginMedis() {

if ($this->ci->session->userdata('medis') == TRUE) {
return TRUE;
} else {
return FALSE;
}
}

function isAdmin() {
if ($this->ci->session->userdata('status') == 'admin') {
return TRUE;
} else {
return FALSE;
}
}

function isPasien() {
if ($this->ci->session->userdata('status') == 'user') {
return TRUE;
} else {
return FALSE;
}
}

function isMedis() {
if ($this->ci->session->userdata('status_medis') == 'dokter' ||  $this->ci->session->userdata('status_medis') == 'bidan' || $this->ci->session->userdata('status_medis') == 'perawat') {
return TRUE;
} else {
return FALSE;
}
}

function isAlumni() {
if ($this->ci->session->userdata('type') == 'alumni') {
return TRUE;
} else {
return FALSE;
}
}

function checkAdmin() {
if (($this->isLoginP() && $this->isAdmin()) != TRUE) {
$this->ci->session->set_flashdata('error', '<div style="color: red;">Maaf, Anda tidak memiliki hak akses sebagai admin</div>');
redirect('tamu/signin');
}
}

function checkPasien() {
if (($this->isLoginP() && $this->isPasien()) != TRUE) {
$this->ci->session->set_flashdata('error', 'Maaf, Anda tidak memiliki hak akses sebagai guru');
redirect('tamu/signin');
}
}

function checkMedis() {
if (($this->isLoginMedis() && $this->isMedis()) != TRUE) {
$this->ci->session->set_flashdata('error', 'Maaf, Anda tidak memiliki hak akses sebagai siswa');
redirect('tamu/signin');
}
}

}

?>