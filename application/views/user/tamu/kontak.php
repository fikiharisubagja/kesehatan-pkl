<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('user/layout/head') ?>

<body class="animsition">

  <!-- Header -->
<?php $this->load->view('user/layout/header') ?>  
  <!-- Slide1 -->


  <!-- Banner -->

  <!-- New Product -->

  <!-- Banner2 -->
  


  <!-- Blog -->
<section class="bgwhite p-t-60 p-b-25">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-lg-9 p-b-80">
          <div class="p-r-50 p-r-0-lg">
            <div class="p-b-40">
            <center>
              <?php echo form_open_multipart('Tamu/kritiksaran'); ?>
              <h4 class="m-text25 p-b-14">
                KRITIK DAN SARAN
              </h4>

              <p class="s-text8 p-b-40">
                Tulis kritik & saran anda di bawah ini *
              </p>

              <div class="bo12 of-hidden size19 m-b-20">
                <input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="nama_lengkap" placeholder="Nama Lengkap *">
              </div>

              <div class="bo12 of-hidden size19 m-b-20">
                <input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="email" placeholder="Email *">
              </div>
              
              <textarea class="dis-block s-text7 size18 bo12 p-l-18 p-r-18 p-t-13 m-b-20" name="isi" placeholder="Tulis Kritik & Saran..."></textarea>

              <div class="w-size24">
                <!-- Button -->
                <button class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                  Kirim
                </button>
              </div>
            </form></center>
          </div>
        </div>

        
      </div>
    </div>
  </section>

  <!-- Instagram -->
<section style="background-color: #80CCFF" class="blog bgwhite p-t-94 p-b-65">
    <div class="container">
      <div class="sec-title p-b-52">
        <h3 style="font-family: Cooper Black" class="m-text5 t-center">
          Mengapa Harus Arkamaya Medical ?
        </h3></br>
      </div>

   <div class="row">
        <div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
          <!-- Block3 -->
         <center> <div class="block3">
            <a class="block3-img dis-block hov-img-zoom">
              <img style="width: 100px" src="<?php echo base_url(); ?>assets/front/images/heart.png" alt="IMG-BLOG">
            </a>

           <div class="block3-txt p-t-14">
              <h4 class="p-b-7">
                <a style="font-family: Cooper Black" class="m-text11">
                 Terpercaya
                </a>
              </h4>
            </div>
          </div></center>
        </div>

        <div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
          <!-- Block3 -->
          <center><div class="block3">
            <a class="block3-img dis-block hov-img-zoom">
              <img style="width: 100px" src="<?php echo base_url(); ?>assets/front/images/family.png" alt="IMG-BLOG">
            </a>

            <div class="block3-txt p-t-14">
              <h4 class="p-b-7">
                <a style="font-family: Cooper Black" class="m-text11">
                  Peduli Keluarga
                </a>
              </h4>
            </div>
          </div></center>
        </div>

        <div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
          <!-- Block3 -->
          <center> <div class="block3">
            <a class="block3-img dis-block hov-img-zoom">
              <img style="width: 100px" src="<?php echo base_url(); ?>assets/front/images/ambulance.png" alt="IMG-BLOG">
            </a>

            <div class="block3-txt p-t-14">
              <h4 class="p-b-7">
                <a style="font-family: Cooper Black" class="m-text11">
                  Cepat Tanggap
                </a>
              </h4>
            </div>
          </div></center>
        </div>
      </div>
    </div>
</section>

  <!-- Shipping -->

  <!-- Footer -->
  <footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
    <div class="flex-w p-b-90">
      <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
        <h4 class="s-text12 p-b-30">
          <center>DOKTER</center>
        </h4>

        <div>
          <p class="s-text7 w-size27">
            <center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
          </p>

        </div>
      </div>

      <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
        <h4 class="s-text12 p-b-30">
          <center>BIDAN</center>
        </h4>

        <div>
          <p class="s-text7 w-size27">
            <center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
          </p>

        </div>
      </div>

      <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
        <h4 class="s-text12 p-b-30">
          <center>PERAWAT</center>
        </h4>

        <div>
          <p class="s-text7 w-size27">
            <center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
          </p>

        </div>
      </div>
    </div>

    <div class="t-center p-l-15 p-r-15">

       <div class="t-center s-text8 p-t-20">
        Arkamaya Medical © 2019. All rights reserved. <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="<?php echo base_url(); ?>assets/front/https://colorlib.com" target="_blank">Ricka & Fiki</a>
      </div>
    </div>
  </footer>



  <!-- Back to top -->
  <div class="btn-back-to-top bg0-hov" id="myBtn">
    <span class="symbol-btn-back-to-top">
      <i class="fa fa-angle-double-up" aria-hidden="true"></i>
    </span>
  </div>

  <!-- Container Selection1 -->
  <div id="dropDownSelect1"></div>



<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/popper.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.min.js"></script>
  <script type="text/javascript">
    $(".selection-1").select2({
      minimumResultsForSearch: 20,
      dropdownParent: $('#dropDownSelect1')
    });
  </script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/slick/slick.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/slick-custom.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/sweetalert/sweetalert.min.js"></script>
  <script type="text/javascript">
    $('.block2-btn-addcart').each(function(){
      var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
      $(this).on('click', function(){
        swal(nameProduct, "is added to cart !", "success");
      });
    });

    $('.block2-btn-addwishlist').each(function(){
      var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
      $(this).on('click', function(){
        swal(nameProduct, "is added to wishlist !", "success");
      });
    });
  </script>

<!--===============================================================================================-->
  <script src="<?php echo base_url(); ?>assets/front/js/main.js"></script>

</body>
</html>
