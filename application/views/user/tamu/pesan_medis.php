<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('user/layout/head') ?>

<body class="animsition">

	<!-- Header -->
<?php $this->load->view('user/layout/header') ?><br><br>


  <div class="item-slick1 item3-slick1" style="width:1350px; height: 515px; background-image: url(<?php echo base_url(); ?>assets/front/images/jam.jpg);">
        </div>	

	<!-- Banner -->

  <center> <a style="font-family: Cooper Black"> Layanan Apa Yang Anda Butuhkan ? </a> </center>
	<section class="banner bgwhite p-t-40 p-b-40">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-md-8 col-lg-4 m-l-r-auto">
					<!-- block1 -->
					<div class="block1 hov-img-zoom pos-relative m-b-30">
						<img height="480px" width="720px" src="<?php echo base_url(); ?>assets/front/images/dokter.jpg" alt="IMG-BENNER">

						<div class="block1-wrapbtn w-size2">
							<!-- Button -->
							<a href="<?php echo base_url('index.php/tamu/signin'); ?>" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4">
							Dokter
							</a>
						</div>
					</div>

					<!-- block1 -->

				</div>

				<div class="col-sm-10 col-md-8 col-lg-4 m-l-r-auto">
					<!-- block1 -->

					<!-- block1 -->
					<div class="block1 hov-img-zoom pos-relative m-b-30">
						<img height="480px" width="720px" src="<?php echo base_url(); ?>assets/front/images/bidan.jpg" alt="IMG-BENNER">

						<div class="block1-wrapbtn w-size2">
							<!-- Button -->
							<a href="<?php echo base_url('index.php/tamu/signin'); ?>" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4">
								Bidan
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-10 col-md-8 col-lg-4 m-l-r-auto">
					<!-- block1 -->
					<div class="block1 hov-img-zoom pos-relative m-b-30">
						<img height="480px" width="720px" src="<?php echo base_url(); ?>assets/front/images/perawat.jpg" alt="IMG-BENNER">

						<div class="block1-wrapbtn w-size2">
							<!-- Button -->
							<a href="<?php echo base_url('index.php/tamu/signin'); ?>" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4">
								Perawat
							</a>
						</div>
					</div>

					<!-- block2 -->
				</div>
			</div>
		</div>
	</section>

	<!-- New Product -->


	<!-- Banner2 -->


	<!-- Blog -->
  <section style="background-color: #99CCFF" class="blog bgwhite p-t-94 p-b-65">
    <div class="container">
      <div class="sec-title p-b-52">
        <h3 style="font-family: Cooper Black" class="m-text5 t-center">
          Syarat Menjadi Team Medis <br>Arkamaya Medical
        </h3></br>
      </div>

 <div class="row">
        <div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
          <!-- Block3 -->
         <center> <div class="block3">
            <a class="block3-img dis-block hov-img-zoom">
              <img style="width: 100px" src="<?php echo base_url(); ?>assets/front/images/doctor.png" alt="IMG-BLOG">
            </a>

           <div class="block3-txt p-t-14">
              <h4 class="p-b-7">
                <a class="m-text11">
                 Dokter
                </a>
              </h4>

              <p class="s-text8 p-t-16">
                - Jujur dan tanggung jawab<br>
                - Mempunyai SIP dan STR<br>
                - Mempunyai Smartphone<br>
                - Mempunyai pribadi<br>
              </p>
            </div>
          </div></center>
        </div>

        <div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
          <!-- Block3 -->
          <center><div class="block3">
            <a class="block3-img dis-block hov-img-zoom">
              <img style="width: 100px" src="<?php echo base_url(); ?>assets/front/images/midwife.png" alt="IMG-BLOG">
            </a>

            <div class="block3-txt p-t-14">
              <h4 class="p-b-7">
                <a class="m-text11">
                  Bidan
                </a>
              </h4>
              <p class="s-text8 p-t-16">
                - Jujur dan tanggung jawab<br>
                - Mempunyai SIPB dan STR<br>
                - Mempunyai Smartphone<br>
                - Mempunyai pribadi<br>
              </p>
            </div>
          </div></center>
        </div>

        <div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
          <!-- Block3 -->
          <center> <div class="block3">
            <a class="block3-img dis-block hov-img-zoom">
              <img style="width: 100px" src="<?php echo base_url(); ?>assets/front/images/nurse.png" alt="IMG-BLOG">
            </a>

            <div class="block3-txt p-t-14">
              <h4 class="p-b-7">
                <a class="m-text11">
                  Perawat
                </a>
              </h4>
              <p class="s-text8 p-t-16">
               - Jujur dan tanggung jawab<br>
                - Mempunyai SIPP dan STR<br>
                - Mempunyai Smartphone<br>
                - Mempunyai pribadi<br>
              </p>
            </div>
          </div></center>
        </div>
      </div>
    </div>
  </section>

	<!-- Instagram -->


	<!-- Footer -->
	 <footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
    <div class="flex-w p-b-90">

      <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
        <h4 class="s-text12 p-b-30">
        <center>  DOKTER </center>
        </h4>

        <div>
          <p class="s-text7 w-size27">
            Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879
          </p>
        </div>
      </div>

      <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
        <h4 class="s-text12 p-b-30">
        <center>  BIDAN </center>
        </h4>

        <div>
          <p class="s-text7 w-size27">
            Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879
          </p>
        </div>
      </div>

      <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
        <h4 class="s-text12 p-b-30">
        <center>  PERAWAT </center>
        </h4>

        <div>
          <p class="s-text7 w-size27">
            Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879
          </p>
        </div>
      </div>
    </div>

    <div class="t-center p-l-15 p-r-15">

      <div class="t-center s-text8 p-t-20">
        Arkamaya Medical © 2019. All rights reserved. <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="<?php echo base_url(); ?>assets/front/https://colorlib.com" target="_blank">Ricka & Fiki</a>
      </div>
    </div>
  </footer>



  <!-- Back to top -->
  <div class="btn-back-to-top bg0-hov" id="myBtn">
    <span class="symbol-btn-back-to-top">
      <i class="fa fa-angle-double-up" aria-hidden="true"></i>
    </span>
  </div>

  <!-- Container Selection -->
  <div id="dropDownSelect1"></div>
  <div id="dropDownSelect2"></div>



<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/popper.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.min.js"></script>
  <script type="text/javascript">
    $(".selection-1").select2({
      minimumResultsForSearch: 20,
      dropdownParent: $('#dropDownSelect1')
    });

    $(".selection-2").select2({
      minimumResultsForSearch: 20,
      dropdownParent: $('#dropDownSelect2')
    });
  </script>
<!--===============================================================================================-->
  <script src="<?php echo base_url(); ?>assets/front/js/main.js"></script>

</body>
</html>
