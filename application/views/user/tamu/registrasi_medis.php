<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Medical | Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/back/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/back/bower_components/font-awesome/css/font-awesome.min.css') ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/back/bower_components/Ionicons/css/ionicons.min.css') ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/back/dist/css/AdminLTE.min.css') ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/back/plugins/iCheck/square/blue.css') ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="file:///C|/xampp/htdocs/AdminLTE-master/index2.html"><b>Registrasi Medis</b></a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Daftar menjadi medis</p>

              <?php echo form_open_multipart('Tamu/insert_persyaratan'); ?>
                   
                    <div class="form-group">
                      <label >Nama Lengkap</label>
                      <input type="text" class="form-control" placeholder="Masukan Nama lengkap dan gelar anda"  name="nama_lengkap" value="" required="">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Username</label>
                      <input type="textarea" class="form-control" id="exampleInputEmail1" placeholder="Masukan username anda"  name="username" value="" required="">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Email</label>
                      <input type="textarea" class="form-control" id="exampleInputEmail1" placeholder="Masukan alamat email anda"  name="email" value="" required="">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Password</label>
                      <input type="password" class="form-control" name="password" minlength="8" id="password" placeholder="Masukan Password" data-toggle="password" required="">
                    </div>

                     <div class="form-group">
                      <label for="exampleInputEmail1">No Telephone</label>
                      <input type="textarea" class="form-control" id="exampleInputEmail1" placeholder="Masukan No Telephone"  name="no_hp" value="" required="">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">SIP</label>
                      <input type="file" class="form-control" id="exampleInputEmail1" placeholder="Masukan lulusan"  name="sip" value="" >
                      * Wajib diinput bagi dokter
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">STR</label>
                      <input type="file" class="form-control" id="exampleInputEmail1" placeholder="Masukan lulusan"  name="str" value="" required="">
                      * Wajib diinput
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">STB</label>
                      <input type="file" class="form-control" id="exampleInputEmail1" placeholder="Masukan lulusan"  name="str" value="" required="">
                      * Wajib diinput
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Ijazah</label>
                      <input type="file" class="form-control" id="exampleInputEmail1" placeholder="Masukan lulusan"  name="str" value="" required="">
                      * Wajib diinput
                    </div>

                      <div class="form-group">
                      <label for="exampleInputEmail1">Status Medis</label>
                      <?php $style='class="form-control input-sm"';
                       echo form_dropdown('status_medis',$status_medis,'',$style);

                      ?>
                    </div>

                     <div class="box-footer">
                      <button type="submit" class="btn btn-primary">daftar</button>
                   </div>
                                        
                    </div>


            </form>

     </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url ('assets/back/bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/back/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets/back/plugins/iCheck/icheck.min.js') ?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-show-password/1.0.3/bootstrap-show-password.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });

  $("#password").password('toggle');
</script>
</body>
</html>

