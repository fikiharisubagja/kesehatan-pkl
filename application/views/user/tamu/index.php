<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('user/layout/head') ?>

<body class="animsition">

	<!-- Header -->
<?php $this->load->view('user/layout/header') ?>	
	<!-- Slide1 -->
	<section class="slide1">
		<div class="wrap-slick1">
			<div class="slick1">

				<div class="item-slick1 item2-slick1" style="height: 600px; width: 300px; background-image: url(<?php echo base_url(); ?>assets/front/images/banner-medis1.jpg);">
					<div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">
						<h2 style="color: white; font-family: Cooper Black" class="caption2-slide1 xl-text1 t-center animated visible-false m-b-37" data-appear="lightSpeedIn" >
							Bergabung Sekarang Juga!!
						</h2>

						<div class="wrap-btn-slide1 w-size1 animated visible-false" data-appear="slideInUp">
							<!-- Button -->
							<a href="<?php echo base_url('index.php/Tamu/registrasi_medis'); ?>" class="flex-c-m size2 bo-rad-23 s-text2 bgwhite hov1 trans-0-4">
								Bergabung
							</a>
						</div>
					</div>
				</div>

				<div class="item-slick1 item3-slick1" style="height: 600px; width: 300px; background-image: url(<?php echo base_url(); ?>assets/front/images/banner-medis2.jpg);">
					<div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">

						<h2 style="color: white; font-family: Cooper Black" class="caption2-slide1 xl-text1 t-center animated visible-false m-b-37" data-appear="rotateInUpRight">
							Bergabung Sekarang Juga!
						</h2>

						<div class="wrap-btn-slide1 w-size1 animated visible-false" data-appear="rotateIn">
							<!-- Button -->
							<a href="<?php echo base_url('index.php/Tamu/registrasi_medis'); ?>" class="flex-c-m size2 bo-rad-23 s-text2 bgwhite hov1 trans-0-4">
							 Bergabung
							</a>
						</div>
					</div>
				</div>

				<div class="item-slick1 item3-slick1" style="height: 600px; width: 300px; background-image: url(<?php echo base_url(); ?>assets/front/images/demam-berdarah.jpg);">
					<div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">

						<h2 style="color: white; font-family: Cooper Black" class="caption2-slide1 xl-text1 t-center animated visible-false m-b-37" data-appear="rotateInUpRight">
							Waspada Demam Berdarah! <br>
							Konsultasikan Dengan Dokter Kami Di ARKAMAYA Medical Sekarang Juga.
						</h2>

						<div class="wrap-btn-slide1 w-size1 animated visible-false" data-appear="rotateIn">
							<!-- Button -->
							<a href="<?php echo base_url('index.php/Tamu/registrasi_medis'); ?>" class="flex-c-m size2 bo-rad-23 s-text2 bgwhite hov1 trans-0-4">
							 Bergabung
							</a>
						</div>
					</div>
				</div>

				<div class="item-slick1 item3-slick1" style="height: 600px; width: 300px; background-image: url(<?php echo base_url(); ?>assets/front/images/merawat-lansia.jpg);">
					<div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">

						<h2 style="color: white; font-family: Cooper Black" class="caption2-slide1 xl-text1 t-center animated visible-false m-b-37" data-appear="rotateInUpRight">
							Perawat Lansia <br>
							Melayani Dengan Sabar dan Ramah.
						</h2>

						<div class="wrap-btn-slide1 w-size1 animated visible-false" data-appear="rotateIn">
							<!-- Button -->
							<a href="<?php echo base_url('index.php/Tamu/registrasi_medis'); ?>" class="flex-c-m size2 bo-rad-23 s-text2 bgwhite hov1 trans-0-4">
							 Bergabung
							</a>
						</div>
					</div>
				</div>


				<div class="item-slick1 item3-slick1" style="height: 600px; width: 300px; background-image: url(<?php echo base_url(); ?>assets/front/images/banner-bidan.jpg);">
					<div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">

						<h2 style="color: white; font-family: Cooper Black" class="caption2-slide1 xl-text1 t-center animated visible-false m-b-37" data-appear="rotateInUpRight">
							Bidan <br>
							Melahirkan di rumah tanpa harus datang ke tempat praktik.
						</h2>

						<div class="wrap-btn-slide1 w-size1 animated visible-false" data-appear="rotateIn">
							<!-- Button -->
							<a href="<?php echo base_url('index.php/Tamu/registrasi_medis'); ?>" class="flex-c-m size2 bo-rad-23 s-text2 bgwhite hov1 trans-0-4">
							 Bergabung
							</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>

	<!-- Cara Pesan -->

	<div class="item-slick1 item3-slick1" class="item-slick1 item3-slick1" style="width:1349px; height: 700px; background-image: url(<?php echo base_url(); ?>assets/front/images/peta.jpg);">

				</div><br><br><br><br>

	<div class="item-slick1 item3-slick1" style="width:1000px; height: 650px; background-image: url(<?php echo base_url(); ?>assets/front/images/pesan.jpg);">
	</div>

	<!-- New Product -->
	<br><br><br><br><br><section class="newproduct bgwhite p-t-45 p-b-105" style="background-color: #99CCFF">
		<div class="container">
			<div class="sec-title p-b-60">
				<h3 style="font-family: Cooper Black" class="m-text5 t-center">
					Review
				</h3>
			</div>

			<!-- Slide2 -->
			<div class="wrap-slick2">
				<div class="slick2">

					<div class="item-slick2 p-l-15 p-r-15">
						<!-- Kegiatan Dokter -->
						<div class="block2">
							<center><div class="block2-img wrap-pic-w of-hidden pos-relative">
								<img style="width: 200px" src="<?php echo base_url(); ?>assets/front/images/rukmayah.png" alt="IMG-PRODUCT">
							</div></center>

							<div style="font-color: #" class="block2-txt p-t-20">
								<center><a href="<?php echo base_url(); ?>assets/front/product-detail.html" class="block2-name dis-block s-text3 p-b-5">
									Rukmayah, 62 tahun.
								</a><i><a style="font-size: 70%"> Ibu Rumah Tangga</i></a></center>

								<span style="font-family: cursive" class="block2-name dis-block s-text3 p-b-5">
									<i>"Aplikasi Arkamaya Medical sangat membantu saya menjalani pengobatan selama di rumah."</i>
								</span>
							</div>
						</div>
					</div>

					<div class="item-slick2 p-l-15 p-r-15">
						<!-- Kegiatan Perawat -->
						<div class="block2">
							<center><div class="block2-img wrap-pic-w of-hidden pos-relative">
								<img style="width: 200px" src="<?php echo base_url(); ?>assets/front/images/girl.png" alt="IMG-PRODUCT">
							</div></center>

							<div class="block2-txt p-t-20">
								<center><a href="<?php echo base_url(); ?>assets/front/product-detail.html" class="block2-name dis-block s-text3 p-b-5">
									Stephanie Poetri, 21 tahun.
								</a><i><a style="font-size: 70%"> Mahasiswa</i></a></center>

								<span style="font-family: cursive" class="block2-name dis-block s-text3 p-b-5">
									<i>"Dokter Arkamaya Medical sangat ramah dan aplikasi ini sangat membantu karena kita tidak perlu datang ke rumah sakit atau klinik. Terimkasih Arkamaya Medical"</i>
								</span>
							</div>
						</div>
					</div>

					<div class="item-slick2 p-l-15 p-r-15">
						<!-- Block2 -->
						<div class="block2">
							<center><div class="block2-img wrap-pic-w of-hidden pos-relative">
								<img style="width: 200px" src="<?php echo base_url(); ?>assets/front/images/ajun.png" alt="IMG-PRODUCT">
							</div></center>

							<div class="block2-txt p-t-20">
								<center><a href="<?php echo base_url(); ?>assets/front/product-detail.html" class="block2-name dis-block s-text3 p-b-5">
									Ajun Perwira, 26 tahun.
								</a><i><a style="font-size: 70%"> Sutradara</i></a></center>

								<span style="font-family: cursive" class="block2-name dis-block s-text3 p-b-5">
									<i>"Dokter Arkamaya Medical sangat ramah dan aplikasi ini sangat membantu karena kita tidak perlu datang ke rumah sakit atau klinik. Terimkasih Arkamaya Medical"</i>
								</span>
							</div>
						</div>
					</div>

					<div class="item-slick2 p-l-15 p-r-15">
						<!-- Block2 -->
						<div class="block2">
							<center><div class="block2-img wrap-pic-w of-hidden pos-relative">
								<img style="width: 200px" src="<?php echo base_url(); ?>assets/front/images/zulfa.png" alt="IMG-PRODUCT">

							</div></center>

							<div class="block2-txt p-t-20">
								<center><a href="<?php echo base_url(); ?>assets/front/product-detail.html" class="block2-name dis-block s-text3 p-b-5">
									Zulfa Maharani, 24 tahun.
								</a><i><a style="font-size: 70%"> Designer</i></a></center>

								<span style="font-family: cursive" class="block2-name dis-block s-text3 p-b-5">
									<i>"Dokter Arkamaya Medical sangat ramah dan aplikasi ini sangat membantu karena kita tidak perlu datang ke rumah sakit atau klinik. Terimkasih Arkamaya Medical"</i>
								</span>
							</div>
						</div>
					</div>

					<div class="item-slick2 p-l-15 p-r-15">
						<!-- Block2 -->
						<div class="block2">
							<center><div class="block2-img wrap-pic-w of-hidden pos-relative">
								<img style="width: 200px" src="<?php echo base_url(); ?>assets/front/images/arbani.png" alt="IMG-PRODUCT">
							</div></center>

							<div class="block2-txt p-t-20">
								<center><a href="<?php echo base_url(); ?>assets/front/product-detail.html" class="block2-name dis-block s-text3 p-b-5">
									Arbani Yasiz, 24 tahun.
								</a><i><a style="font-size: 70%"> Aktor</i></a></center>


								<span style="font-family: cursive" class="block2-name dis-block s-text3 p-b-5">
									<i>"Dokter Arkamaya Medical sangat ramah dan aplikasi ini sangat membantu karena kita tidak perlu datang ke rumah sakit atau klinik. Terimkasih Arkamaya Medical"</i>
								</span>
							</div>
						</div>
					</div>

					<div class="item-slick2 p-l-15 p-r-15">
						<!-- Block2 -->
						<div class="block2">
							<center><div class="block2-img wrap-pic-w of-hidden pos-relative">
								<img style="width: 200px" src="<?php echo base_url(); ?>assets/front/images/Gritte.png" alt="IMG-PRODUCT">
							</div></center>

							<div class="block2-txt p-t-20">
								<center><a href="<?php echo base_url(); ?>assets/front/product-detail.html" class="block2-name dis-block s-text3 p-b-5">
									Gritte Agatha, 23 tahun.
								</a><i><a style="font-size: 70%"> Penyiar Radio</i></a></center>

								<span style="font-family: cursive" class="block2-name dis-block s-text3 p-b-5">
									<i>"Dokter Arkamaya Medical sangat ramah dan aplikasi ini sangat membantu karena kita tidak perlu datang ke rumah sakit atau klinik. Terimkasih Arkamaya Medical"</i>
								</span>
							</div>
						</div>
					</div>

					<div class="item-slick2 p-l-15 p-r-15">
						<!-- Block2 -->
						<div class="block2">
							<center><div class="block2-img wrap-pic-w of-hidden pos-relative">
								<img style="width: 200px" src="<?php echo base_url(); ?>assets/front/images/dimas.png" alt="IMG-PRODUCT">

							</div></center>

							<div class="block2-txt p-t-20">
								<center><a href="<?php echo base_url(); ?>assets/front/product-detail.html" class="block2-name dis-block s-text3 p-b-5">
									Dimas Aditya, 30 tahun.
								</a><i><a style="font-size: 70%"> Pengusaha</i></a></center>

								<span style="font-family: cursive" class="block2-name dis-block s-text3 p-b-5">
									<i>"Dokter Arkamaya Medical sangat ramah dan aplikasi ini sangat membantu karena kita tidak perlu datang ke rumah sakit atau klinik. Terimkasih Arkamaya Medical"</i>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>

	<!-- Banner2 -->
	


	<!-- Blog -->
	<section class="blog bgwhite p-t-94 p-b-65">
		<div class="container">
			<div class="sec-title p-b-52">
				<h3 style="font-family: Cooper Black" class="m-text5 t-center">
					Blog Arkamaya Medical
				</h3>
			</div>

			<div class="row">
				<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
					<!-- Block3 -->
					<div class="block3">
						<a href="<?php echo base_url(); ?>assets/front/blog-detail.html" class="block3-img dis-block hov-img-zoom">
							<img src="<?php echo base_url(); ?>assets/front/images/review-lansia.jpg" alt="IMG-BLOG">
						</a>

						<div class="block3-txt p-t-14">
							<span class="s-text6">Perawat</span>
							<br>
							<span class="s-text6">31 Desember 2018</span>
							<h4 class="p-b-7">
								<a href="<?php echo base_url('index.php/Tamu/lansia'); ?>" class="m-text11">
									Kegiatan Lansia Yang Menyehatkan
								</a>
							</h4>

							<p class="s-text8 p-t-16">
								Seiring bertambahnya usia, masyarakat lanjut usia alias lansia bisa mengalami penurunan fungsi kognitif, jika tidak diimbangi dengan gaya hidup yang sehat.
							</p>
						</div>
					</div>
				</div>

				<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
					<!-- Block3 -->
					<div class="block3">
						<a href="<?php echo base_url(); ?>assets/front/blog-detail.html" class="block3-img dis-block hov-img-zoom">
							<img height=280px src="<?php echo base_url(); ?>assets/front/images/demam-berdarah2.jpg" alt="IMG-BLOG">
						</a>

						<div class="block3-txt p-t-14">
							<span class="s-text6">Dokter</span> <br> <span class="s-text6">2 Januari 2019</span>
							<h4 class="p-b-7">
								<a href="<?php echo base_url('index.php/Tamu/nyamuk'); ?>" class="m-text11">
									Waspada gejala demam berdarah dan cara pencegahannya
								</a>
							</h4>
							<p class="s-text8 p-t-16">
								Demam berdarah ringan menyebabkan demam tinggi, ruam, dan nyeri otot dan sendi. Demam berdarah yang parah, atau juga dikenal sebagai dengue hemorrhagic fever, dapat menyebabkan perdarahan serius, penurunan tekanan darah yang tiba-tiba (shock), dan kematian.
							</p>
						</div>
					</div>
				</div>

				<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
					<!-- Block3 -->
					<div class="block3">
						<a href="<?php echo base_url(); ?>assets/front/blog-detail.html" class="block3-img dis-block hov-img-zoom">
							<img height=280px src="<?php echo base_url(); ?>assets/front/images/melahirkan.jpg" alt="IMG-BLOG">
						</a>

						<div class="block3-txt p-t-14">
							<span class="s-text6">Bidan</span> <br>
							<span class="s-text6">5 Januari 2019</span>
							<h4 class="p-b-7">
								<a href="<?php echo base_url('index.php/Tamu/melahirkan'); ?>" class="m-text11">
									Tips melahirkan lancar bagi ibu hamil
								</a>
							</h4>

							

							<p class="s-text8 p-t-16">
								Kekhawatiran menghadapi persalinan wajar dirasakan oleh ibu yang sedang mengandung anak pertama, dan tak jarang pula ibu yang sudah pernah melahirkan juga merasakannya. Dilansir dari situs parents.com, berikut ini 11 rahasia yang bisa membantu anda menjalani persalinan dengan lebih mudah dan lebih cepat.
							</p>
						</div>
					</div>
				</div>

				<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
					<!-- Block3 -->
					<div class="block3">
						<a href="<?php echo base_url('index.php/Tamu/hidupsehat'); ?>" class="block3-img dis-block hov-img-zoom">
							<img height=280px src="<?php echo base_url(); ?>assets/front/images/hidupsehat.jpg" alt="IMG-BLOG">
						</a>

						<div class="block3-txt p-t-14">
							<span class="s-text6">Dokter</span> <br>
							<span class="s-text6">5 Januari 2019</span>
							<h4 class="p-b-7">
								<a href="<?php echo base_url('index.php/Tamu/hidupsehat'); ?>" class="m-text11">
									8 Tips Badan Sehat dan Bugar
								</a>
							</h4>
							<p class="s-text8 p-t-16">
								Menjalani aktivitas kegiatan kerja sehari-hari seringkali membuat badan jadi gampang mudah capek dan rentan sakit. Kesibukan kerja yang padat sering menimbulkan kelelahan fisik dan membuat pikiran mudah cepat stress.
							</p>
						</div>
					</div>
				</div>

				<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
					<!-- Block3 -->
					<div class="block3">
						<a href="<?php echo base_url('index.php/Tamu/bayibatuk'); ?>" class="block3-img dis-block hov-img-zoom">
							<img height=280px src="<?php echo base_url(); ?>assets/front/images/bayibatuk.jpg" alt="IMG-BLOG">
						</a>

						<div class="block3-txt p-t-14">
							<span class="s-text6">Bidan</span> <br>
							<span class="s-text6">5 Januari 2019</span>
							<h4 class="p-b-7">
								<a href="<?php echo base_url('index.php/Tamu/bayibatuk'); ?>" class="m-text11">
									7 Cara Mengatasi Batuk Pada Bayi
								</a>
							</h4>

							

							<p class="s-text8 p-t-16">Ketika orang dewasa terkena batuk, langkah pertama yang dilakukan adalah mencari toko obat terdekat untuk membeli obat batuk. Ketika batuk dialami oleh bayi Anda, mungkin Anda tidak bisa langsung memberinya obat. Lalu, apa yang harus dilakukan?
							</p>
						</div>
					</div>
				</div>

				

				<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
					<!-- Block3 -->
					<div class="block3">
						<a href="<?php echo base_url('index.php/Tamu/makansehat'); ?>" class="block3-img dis-block hov-img-zoom">
							<img height=280px src="<?php echo base_url(); ?>assets/front/images/buah.jpg" alt="IMG-BLOG">
						</a>

						<div class="block3-txt p-t-14">
							<span class="s-text6">Bidan</span> <br>
							<span class="s-text6">5 Januari 2019</span>
							<h4 class="p-b-7">
								<a href="<?php echo base_url('index.php/Tamu/makansehat'); ?>" class="m-text11">
									Ini Makanan Sehat yang Perlu Dikonsumsi Setiap Hari
								</a>
							</h4>
                            <p class="s-text8 p-t-16">
								Makanan sehat sering dianggap tidak enak, padahal banyak jenis makanan sehat yang terasa lezat, namun kerap terlupakan. Mengonsumsi makanan sehat memiliki banyak manfaat, termasuk melindungi diri dari ancaman penyakit kronis.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Instagram -->
<section style="background-color: #80CCFF" class="blog bgwhite p-t-94 p-b-65">
    <div class="container">
      <div class="sec-title p-b-52">
        <h3 style="font-family: Cooper Black" class="m-text5 t-center">
          Mengapa Harus Arkamaya Medical ?
        </h3></br>
      </div>

	 <div class="row">
        <div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
          <!-- Block3 -->
         <center> <div class="block3">
            <a class="block3-img dis-block hov-img-zoom">
              <img style="width: 100px" src="<?php echo base_url(); ?>assets/front/images/heart.png" alt="IMG-BLOG">
            </a>

           <div class="block3-txt p-t-14">
              <h4 class="p-b-7">
                <a style="font-family: Cooper Black" class="m-text11">
                 Terpercaya
                </a>
              </h4>
            </div>
          </div></center>
        </div>

        <div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
          <!-- Block3 -->
          <center><div class="block3">
            <a class="block3-img dis-block hov-img-zoom">
              <img style="width: 100px" src="<?php echo base_url(); ?>assets/front/images/family.png" alt="IMG-BLOG">
            </a>

            <div class="block3-txt p-t-14">
              <h4 class="p-b-7">
                <a style="font-family: Cooper Black" class="m-text11">
                  Peduli Keluarga
                </a>
              </h4>
            </div>
          </div></center>
        </div>

        <div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
          <!-- Block3 -->
          <center> <div class="block3">
            <a class="block3-img dis-block hov-img-zoom">
              <img style="width: 100px" src="<?php echo base_url(); ?>assets/front/images/ambulance.png" alt="IMG-BLOG">
            </a>

            <div class="block3-txt p-t-14">
              <h4 class="p-b-7">
                <a style="font-family: Cooper Black" class="m-text11">
                  Cepat Tanggap
                </a>
              </h4>
            </div>
          </div></center>
        </div>
      </div>
    </div>
</section>

	<!-- Shipping -->

	<!-- Footer -->
	<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
		<div class="flex-w p-b-90">
			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<center>DOKTER</center>
				</h4>

				<div>
					<p class="s-text7 w-size27">
						<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
					</p>

				</div>
			</div>

			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<center>BIDAN</center>
				</h4>

				<div>
					<p class="s-text7 w-size27">
						<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
					</p>

				</div>
			</div>

			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<center>PERAWAT</center>
				</h4>

				<div>
					<p class="s-text7 w-size27">
						<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
					</p>

				</div>
			</div>
		</div>

		<div class="t-center p-l-15 p-r-15">

			 <div class="t-center s-text8 p-t-20">
        Arkamaya Medical © 2019. All rights reserved. <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="<?php echo base_url(); ?>assets/front/https://colorlib.com" target="_blank">Ricka & Fiki</a>
      </div>
    </div>
	</footer>



	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection1 -->
	<div id="dropDownSelect1"></div>



<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.min.js"></script>
	<script type="text/javascript">
		$(".selection-1").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/slick/slick.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/slick-custom.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/sweetalert/sweetalert.min.js"></script>
	<script type="text/javascript">
		$('.block2-btn-addcart').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to cart !", "success");
			});
		});

		$('.block2-btn-addwishlist').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");
			});
		});
	</script>

<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>assets/front/js/main.js"></script>

</body>
</html>
