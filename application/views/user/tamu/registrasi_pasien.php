<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Medical | Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/back/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/back/bower_components/font-awesome/css/font-awesome.min.css') ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/back/bower_components/Ionicons/css/ionicons.min.css') ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/back/dist/css/AdminLTE.min.css') ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url ('assets/back/plugins/iCheck/square/blue.css') ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="file:///C|/xampp/htdocs/AdminLTE-master/index2.html"><b>ARKAMAYA Medical</b></a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Daftar Menjadi Pasien</p>

              <?php echo form_open_multipart('Tamu/do_registration'); ?>
                   
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Lengkap</label>
                      <input type="text" class="form-control" placeholder="Masukan Nama Lengkap Anda"  name="nama_lengkap" value="" >
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Username</label>
                      <input type="textarea" class="form-control" placeholder="Masukan Username Anda"  name="username" value="" required="">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Password</label>
                      <input type="password" class="form-control" id="password" minlength="8" placeholder="Masukan password anda" data-toggle="password" name="password" value="" required="">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Email</label>
                      <input type="textarea" class="form-control" placeholder="Masukan Alamat Email Anda"  name="email" value="" required="">
                    </div>                 

                    <div class="form-group">
                      <label for="exampleInputEmail1">No Telephone</label>
                      <input type="text" class="form-control" placeholder="Masukan No Telephone Anda"  name="no_hp" value="" >
                    </div>

                     <div class="box-footer">
                      <button type="submit" class="btn btn-primary">daftar</button>
                   </div>
                                        
                    </div>

            
            <!-- <div class="col-sm-12 col-md-6 col-lg-5 p-b-50" style="background-color: pink">

            <div class="form-group">
                      <label for="exampleInputEmail1">Masukan password</label>
                      <input type="password" class="form-control" id="exampleInputEmail1" placeholder="Masukan bagian medis"  name="password" value="" required="">
                    </div>
            <div class="box-footer">
                      <button type="submit" class="btn btn-primary">daftar</button>
                   </div>

            </div> -->

            </form>


<!--       <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Full name" name="username" required="">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" name="email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password" required="">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> I agree to the <a href="#">terms</a>
            </label>
          </div>
        </div> -->
        <!-- /.col -->
        <!-- <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
         --><!-- /.col -->
      <!-- </div>
    </form> -->


     </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url ('assets/back/bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/back/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets/back/plugins/iCheck/icheck.min.js') ?>"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });

  $('#password, #confirm_password').on('keyup', function () {
  if ($('#password').val() == $('#confirm_password').val()) {
    $('#message').html('Password sama').css('color', 'green');
  } else 
    $('#message').html('Password Tidak Sama').css('color', 'red');
});
</script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-show-password/1.0.3/bootstrap-show-password.min.js"></script>
</body>
</html>

