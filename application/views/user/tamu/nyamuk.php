<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('user/layout/head') ?>

<body class="animsition">

	<!-- Header -->
<?php $this->load->view('user/layout/header') ?>	
	<!-- Slide1 -->


	<!-- Banner -->

	<!-- New Product -->

	<!-- Banner2 -->
	


	<!-- Blog -->
<section class="bgwhite p-t-60 p-b-25">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-lg-9 p-b-80">
					<div class="p-r-50 p-r-0-lg">
						<div class="p-b-40">
							<div class="blog-detail-img wrap-pic-w">
								<img style="height:500px; width:1200px" src="<?php echo base_url(); ?>assets/front/images/demam-berdarah2.jpg" alt="IMG-BLOG">
							</div>

							<div class="blog-detail-txt p-t-33">
								<h4 class="p-b-11 m-text24">
									Waspada gejala demam berdarah dan cara pencegahannya
								</h4>

								<div class="s-text8 flex-w flex-m p-b-21">
									<span>
										By Admin
										<span class="m-l-3 m-r-6">|</span>
									</span>

									<span>
										28 Dec, 2018
										<span class="m-l-3 m-r-6">|</span>
									</span>

									<span>
										Cooking, Food
										<span class="m-l-3 m-r-6">|</span>
									</span>

									<span>
										8 Comments
									</span>
								</div>

								<p class="p-b-25">
									<b>Apa itu demam berdarah dengue (DBD)?</b></p>

<p class="p-b-25">								
Demam berdarah dengue (DBD) adalah penyakit menular yang disebabkan oleh virus dengue yang dibawa oleh nyamuk. Demam berdarah DBD dulu disebut penyakit “break-bone” karena kadang menyebabkan nyeri sendi dan otot di mana tulang terasa retak.

Demam berdarah ringan menyebabkan demam tinggi, ruam, dan nyeri otot dan sendi. Demam berdarah yang parah, atau juga dikenal sebagai dengue hemorrhagic fever, dapat menyebabkan perdarahan serius, penurunan tekanan darah yang tiba-tiba (shock), dan kematian.</p>

<p class="p-b-25">
<b>Seberapa umumkah demam berdarah dengue?</b></p>

<p class="p-b-25">
Jutaan kasus infeksi DBD terjadi setiap tahunnya di seluruh dunia. Kondisi ini dapat terjadi pada pasien dengan usia berapapun. Demam berdarah dengue paling banyak ditemui selama musim hujan dan setelah musim hujan di area tropis dan subtropis di:</p>
<p class="p-b-25">
- Afrika
</p>
<p class="p-b-25">
- Asia Tenggara dan China
</p>
<p class="p-b-25">
- India
</p>
<p class="p-b-25">
- Timur Tengah
</p>
<p class="p-b-25">
- Karibia
</p>
<p class="p-b-25">
- Amerika Tengah dan Amerika Selatan</p>
<p class="p-b-25">
- Australia Pasifik Selatan dan Pasifik Tengah.</p>
<p class="p-b-25">

<p class="p-b-25">
<b>Apa saja tanda-tanda dan gejala demam berdarah dengue (DBD)?</b></p>
<p class="p-b-25">
Terdapat tiga jenis demam dengue: demam berdarah klasik, dengue hemorrhagic fever, dan dengue shock syndrome. Masing-masing memiliki gejala yang berbeda.</p>

<p class="p-b-25">
<b>Gejala demam berdarah klasik</b></p>

<p class="p-b-25">
Gejala dari demam berdarah klasik biasanya diawali dengan demam selama 4 hingga 7 hari setelah digigit oleh nyamuk yang terinfeksi, serta:</p>

<p class="p-b-25">

Demam tinggi, hingga 40 derajat C</p>
<p class="p-b-25">
- Sakit kepala parah</p>
<p class="p-b-25">
- Nyeri pada retro-orbital (bagian belakang mata)</p>
<p class="p-b-25">
- Nyeri otot dan sendi parah</p>
<p class="p-b-25">
- Mual dan muntah</p>
<p class="p-b-25">
- Ruam</p>

<p class="p-b-25">
Ruam mungkin muncul di seluruh tubuh 3 sampai 4 hari setelah demam, kemudian berkurang setelah 1 hingga 2 hari. Anda mungkin mengalami ruam kedua beberapa hari kemudian.</p>

<p class="p-b-25">
<b>Gejala dengue hemorrhagic fever</b></p>

<p class="p-b-25">
Gejala dari dengue hemorrhagic fever meliputi semua gejala dari demam berdarah klasik, ditambah:</p>

<p class="p-b-25">
- Kerusakan pada pembuluh darah dan getah bening</p>
<p class="p-b-25">
- Pendarahan dari hidung, gusi, atau di bawah kulit, menyebabkan memar berwarna keunguan
Jenis penyakit dengue ini dapat menyebabkan kematian.</p>
<p class="p-b-25">
Gejala dengue shock syndrome</p>
<p class="p-b-25">
Gejala dari dengue shock syndrome, jenis penyakit dengue yang paling parah, meliputi semua gejala demam berdarah klasik dan dengue hemorrhagic fever, ditambah:</p>
<p class="p-b-25">
- Kebocoran di luar pembuluh darah</p>
<p class="p-b-25">- Perdarahan parah</p>
<p class="p-b-25">- Shock (tekanan darah sangat rendah)</p>
<p class="p-b-25">
Jenis penyakit ini biasanya terjadi pada anak-anak (dan beberapa orang dewasa) yang mengalami infeksi dengue kedua kalinya. Jenis penyakit ini sering kali fatal, terutama pada anak-anak dan dewasa muda.

Kemungkinan ada tanda-tanda dan gejala yang tidak disebutkan di atas. Bila Anda memiliki kekhawatiran akan sebuah gejala tertentu, konsultasikanlah dengan dokter Anda.

Kapan saya harus periksa ke dokter?
Jika Anda memiliki tanda-tanda atau gejala-gejala di atas atau pertanyaan lainnya, konsultasikanlah dengan dokter Anda. Tubuh masing-masing orang berbeda. Selalu konsultasikan ke dokter untuk menangani kondisi kesehatan Anda.</p>

<p class="p-b-25">
	Penyebab
Apa penyebab demam berdarah dengue?
Demam berdarah DBD disebabkan oleh virus yang disebarkan oleh gigitan nyamuk. Terdapat 4 virus dengue, yaitu virus DEN-1, DEN-2, DEN-3 dan DEN-4. Nyamuk yang berasal dari famili tertentu yaitu Aedes aegypti atau Aedes albopictus dapat membawa virus untuk menginfeksi darah manusia dengan gigitan dan mentransfer darah yang terinfeksi ke orang lain. Begitu Anda pulih dari demam berdarah, imunitas Anda akan terbentuk namun hanya sampai strain tertentu.</p>

<p class="p-b-25">

Terdapat 4 strain virus tertentu, yang berarti Anda dapat terinfeksi lagi. Penting untuk mengidentifikasi tanda-tanda dan mendapatkan penanganan.</p>
<p class="p-b-25">
<b>Siapa yang berisiko terkena demam berdarah dengue (DBD)?</b></p>
<p class="p-b-25">
Ada banyak faktor yang meningkatkan risiko Anda terkena demam berdarah DBD, yaitu:</p>
<p class="p-b-25">

Tinggal atau berpergian ke area tropis. Berada di daerah tropis dan subtropis meningkatkan risiko terkenanya virus yang menyebabkan demam berdarah. Daerah yang berisiko tinggi adalah Asia Tenggara, bagian barat Kepulauan Pasifik, Amerika Latin, dan Karibia.
Infeksi sebelumnya dengan virus demam dengue meningkatkan risiko gejala yang serius jika Anda terinfeksi kembali.</p>
<p class="p-b-25">
Obat & Pengobatan</p>
<p class="p-b-25">
Informasi yang diberikan bukanlah pengganti nasihat medis. SELALU konsultasikan pada dokter Anda.</p>
<p class="p-b-25">
<b>Bagaimana mendiagnosis demam berdarah dengue?</b></p>
<p class="p-b-25">
Mendiagnosis demam berdarah mungkin sulit dilakukan, karena tanda-tanda dan gejalanya sulit dibedakan dengan penyakit lain seperti malaria, leptospirosis, dan tifus.

Beberapa tes laboratorium dapat mendeteksi bukti virus dengue, namun hasil tes biasanya keluar agak lama untuk segera memberi keputusan pengobatan.</p>

<p class="p-b-25">
Bagaimana cara mengobati demam berdarah dengue?</p>
<p class="p-b-25">
Tidak ada penanganan spesifik untuk demam berdarah DBD, kebanyakan pasien pulih dalam 2 minggu. Penting untuk menangani gejala-gejala untuk menghindari komplikasi. Dokter biasanya merekomendasikan pilihan pengobatan berikut:</p>

<p class="p-b-25">
- Istirahat yang banyak di tempat tidur</p>
<p class="p-b-25">
- Minum banyak cairan</p>
<p class="p-b-25">
- Minum obat untuk menurunkan demam. Paracetamol (Tylenol®, Panadol®) dapat meringankan rasa sakit dan menurunkan demam.</p>
<p class="p-b-25">
- Hindari penghilang rasa sakit yang dapat meningkatkan komplikasi perdarahan, seperti aspirin, ibuprofen (Advil®, Motrin®) dan naproxen sodium (Aleve®).</p>
<p class="p-b-25">
- Untuk kasus yang lebih serius, demam berdarah dapat menyebabkan shock atau hemorrhagic fever yang memerlukan perhatian medis lebih.</p>
<p class="p-b-25">
<b>Apa saja perubahan gaya hidup yang dapat dilakukan untuk mencegah dan mengatasi demam berdarah dengue?</b></p>
<p class="p-b-25">
Anda dapat mengatasi demam berdarah dengue dengan perawatan di rumah. Anda memerlukan hidrasi serta penanganan rasa sakit yang baik. Berikut adalah gaya hidup dan pengobatan rumahan yang dapat membantu Anda:</p>

<p class="p-b-25">

- Tinggallah di tempat yang ber-AC. Penting untuk menjaga rumah dari nyamuk terutama pada malam hari.</p>
<p class="p-b-25">
-Atur ulang kegiatan di luar ruangan. Hindari berada di luar ruangan pada dini hari, senja, atau malam hari, di mana banyak nyamuk di luar.
Gunakan pakaian pelindung. Apabila Anda berada di daerah yang banyak nyamuk, gunakan baju berlengan panjang, celana panjang, kaus kaki, dan sepatu.</p>
<p class="p-b-25">
- Gunakan penangkal nyamuk. Permethrin dapat dipakaikan ke pakaian, sepatu, alat kemah Anda. Anda juga dapat membeli pakaian yang mengandung permethin. Untuk kulit Anda, gunakan penangkal yang mengandung paling sedikit 10% konsentrasi DEET.</p>
<p class="p-b-25">
- Kurangi tempat tinggal nyamuk. Nyamuk yang membawa virus dengue biasanya tinggal di dalam dan sekitar perumahan, berkembang biak di genangan air, seperti ban mobil. Kurangi habitat perkembangbiakan nyamuk untuk mengurangi populasi nyamuk.
								</p>
							</div>

							
						</div>

						<!-- Leave a comment -->
						<form class="leave-comment">
							<h4 class="m-text25 p-b-14">
								Leave a Comment
							</h4>

							<p class="s-text8 p-b-40">
								Your email address will not be published. Required fields are marked *
							</p>

							<textarea class="dis-block s-text7 size18 bo12 p-l-18 p-r-18 p-t-13 m-b-20" name="comment" placeholder="Comment..."></textarea>

							<div class="bo12 of-hidden size19 m-b-20">
								<input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="name" placeholder="Name *">
							</div>

							<div class="bo12 of-hidden size19 m-b-20">
								<input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="email" placeholder="Email *">
							</div>

							<div class="bo12 of-hidden size19 m-b-30">
								<input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="website" placeholder="Website">
							</div>

							<div class="w-size24">
								<!-- Button -->
								<button class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
									Post Comment
								</button>
							</div>
						</form>
					</div>
				</div>

				
			</div>
		</div>
	</section>

	<!-- Instagram -->
	<section class="instagram p-t-20">
		<div class="sec-title p-b-52 p-l-15 p-r-15">
			
		</div>

		<div class="flex-w">
			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-03.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-07.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-09.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-13.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-15.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>
		</div>
	</section>

	<!-- Shipping -->

	<!-- Footer -->
	<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
		<div class="flex-w p-b-90">
			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<center>DOKTER</center>
				</h4>

				<div>
					<p class="s-text7 w-size27">
						<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
					</p>

				</div>
			</div>

			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<center>BIDAN</center>
				</h4>

				<div>
					<p class="s-text7 w-size27">
						<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
					</p>

				</div>
			</div>

			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<center>PERAWAT</center>
				</h4>

				<div>
					<p class="s-text7 w-size27">
						<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
					</p>

				</div>
			</div>
		</div>

		<div class="t-center p-l-15 p-r-15">

			 <div class="t-center s-text8 p-t-20">
        Arkamaya Medical © 2019. All rights reserved. <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="<?php echo base_url(); ?>assets/front/https://colorlib.com" target="_blank">Ricka & Fiki</a>
      </div>
    </div>
	</footer>



	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection1 -->
	<div id="dropDownSelect1"></div>



<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.min.js"></script>
	<script type="text/javascript">
		$(".selection-1").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/slick/slick.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/slick-custom.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/sweetalert/sweetalert.min.js"></script>
	<script type="text/javascript">
		$('.block2-btn-addcart').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to cart !", "success");
			});
		});

		$('.block2-btn-addwishlist').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");
			});
		});
	</script>

<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>assets/front/js/main.js"></script>

</body>
</html>
