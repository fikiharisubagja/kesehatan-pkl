<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('user/layout/head') ?>

<body class="animsition">

	<!-- Header -->
<?php $this->load->view('user/layout/header') ?>	
	<!-- Slide1 -->


	<!-- Banner -->

	<!-- New Product -->

	<!-- Banner2 -->
	


	<!-- Blog -->
<section class="bgwhite p-t-60 p-b-25">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-lg-9 p-b-80">
					<div class="p-r-50 p-r-0-lg">
						<div class="p-b-40">
							<div class="blog-detail-img wrap-pic-w">
								<img style="height:500px; width:1200px" src="<?php echo base_url(); ?>assets/front/images/review-lansia.jpg" alt="IMG-BLOG">
							</div>

							<div class="blog-detail-txt p-t-33">
								<h4 class="p-b-11 m-text24">
									Kegiatan Lansia Yang Menyehatkan
								</h4>

								<div class="s-text8 flex-w flex-m p-b-21">
									<span>
										By Admin
										<span class="m-l-3 m-r-6">|</span>
									</span>

									<span>
										28 Dec, 2018
										<span class="m-l-3 m-r-6">|</span>
									</span>

									<span>
										Cooking, Food
										<span class="m-l-3 m-r-6">|</span>
									</span>

									<span>
										8 Comments
									</span>
								</div>

								<p class="p-b-25">
									Seiring bertambahnya usia, masyarakat lanjut usia alias lansia bisa mengalami penurunan fungsi kognitif, jika tidak diimbangi dengan gaya hidup yang sehat.

Dokter Nugroho Abikusno dari InResAge Universitas Trisakti, yang juga tergabung dalam Perkumpulan Asuhan Demensia Indonesia, menyatakan lansia perlu didukung oleh lingkungan yang positif agar dia tetap produktif.

"Kalau tidak produktif, bisa jadi pelupa," kata dia saat ditemui di sebuah diskusi tentang demensia di Universitas Atma Jaya Pluit, Kamis 19 April 2018.

Demensia dapat berdampak besar pada kualitas hidup lansia. Untuk mencegahnya, Nugroho memberikan sejumlah saran. Selain dibutuhkan makanan yang sehat, untuk mengurangi risiko demensia, perlu juga melakukan berbagai rutinitas yang baik.</p>

<p class="p-b-25">
<b>1. Olahraga</b></p>

<p class="p-b-25">
Buat aktivitas olah raga menjadi sesuatu yang menyenangkan, pilihannya pun bebas, mulai dari jalan kaki hingga joging. Lansia yang sudah lanjut juga dapat melakukan senam khusus untuk lansia maupun olahraga seperti yoga dan tai chi.</p>

<p class="p-b-25">
<b>2. Tidur 7 jam sehari</b></p>


<p class="p-b-25">
Usahakan tidur 7 jam sehari antara pukul 22.00-03.00 agar tubuh dapat melakukan detoksifikasi, salah satu tanda detoks adalah buang air di pagi hari.</p>

<p class="p-b-25">
<b>3. Latihan otak</b></p>
<p class="p-b-25">
Aktivitas seperti membaca, menulis dan mendengarkan dipercaya dapat membantu melatih daya ingat. Selain itu, aktivitas yang tergolong membantu daya ingat juga termasuk mendongeng dan belajar bahasa.</p>

<p class="p-b-25">
<b>4. Mengasuh cucu</b></p>

<p class="p-b-25">
Di kota besar, lazim ditemui keluarga tiga generasi tinggal dalam satu atap. Menurut Nugroho, salah satu keuntungan mengasuh cucu adalah membantu lansia berinteraksi dengan generasi yang lebih muda. Berinteraksi dengan orang lain merupakan aktivitas sosial yang membantu lansia tidak merasa kesepian.</p>

<p class="p-b-25">
<b>5. Bertemu teman sebaya</b></p>

<p class="p-b-25">
Bertukar kabar dengan sesama lansia dapat membantu orang tua untuk melatih keterampilan berkomunikasi. Lansia disarankan bersosialisasi dengan teman lama mereka, bisa jadi rekan kerja atau teman semasa sekolah dulu.

Seiring dengan perkembangan zaman, mengobrol dengan teman juga bisa dilakukan melalui pesan singkat di ponsel. Menurut Nugroho belajar menggunakan ponsel, misalnya untuk berkirim pesan di WhatsApp, membantu mengasah kemampuan kognitif.
								</p>
							</div>

							
						</div>

						<!-- Leave a comment -->
						<form class="leave-comment">
							<h4 class="m-text25 p-b-14">
								Leave a Comment
							</h4>

							<p class="s-text8 p-b-40">
								Your email address will not be published. Required fields are marked *
							</p>

							<textarea class="dis-block s-text7 size18 bo12 p-l-18 p-r-18 p-t-13 m-b-20" name="comment" placeholder="Comment..."></textarea>

							<div class="bo12 of-hidden size19 m-b-20">
								<input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="name" placeholder="Name *">
							</div>

							<div class="bo12 of-hidden size19 m-b-20">
								<input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="email" placeholder="Email *">
							</div>

							<div class="bo12 of-hidden size19 m-b-30">
								<input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="website" placeholder="Website">
							</div>

							<div class="w-size24">
								<!-- Button -->
								<button class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
									Post Comment
								</button>
							</div>
						</form>
					</div>
				</div>

				
			</div>
		</div>
	</section>

	<!-- Instagram -->
	<section class="instagram p-t-20">
		<div class="sec-title p-b-52 p-l-15 p-r-15">
			
		</div>

		<div class="flex-w">
			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-03.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-07.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-09.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-13.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-15.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>
		</div>
	</section>

	<!-- Shipping -->

	<!-- Footer -->
	<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
		<div class="flex-w p-b-90">
			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<center>DOKTER</center>
				</h4>

				<div>
					<p class="s-text7 w-size27">
						<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
					</p>

				</div>
			</div>

			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<center>BIDAN</center>
				</h4>

				<div>
					<p class="s-text7 w-size27">
						<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
					</p>

				</div>
			</div>

			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<center>PERAWAT</center>
				</h4>

				<div>
					<p class="s-text7 w-size27">
						<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
					</p>

				</div>
			</div>
		</div>

		<div class="t-center p-l-15 p-r-15">

			 <div class="t-center s-text8 p-t-20">
        Arkamaya Medical © 2019. All rights reserved. <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="<?php echo base_url(); ?>assets/front/https://colorlib.com" target="_blank">Ricka & Fiki</a>
      </div>
    </div>
	</footer>



	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection1 -->
	<div id="dropDownSelect1"></div>



<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.min.js"></script>
	<script type="text/javascript">
		$(".selection-1").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/slick/slick.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/slick-custom.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/sweetalert/sweetalert.min.js"></script>
	<script type="text/javascript">
		$('.block2-btn-addcart').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to cart !", "success");
			});
		});

		$('.block2-btn-addwishlist').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");
			});
		});
	</script>

<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>assets/front/js/main.js"></script>

</body>
</html>
