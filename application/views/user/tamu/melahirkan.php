<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('user/layout/head') ?>

<body class="animsition">

	<!-- Header -->
<?php $this->load->view('user/layout/header') ?>
<section class="bgwhite p-t-60 p-b-25">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-lg-9 p-b-80">
					<div class="p-r-50 p-r-0-lg">
						<div class="p-b-40">
							<div class="blog-detail-img wrap-pic-w">
								<img style="height:500px; width:1000px" src="<?php echo base_url(); ?>assets/front/images/melahirkan.jpg" alt="IMG-BLOG">
							</div>

							<div class="blog-detail-txt p-t-33">
								<h4 class="p-b-11 m-text24">
									Tips melahirkan lancar bagi ibu hamil
								</h4>

								<div class="s-text8 flex-w flex-m p-b-21">
									<span>
										By Admin
										<span class="m-l-3 m-r-6">|</span>
									</span>

									<span>
										28 Dec, 2018
										<span class="m-l-3 m-r-6">|</span>
									</span>

									<span>
										Cooking, Food
										<span class="m-l-3 m-r-6">|</span>
									</span>

									<span>
										8 Comments
									</span>
								</div>

								<p class="p-b-25">
									Kekhawatiran menghadapi persalinan wajar dirasakan oleh ibu yang sedang mengandung anak pertama, dan tak jarang pula ibu yang sudah pernah melahirkan juga merasakannya.

Dilansir dari situs parents.com, berikut ini 11 rahasia yang bisa membantu anda menjalani persalinan dengan lebih mudah dan lebih cepat.</p>

<p class="p-b-25">

<b>1. Menjaga kebugaran tubuh</b></p>
<p class="p-b-25">
Ibu hamil dengan kondisi tubuh yang fit punya kesempatan untuk menjalani proses persalinan yang cepat. Bagi Bunda yang sering melakukan olahraga, tubuh Bunda akan tetap bugar dan daya tahan tubuh tinggi saat menghadapi kontraksi sehingga tidak membutuhkan bantuan epidural atau obat medis lain.

Selama hamil, Bunda bisa melakukan olahraga ringan untuk meningkatkan kebugaran tubuh. Berenang, jalan kaki di sekitar rumah, atau mengikuti kelas olahraga untuk ibu hamil, tentunya setelah mendapat persetujuan dari dokter ya.</p>

<p class="p-b-25">

<b>2. Mengikuti kelas ibu hamil</b></p>
<p class="p-b-25">
Buat diri Anda familiar dengan proses kelahiran beserta semua tahapannya. Melatih diri untuk menghadapi setiap kontraksi akan membantu Bunda menghilangkan kepanikan dan bisa menjalani persalinan yang lebih mudah.


Carilah kelas ibu hamil yang tidak terlalu ramai, instruktur yang bersertifikat dan berpengalaman. Juga memiliki tujuan pelatihan yang sama dengan yang Bunda inginkan.</p>

<p class="p-b-25">

<b>3. Mendapatkan bantuan profesional </b></p>

<p class="p-b-25">
Sebuah analisa yang diterbitkan oleh Jurnal Ginekologi Amerika mengungkapkan bahwa ibu yang melahirkan dengan ditemani oleh seorang doula (seorang profesional yang dilatih untuk membantu ibu melahirkan dan juga pasangan) 50% lebih rendah tingkat kemungkinannya untuk menjalani operasi caesar.

Selain itu, mereka yang ditemani doula menjalani proses persalinan 25% lebih cepat. Dan 30% lebih rendah tingkat kebutuhannya untuk menggunakan suntikan obat saat melahirkan. Selain mendiskusikan dengan dokter, Bunda dan pasangan juga harus merasa nyaman dengan doula yang kalian pekerjakan.</p>

<p class="p-b-25">

<b>4. Mengalihkan perhatian</b></p>

<p class="p-b-25">
Bagi Anda yang akan melahirkan anak pertama, proses persalinan rata-rata mencapai 12 hingga 14 jam. Saat kontraksi pertama terjadi, Anda akan merasakannya di bagian bawah punggung, kemudian perut bagian bawah akan terasa kram.

Cobalah untuk tetap tenang, jika Anda menghitung setiap kontraksi dan mulai mengkhawatirkan kapan kontraksi berikutnya akan terjadi, Anda akan merasa sangat lelah dan lemas.

Alihkan perhatian dengan melakukan aktivitas lain seperti berjalan di halaman rumah, mandi dengan shower atau mengobrol dengan pasangan. Apapun yang membantu Anda untuk rileks bisa mempercepat segala sesuatu yang akan terjadi.</p>

<p class="p-b-25">
<b>5. Mengemil dengan hati-hati </b></p>

<p class="p-b-25">
Makanan kecil saat anda berada pada fase pertama proses kelahiran membantu menambah energi dalam tubuh anda. Hindari makanan yang terlalu berlemak atau yang sulit dicerna, perut yang terlalu kenyang akan membuat anda merasa mual bahkan muntah pada fase terakhir persalinan.

Sebuah studi terbaru di Universitas California menyatakan bahwa menambah cairan di pembuluh darah bisa mempercepat persalinan. Saat masih berada di rumah, minumlah air putih yang banyak. Ketika sudah berada di rumah sakit, mintalah minum pada suster atau pasangan Anda kapanpun Anda merasa haus.</p>

<p class="p-b-25">

<b>6. Mandi dengan shower hangat</b></p>

<p class="p-b-25">
Rasa sakit yang terjadi selama kontraksi membuat otot di tubuh Bunda tegang sehingga menambah ketidaknyamanan yang Bunda rasakan. Mandi dengan air shower hangat bisa meredakan ketegangan di tubuh Bunda. Hal ini bisa Anda lakukan pada tahap manapun dalam proses melahirkan.</p>

<p class="p-b-25">

<b>7. Berendam di bathtub</b></p>

<p class="p-b-25">
Saat Bunda sudah mengalami kontraksi yang berat namun dokter berkata bahwa belum saatnya untuk suntikan epidural, Bunda bisa berendam di bathtub untuk membantu merilekskan otot-otot yang tegang karena kontraksi.

Bunda juga bisa dengan mudah bergerak saat berada dalam air, menggosokkan punggung secara pelan di dinding bathtub juga membantu mengurangi rasa kram dan tegang.

Saat Anda merasa telah cukup rileks, Anda bisa keluar dari bathtub. Jangan terkejut kalau pembukaan Anda sudah sempurna dan bayi siap lahir dalam waktu kurang dari satu jam.</p>

<p class="p-b-25">
<b>8. Menerima Pijatan</b></p>

<p class="p-b-25">
Dalam sebuah studi yang dilakukan oleh Sekolah Medis di Universitas Miami, menyatakan bahwa memijat perempuan yang berada dalam proses persalinan membantu mengurangi rasa sakit dan menurunkan tingkat kegelisahan si perempuan.

Bunda bisa mencobanya dengan pasangan saat Bunda mulai merasakan kontraksi awal, minta dia memijat bagian tubuh yang Anda inginkan seperti di leher atau punggung bagian bawah. Tapi ada saat-saat tertentu ketika Anda sama sekali tak mau disentuh.</p>

<p class="p-b-25">
<b>9. Jangan berbaring</b></p>

<p class="p-b-25">
Duduk tegak membantu anda mempercepat persalinan, karena gaya gravitasi akan membuat kepala bayi menekan mulut rahim sehingga membantu proses pembukaan jalan lahir.

Anda juga bisa mencoba mengubah posisi dengan berlutut, berdiri atau berjongkok. Gerakan seperti ini membantu melebarkan panggul Anda sehingga kepala bayi mudah keluar.</p>

<p class="p-b-25">
<b>10. Bersedia menerima bantuan medis</b></p>
<p class="p-b-25">
Faktanya, saat tubuh Anda tegang karena kontraksi, suntikan anestesi di bawah punggung akan membuat otot-otot anda rileks sehingga mempercepat proses pembukaan. Obat yang disuntikkan akan menyebar ke bagian yang membutuhkan sebelum mencapai bayi, jadi anda tak perlu khawatir.

“Saat anda berada pada persalinan yang aktif, dan pembukaan telah melewati 3 cm. Menerima suntikan epidural tidak akan memperpanjang masa persalinan atau meningkatkan kemungkinan untuk operasi caesar,” kata Dr. Philip Samuels, MD. Ahli kandungan di Universitas Ohio State.</p>

<p class="p-b-25">
<b>11. Bernapas dengan teratur</b></p>

<p class="p-b-25">
Bernapas dengan teratur tidak hanya membantu Bunda selama kontraksi, menarik napas dan menghembuskannya secara perlahan bisa mengurangi ketegangan di bagian tubuh yang terasa kram karena kontraksi.

Untuk membantu agar Bunda tidak panik selama masa kontraksi yang menyakitkan, Bunda harus ingat bahwa setiap persalinan selalu berakhir dengan kebahagiaan. Hadirnya sang bayi akan membuat Anda merasa seberat apapun kesulitan yang dilalui saat melahirkan menjadi sepadan.

Menjadi ibu adalah anugerah, tidak semua perempuan bisa merasakannya. Jadi jangan pernah takut menghadapi persalinan, karena setelah semua proses itu berakhir, Bunda akan menggendong seorang bayi lucu dalam pelukan..
								</p>
							</div>

							
						</div>

						<!-- Leave a comment -->
						<form class="leave-comment">
							<h4 class="m-text25 p-b-14">
								Leave a Comment
							</h4>

							<p class="s-text8 p-b-40">
								Your email address will not be published. Required fields are marked *
							</p>

							<textarea class="dis-block s-text7 size18 bo12 p-l-18 p-r-18 p-t-13 m-b-20" name="comment" placeholder="Comment..."></textarea>

							<div class="bo12 of-hidden size19 m-b-20">
								<input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="name" placeholder="Name *">
							</div>

							<div class="bo12 of-hidden size19 m-b-20">
								<input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="email" placeholder="Email *">
							</div>

							<div class="bo12 of-hidden size19 m-b-30">
								<input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="website" placeholder="Website">
							</div>

							<div class="w-size24">
								<!-- Button -->
								<button class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
									Post Comment
								</button>
							</div>
						</form>
					</div>
				</div>

				
			</div>
		</div>
	</section>

	<!-- Instagram -->
	<section class="instagram p-t-20">
		<div class="sec-title p-b-52 p-l-15 p-r-15">
			
		</div>

		<div class="flex-w">
			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-03.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-07.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-09.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-13.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-15.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>
		</div>
	</section>

	<!-- Shipping -->

	<!-- Footer -->
	<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
		<div class="flex-w p-b-90">
			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<center>DOKTER</center>
				</h4>

				<div>
					<p class="s-text7 w-size27">
						<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
					</p>

				</div>
			</div>

			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<center>BIDAN</center>
				</h4>

				<div>
					<p class="s-text7 w-size27">
						<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
					</p>

				</div>
			</div>

			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<center>PERAWAT</center>
				</h4>

				<div>
					<p class="s-text7 w-size27">
						<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
					</p>

				</div>
			</div>
		</div>

		<div class="t-center p-l-15 p-r-15">

			 <div class="t-center s-text8 p-t-20">
        Arkamaya Medical © 2019. All rights reserved. <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="<?php echo base_url(); ?>assets/front/https://colorlib.com" target="_blank">Ricka & Fiki</a>
      </div>
    </div>
	</footer>



	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection1 -->
	<div id="dropDownSelect1"></div>



<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.min.js"></script>
	<script type="text/javascript">
		$(".selection-1").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/slick/slick.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/slick-custom.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/sweetalert/sweetalert.min.js"></script>
	<script type="text/javascript">
		$('.block2-btn-addcart').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to cart !", "success");
			});
		});

		$('.block2-btn-addwishlist').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");
			});
		});
	</script>

<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>assets/front/js/main.js"></script>

</body>
</html>
