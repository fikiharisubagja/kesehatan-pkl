	<header class="header1">
		<!-- Header desktop -->
		<div class="container-menu-header">
			<div class="topbar" style="background-color: #80CCFF">
				<div class="topbar-social">
					<a href="https://www.facebook.com/ptarkamaya" class="topbar-social-item fa fa-facebook"></a>
					<a href="https://www.twitter.com" class="topbar-social-item fa fa-twitter"></a>
					<a href="https://www.instagram.com/ptarkamaya" class="topbar-social-item fa fa-instagram"></a>
					<a href="https://www.youtube.com" class="topbar-social-item fa fa-youtube-play"></a>
				</div>

				<span class="topbar-child1">
				
				</span>

				<div class="topbar-child2">
					<span class="topbar-email">
						<a href="<?php echo base_url('index.php/Tamu/signin')?>">Sigin/Signup</a>
					</span>

					
				</div>

			</div>

			<div class="wrap_header">
				<!-- Logo -->
				<a href="<?php echo base_url('index.php/Tamu'); ?>" class="logo">
					<img style=""  src="<?php echo base_url(); ?>assets/front/images/am.jpg" alt="IMG-LOGO" >
				</a>

				<!-- Menu -->
				<div class="wrap_menu">
					<nav class="menu">
						<ul class="main_menu">
							<li>
								<a href="<?php echo base_url('index.php/Tamu'); ?>">Beranda</a>
								
							</li>

							<li>
								<a href="<?php echo base_url('index.php/Tamu/pesan_medis'); ?>">Layanan</a>
							</li>

							<li>
								<a href="<?php echo base_url('index.php/Tamu/registrasi_medis'); ?>">Bergabung Sebagai Medis</a>
							</li>

							<li>
								<a href="<?php echo base_url('index.php/Tamu/tentang_kami'); ?>">Tentang Kami</a>
							</li>

							<li>
								<a href="<?php echo base_url('index.php/Tamu/kontak'); ?>">Kontak</a>
							</li>
						</ul>
					</nav>
				</div>

				<!-- Header Icon -->
				<div class="header-icons">
					<a href="<?php echo base_url('index.php/tamu/signin'); ?>" class="header-wrapicon1 dis-block">
						<img src="<?php echo base_url(); ?>assets/front/images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
					</a>
					</div>
				</div>
			</div>
		</div>

		<!-- Header Mobile -->
		<div class="wrap_header_mobile">
			<!-- Logo moblie -->
			<a href="<?php echo base_url(); ?>assets/front/index.html" class="logo-mobile">
				<img src="<?php echo base_url(); ?>assets/front/images/icons/Logo.png" height="250px" width="250px" alt="IMG-LOGO">
			</a>

			<!-- Button show menu -->
			<div class="btn-show-menu">
				<!-- Header Icon mobile -->
				<div class="header-icons-mobile">
					<a href="<?php echo base_url(); ?>assets/front/#" class="header-wrapicon1 dis-block">
						<img src="<?php echo base_url(); ?>assets/front/images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
					</a>

					<span class="linedivide2"></span>

					<div class="header-wrapicon2">
						<img src="<?php echo base_url(); ?>assets/front/images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICON">
						<span class="header-icons-noti">0</span>

						<!-- Header cart noti -->
						<div class="header-cart header-dropdown">
							<ul class="header-cart-wrapitem">
								<li class="header-cart-item">
									<div class="header-cart-item-img">
										<img src="<?php echo base_url(); ?>assets/front/images/item-cart-01.jpg" alt="IMG">
									</div>

									<div class="header-cart-item-txt">
										<a href="<?php echo base_url(); ?>assets/front/#" class="header-cart-item-name">
											White Shirt With Pleat Detail Back
										</a>

										<span class="header-cart-item-info">
											1 x $19.00
										</span>
									</div>
								</li>

								<li class="header-cart-item">
									<div class="header-cart-item-img">
										<img src="<?php echo base_url(); ?>assets/front/images/item-cart-02.jpg" alt="IMG">
									</div>

									<div class="header-cart-item-txt">
										<a href="<?php echo base_url(); ?>assets/front/#" class="header-cart-item-name">
											Converse All Star Hi Black Canvas
										</a>

										<span class="header-cart-item-info">
											1 x $39.00
										</span>
									</div>
								</li>

								<li class="header-cart-item">
									<div class="header-cart-item-img">
										<img src="<?php echo base_url(); ?>assets/front/images/item-cart-03.jpg" alt="IMG">
									</div>

									<div class="header-cart-item-txt">
										<a href="<?php echo base_url(); ?>assets/front/#" class="header-cart-item-name">
											Nixon Porter Leather Watch In Tan
										</a>

										<span class="header-cart-item-info">
											1 x $17.00
										</span>
									</div>
								</li>
							</ul>

							<div class="header-cart-total">
								Total: $75.00
							</div>

							<div class="header-cart-buttons">
								<div class="header-cart-wrapbtn">
									<!-- Button -->
									<a href="<?php echo base_url('index.php/Pembeli/halaman_cart'); ?>" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
										View Cart
									</a>
								</div>

								<div class="header-cart-wrapbtn">
									<!-- Button -->
									<a href="<?php echo base_url(); ?>assets/front/#" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
										Check Out
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</div>
			</div>
		</div>

		<!-- Menu Mobile -->
		<div class="wrap-side-menu" >
			<nav class="side-menu">
				<ul class="main-menu">
					<li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
						<span class="topbar-child1">
							Free shipping for standard order over $100
						</span>
					</li>

					<li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
						<div class="topbar-child2-mobile">
							<span class="topbar-email">
								fashe@example.com
							</span>

							<div class="topbar-social-mobile">
							<a href="<?php echo base_url(); ?>assets/front/#" class="topbar-social-item fa fa-facebook"></a>
							</div>
						</div>
					</li>

					<li class="item-topbar-mobile p-l-10">
						<div class="topbar-social-mobile">
							<a href="<?php echo base_url(); ?>assets/front/#" class="topbar-social-item fa fa-facebook"></a>
							<a href="<?php echo base_url(); ?>assets/front/#" class="topbar-social-item fa fa-instagram"></a>
							<a href="<?php echo base_url(); ?>assets/front/#" class="topbar-social-item fa fa-pinterest-p"></a>
							<a href="<?php echo base_url(); ?>assets/front/#" class="topbar-social-item fa fa-snapchat-ghost"></a>
							<a href="<?php echo base_url(); ?>assets/front/#" class="topbar-social-item fa fa-youtube-play"></a>
						</div>
					</li>

					<li class="item-menu-mobile">
						<a href="<?php echo base_url(); ?>">Home</a>
						
						<i class="arrow-main-menu fa fa-angle-right" aria-hidden="true"></i>
					</li>

					<li class="item-menu-mobile">
						<a href="<?php echo base_url(); ?>">Shop</a>
					</li>

					<li class="item-menu-mobile">
						<a href="<?php echo base_url(); ?>">Sale</a>
					</li>

					<li class="item-menu-mobile">
						<a href="<?php echo base_url(); ?>">Features</a>
					</li>

					<li class="item-menu-mobile">
						<a href="<?php echo base_url(); ?>">Blog</a>
					</li>

					<li class="item-menu-mobile">
						<a href="<?php echo base_url(); ?>">About</a>
					</li>

					<li class="item-menu-mobile">
						<a href="<?php echo base_url(); ?>">Contact</a>
					</li>
				</ul>
			</nav>
		</div>
	</header>