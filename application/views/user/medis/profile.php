<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('user/layout/head') ?>
<body class="animsition">

	<!-- Header -->

	<?php $this->load->view('user/layout/header_medis') ?>
	<!-- Title Page -->
	

	<!-- content page -->

<section class="bgwhite p-t-66 p-b-20">

<div class="container">
			
			<div class="row">
				
				<div class="col col-md-20">
					<?php foreach ($profile->result_array() as $pp){ ?>
					<span class="pull-left"><strong class="btn-warning">Status Medis : </strong></span>
					<h1><b><input type="text" class="form-control" id="exampleInputEmail1" value="<?php echo $pp['status_medis'] ?>"  name="status_medis"></b></h1>
					
					<br><br>
					<!--left col-->
					<ul class="list-group">
						
							<center><strong style="background-color: #80CCFF" class="list-group-item text-muted" contenteditable="false"><b>PROFILE<b></strong></strong><br>

							    <button data-toggle="modal" data-target="#edit-data<?php echo $pp['id_medis'];?>">Ubah</button>
							<?php echo form_open_multipart('medis/do_edit_profile');?>
							<?php $image = $pp['photo_medis'];
							if (empty($image)) $image = "user.png";
							?>
								<center>
										<img style=" height: 200px; width:200px" src="<?php echo base_url('libs/medis');?>/<?php echo $image?>" class="img-circle" alt="User Image">
									
								</center><br>

								<li> <input type="hidden" class="form-control" id="exampleInputEmail1" value="<?php echo $pp['id_medis'] ?>" name="id_medis"></li>

								<center><li class="list-group-item text-right"><span class="pull-left"><strong class="">Nama : </strong></span> <input type="text" class="form-control" id="exampleInputEmail1" value="<?php echo $pp['nama_lengkap'] ?>"  name="nama_lengkap"></li></center>

								<li class="list-group-item text-right"><span class="pull-left"><strong class="">Alamat : </strong></span> <input type="text" class="form-control" id="exampleInputEmail1" value="<?php echo $pp['alamat'] ?>"  name="alamat_lengkap"></li>

								<li class="list-group-item text-right"><span class="pull-left"><strong class="">Email : </strong></span> <input type="text" class="form-control" id="exampleInputEmail1" value="<?php echo $pp['email'] ?>"  name="email"></li>

								<li class="list-group-item text-right"><span class="pull-left"><strong class="">Umur : </strong></span><input type="text" class="form-control" id="exampleInputEmail1" value="<?php echo $pp['usia'] ?>"  name="usia"></li>
								
								<li class="list-group-item text-muted" contenteditable="false"><b>CONTACT DETAILS</b></li>

								<li class="list-group-item text-right"><span class="pull-left"><strong class="">No Telepon : </strong></span><input type="text" class="form-control" id="exampleInputEmail1" value="<?php echo $pp['no_hp'] ?>"  name="no_hp"></li><br><br>

								<li class="list-group-item text-muted" contenteditable="false"><b>Ubah Password</b></li>
                <li class="list-group-item text-right"><span class="pull-left"><strong class="">Masukkan Password Lama : </strong></span><input type="password" class="form-control" id="exampleInputEmail1" value=""  name="password" data-toggle="password"></li>

                <li class="list-group-item text-right"><span class="pull-left"><strong class="">Masukkan Password Baru : </strong></span><input type="password" class="form-control" id="exampleInputEmail1" value=""  name="password">

                <li class="list-group-item text-right"><span class="pull-left"><strong class="">Ulangi Password : </strong></span><input type="password" class="form-control" id="exampleInputEmail1" value=""  name="password"></li><br><br>
								
								<button type="submit" class="btn btn-warning">Update Profile</button>
							<?php } ?>
						</form>
						
 <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="edit-data<?php echo $pp['id_medis'];?>" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">Ubah Photo</h4>
          </div>
          <form class="form-horizontal" action="<?php echo base_url('index.php/medis/do_edit_profile')?>" method="post" enctype="multipart/form-data" role="form">
            <div class="modal-body">
              <div class="form-group">
                <label class="col-lg-2 col-sm-2 control-label">Nama</label>
                <div class="col-lg-10">
                  <input type="hidden" id="id_medis" name="id_medis" value="">
                  <input type="file" class="form-control" value="" id="nama_lengkap" name="nama_lengkap" placeholder="Tuliskan Nama">
                </div>
              </div>

            </div>
            <div class="modal-footer">
              <button class="btn btn-success" type="submit"> Simpan&nbsp;</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal"> Batal</button>
            </div>
          </form>
        </div>
      </div>
    </div>

						</ul>
					</div>
				</div>
			</div>
		</div>
	

</section>

<section style="background-color: #80CCFF" class="blog bgwhite p-t-94 p-b-40">
	<div class="container">
		<div class="sec-title p-b-40">
			<h3 style="font-family: Cooper Black" class="m-text5 t-center">
				Mengapa Harus Arkamaya Medical ?
			</h3></br>
		</div>

		<div class="row">
			<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
				<!-- Block3 -->
				<center> <div class="block3">
					<a class="block3-img dis-block hov-img-zoom">
						<img style="width: 100px" src="<?php echo base_url(); ?>assets/front/images/heart.png" alt="IMG-BLOG">
					</a>

					<div class="block3-txt p-t-14">
						<h4 class="p-b-7">
							<a style="font-family: Cooper Black" class="m-text11">
								Terpercaya
							</a>
						</h4>
					</div>
				</div></center>
			</div>

			<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
				<!-- Block3 -->
				<center><div class="block3">
					<a class="block3-img dis-block hov-img-zoom">
						<img style="width: 100px" src="<?php echo base_url(); ?>assets/front/images/family.png" alt="IMG-BLOG">
					</a>

					<div class="block3-txt p-t-14">
						<h4 class="p-b-7">
							<a style="font-family: Cooper Black" class="m-text11">
								Peduli Keluarga
							</a>
						</h4>
					</div>
				</div></center>
			</div>

			<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
				<!-- Block3 -->
				<center> <div class="block3">
					<a class="block3-img dis-block hov-img-zoom">
						<img style="width: 100px" src="<?php echo base_url(); ?>assets/front/images/ambulance.png" alt="IMG-BLOG">
					</a>

					<div class="block3-txt p-t-14">
						<h4 class="p-b-7">
							<a style="font-family: Cooper Black" class="m-text11">
								Cepat Tanggap
							</a>
						</h4>
					</div>
				</div></center>
			</div>
		</div>
	</div>
</section>


<!-- Footer -->
<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
	<div class="flex-w p-b-90">
		<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
			<h4 class="s-text12 p-b-30">
				<center>DOKTER</center>
			</h4>

			<div>
				<p class="s-text7 w-size27">
					<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
				</p>

			</div>
		</div>

		<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
			<h4 class="s-text12 p-b-30">
				<center>BIDAN</center>
			</h4>

			<div>
				<p class="s-text7 w-size27">
					<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
				</p>

			</div>
		</div>

		<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
			<h4 class="s-text12 p-b-30">
				<center>PERAWAT</center>
			</h4>

			<div>
				<p class="s-text7 w-size27">
					<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
				</p>

			</div>
		</div>
	</div>

	<div class="t-center p-l-15 p-r-15">

		<div class="t-center s-text8 p-t-20">
			Arkamaya Medical © 2019. All rights reserved. <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="<?php echo base_url(); ?>assets/front/https://colorlib.com" target="_blank">Ricka & Fiki</a>
		</div>
	</div>
</footer>



<!-- Back to top -->
<div class="btn-back-to-top bg0-hov" id="myBtn">
	<span class="symbol-btn-back-to-top">
		<i class="fa fa-angle-double-up" aria-hidden="true"></i>
	</span>
</div>

<!-- Container Selection -->
<div id="dropDownSelect1"></div>
<div id="dropDownSelect2"></div>



<!--===============================================================================================-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/popper.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.min.js"></script>
<script type="text/javascript">
	$(".selection-1").select2({
		minimumResultsForSearch: 20,
		dropdownParent: $('#dropDownSelect1')
	});

	$(".selection-2").select2({
		minimumResultsForSearch: 20,
		dropdownParent: $('#dropDownSelect2')
	});
</script>
<!--===============================================================================================-->
<script src="<?php echo base_url(); ?>assets/front/js/main.js"></script>

</body>
</html>
