<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('user/layout/head') ?>

<body class="animsition">

	<!-- Header -->
<?php $this->load->view('user/layout/header_pelanggan') ?>
<section class="bgwhite p-t-60 p-b-25">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-lg-9 p-b-80">
					<div class="p-r-50 p-r-0-lg">
						<div class="p-b-40">
							<div class="blog-detail-img wrap-pic-w">
								<img style="height:500px; width:1000px" src="<?php echo base_url(); ?>assets/front/images/buah.jpg" alt="IMG-BLOG">
							</div>

							<div class="blog-detail-txt p-t-33">
								<h4 class="p-b-11 m-text24">
									Ini Makanan Sehat yang Perlu Dikonsumsi Setiap Hari
								</h4>

								<div class="s-text8 flex-w flex-m p-b-21">
									<span>
										By Admin
										<span class="m-l-3 m-r-6">|</span>
									</span>

									<span>
										28 Dec, 2018
										<span class="m-l-3 m-r-6">|</span>
									</span>

									<span>
										Cooking, Food
										<span class="m-l-3 m-r-6">|</span>
									</span>

									<span>
										8 Comments
									</span>
								</div>

								<p class="p-b-25">
									Makanan sehat sering dianggap tidak enak, padahal banyak jenis makanan sehat yang terasa lezat, namun kerap terlupakan. Mengonsumsi makanan sehat memiliki banyak manfaat, termasuk melindungi diri dari ancaman penyakit kronis.

Konsumsi makanan sehat, tidak harus membatasi satu atau beberapa jenis makanan saja.  Mengonsumsi berbagai jenis makanan, justru dapat memberikan nutrisi berbeda-beda sehingga mampu melengkapi gizi yang dibutuhkan tubuh.</p>

<p class="p-b-25"><b>Beragam Pilihan Makanan Sehat</b></p>
<p class="p-b-25">Makanan sehat umumnya mencakup berbagai nutrisi dalam jumlah yang memadai, termasuk vitamin dan mineral. Sayuran hijau menempati urutan pertama dalam jenis makanan sehat karena kandungan nutrisi yang lengkap. Di antara jenis sayuran hijau, ada berbagai pilihan yang baik untuk dikonsumsi, seperti sawi hijau, brokoli, dan bayam dengan kandungan serat yang tinggi.</p>

<p class="p-b-25">Selain sayur, buah juga sangat penting dikonsumsi karena mengandung banyak vitamin. Dari sekian banyak buah, beberapa buah yang populer dan kaya akan nutrisi, di antaranya adalah:</p>

<p class="p-b-25">- Apel, karena banyak mengandung serat, vitamin C, dan antioksidan lain.</p>
<p class="p-b-25">- Jeruk, yang tinggi kandungan vitamin C.</p>
<p class="p-b-25">- Alpukat, yang kaya lemak sehat, kalium dan vitamin C.</p>
<p class="p-b-25">- Pisang, sebagai salah satu sumber kalium.</p>
<p class="p-b-25">- Buah-buahan berry, seperti blueberry dan strawberry. Selain rendah kalori, juga kaya akan antioksidan dan serat.</p>
<p class="p-b-25">Mengonsumsi makanan sehat juga sebaiknya tetap seimbang dan beragam. Berikut kelompok makanan sehat utama lain yang harus Anda konsumsi, selain sayuran dan buah-buahan:</p>

<p class="p-b-25"><b>Daging dan telur </b></p>

<p class="p-b-25"> Sebuah analisa yang diterbitkan oleh Jurnal Ginekologi Amerika mengungkapkan bahwa ibu yang melahirkan dengan ditemani oleh seorang doula (seorang profesional yang dilatih untuk membantu ibu melahirkan dan juga pasangan) 50% lebih rendah tingkat kemungkinannya untuk menjalani operasi caesar.

Selain itu, mereka yang ditemani doula menjalani proses persalinan 25% lebih cepat. Dan 30% lebih rendah tingkat kebutuhannya untuk menggunakan suntikan obat saat melahirkan. Selain mendiskusikan dengan dokter, Bunda dan pasangan juga harus merasa nyaman dengan doula yang kalian pekerjakan.</p>

<p class="p-b-25"><b>Kacang-kacangan dan biji-bijian</b></p>
<p class="p-b-25">Kelompok makanan ini merupakan jenis makanan sehat yang renyah dan sarat akan nutrisi, termasuk magnesium dan vitamin E.</p>

<p class="p-b-25"><b>Ikan dan makanan laut lainnya</b></p>
<p class="p-b-25">Ragam makanan laut terutama ikan, adalah jenis makanan sehat yang kaya asam lemak omega-3 dan yodium. Penelitian menunjukkan, orang yang sering makan ikan laut cenderung lebih panjang umur dan memiliki risiko lebih rendah terhadap banyak penyakit, termasuk penyakit jantung.</p>

<p class="p-b-25"><b>Susu</b></p>
<p class="p-b-25"> Susu sangat tinggi vitamin, mineral, protein hewani berkualitas dan lemak sehat. Selain itu, susu merupakan salah satu sumber kalsium terbaik. Anda juga bisa mengonsumsi susu dalam beragam bentuk produk olahan susu, seperti keju. Susu yang difermentasikan menjadi yoghurt, mengandung banyak bakteri baik untuk pencernaan.</p>

<p class="p-b-25"><b>Kenali Berbagai Jenis Makanan yang Perlu dihindari</b></p>
<p class="p-b-25">Selain perlu mengonsumsi makanan sehat, Anda juga harus mengenali makanan yang sebaiknya dihindari karena dapat memicu gangguan kesehatan.

Pertama, hindari makanan dan minuman yang ditambahkan gula. Apabila sering dikonsumsi dalam jumlah besar, dapat memicu terjadinya resistensi insulin di tubuh dan menyebabkan berbagai penyakit serius, termasuk diabetes tipe 2 dan penyakit jantung.

Kemudian, hindari makanan cepat saji atau junk food, seperti kentang goreng, ayam goreng, dan keripik. Makanan jenis ini memiliki kalori tinggi, tetapi sedikit nilai nutrisinya. Selain itu, makanan cepat saji dapat mendorong makan berlebihan.

Hindari juga makanan atau minuman yang tidak sehat lainnya, seperti permen, es krim, daging olahan, dan keju olahan. Namun, apabila Anda ingin mengonsumsinya, sebaiknya hanya dilakukan sesekali saja.</p>

<p class="p-b-25"> Mulailah mengonsumsi makanan sehat yang seimbang, demi menjaga dan meningkatkan kesehatan tubuh Anda. Agar tidak bosan, Anda bisa mencoba kreasi dan kombinasi berbagai jenis makanan sehat dalam variasi sajian yang menarik setiap harinya. Jangan lupa untuk melengkapi pola makan sehat Anda dengan pola hidup yang sehat pula, yakni dengan memperbanyak konsumsi air putih, istirahat yang cukup dan rutin olahraga. Jika perlu, Anda bisa berkonsultasi ke dokter atau ahli gizi untuk mendapatkan rekomendasi makanan sehat yang sesuai dengan kebutuhan tubuh Anda.</p>
							</div>
						</div>

						<!-- Leave a comment -->
						<form class="leave-comment">
							<h4 class="m-text25 p-b-14">
								Leave a Comment
							</h4>

							<p class="s-text8 p-b-40">
								Your email address will not be published. Required fields are marked *
							</p>

							<textarea class="dis-block s-text7 size18 bo12 p-l-18 p-r-18 p-t-13 m-b-20" name="comment" placeholder="Comment..."></textarea>

							<div class="bo12 of-hidden size19 m-b-20">
								<input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="name" placeholder="Name *">
							</div>

							<div class="bo12 of-hidden size19 m-b-20">
								<input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="email" placeholder="Email *">
							</div>

							<div class="bo12 of-hidden size19 m-b-30">
								<input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="website" placeholder="Website">
							</div>

							<div class="w-size24">
								<!-- Button -->
								<button class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
									Post Comment
								</button>
							</div>
						</form>
					</div>
				</div>

				
			</div>
		</div>
	</section>

	<!-- Instagram -->
	<section class="instagram p-t-20">
		<div class="sec-title p-b-52 p-l-15 p-r-15">
			
		</div>

		<div class="flex-w">
			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-03.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-07.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-09.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-13.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-15.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>
		</div>
	</section>

	<!-- Shipping -->

	<!-- Footer -->
	<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
		<div class="flex-w p-b-90">
			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<center>DOKTER</center>
				</h4>

				<div>
					<p class="s-text7 w-size27">
						<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
					</p>

				</div>
			</div>

			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<center>BIDAN</center>
				</h4>

				<div>
					<p class="s-text7 w-size27">
						<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
					</p>

				</div>
			</div>

			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<center>PERAWAT</center>
				</h4>

				<div>
					<p class="s-text7 w-size27">
						<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
					</p>

				</div>
			</div>
		</div>

		<div class="t-center p-l-15 p-r-15">

			 <div class="t-center s-text8 p-t-20">
        Arkamaya Medical © 2019. All rights reserved. <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="<?php echo base_url(); ?>assets/front/https://colorlib.com" target="_blank">Ricka & Fiki</a>
      </div>
    </div>
	</footer>



	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection1 -->
	<div id="dropDownSelect1"></div>



<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.min.js"></script>
	<script type="text/javascript">
		$(".selection-1").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/slick/slick.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/slick-custom.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/sweetalert/sweetalert.min.js"></script>
	<script type="text/javascript">
		$('.block2-btn-addcart').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to cart !", "success");
			});
		});

		$('.block2-btn-addwishlist').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");
			});
		});
	</script>

<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>assets/front/js/main.js"></script>

</body>
</html>
