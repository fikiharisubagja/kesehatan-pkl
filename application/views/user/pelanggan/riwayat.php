<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('user/layout/head') ?>
<body class="animsition">

	<!-- Header -->
<?php $this->load->view('user/layout/header_pelanggan') ?>


	<!-- Title Page -->


	<!-- Content page -->
	<section class="bgwhite p-t-55 p-b-65">
		<div class="container">
			<div class="row">
				

				<div class="col-sm-6 col-md-8 col-lg-9 p-b-50">
					<!--  -->


					<!-- Product -->
                    <?php echo date('d-m-y');?>
					<div class="row">
						<div class="">
							<!-- Block2 -->
						
				<table align="center" class="table table-bordered table-striped" style="">
						<tr style="background: #80CCFF" class="table-head">
							<th class="">ID Transaksi</th>
							<th class="">Jenis Medis</th>
							<th class="">Nama Medis</th>
							<th class="">Deskripsi Gejala</th>
						    <th class="">Nama Obat</th>
							<th class="">Jumlah</th>
							<th class="">Layanan Lainnya</th>
							<th class="">Alamat</th>
							<th class="">Alamat Detail</th>
							<th class="">Total Bayar</th>

							<!--  -->
							<!-- <th class="">Aksi</th> -->

                            
						</tr>
                        
                              <?php
                			
               				foreach ($pesanan->result_array() as $dp) {
                			?>               
						<tr class="table-row">
							<td class="column-1"><?php echo $dp['id_transaksi']; ?></td>
							<td class="column-1"><?php echo $dp['jenis_medis']; ?></td>
							<td class="column-1"><?php echo $dp['nama_lengkap']; ?></td>
							<td class="column-1"><?php echo $dp['des_gejala']; ?></td>
							<td class="column-1"><?php echo $dp['nama_obat']; ?></td>
							<td class="column-1"><?php echo $dp['jumlah_obat']; ?></td>
							<td class="column-1"><?php echo $dp['nama_tambahan']; ?></td>
							<td class="column-1"><?php echo $dp['alamat']; ?></td>
							<td class="column-1"><?php echo $dp['alamat_detail']; ?></td>
							<td class="column-1"><?php echo $dp['total_bayar']; ?></td>
						</tr>
					 <?php
                 
                    }
                	?> 
					</table>
			</div>
		</div>
	</section>

	<section style="background-color: #80CCFF" class="blog bgwhite p-t-94 p-b-65">
    <div class="container">
      <div class="sec-title p-b-52">
        <h3 style="font-family: Cooper Black" class="m-text5 t-center">
          Mengapa Harus Arkamaya Medical ?
        </h3></br>
      </div>

	 <div class="row">
        <div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
          <!-- Block3 -->
         <center> <div class="block3">
            <a class="block3-img dis-block hov-img-zoom">
              <img style="width: 100px" src="<?php echo base_url(); ?>assets/front/images/heart.png" alt="IMG-BLOG">
            </a>

           <div class="block3-txt p-t-14">
              <h4 class="p-b-7">
                <a style="font-family: Cooper Black" class="m-text11">
                 Terpercaya
                </a>
              </h4>
            </div>
          </div></center>
        </div>

        <div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
          <!-- Block3 -->
          <center><div class="block3">
            <a class="block3-img dis-block hov-img-zoom">
              <img style="width: 100px" src="<?php echo base_url(); ?>assets/front/images/family.png" alt="IMG-BLOG">
            </a>

            <div class="block3-txt p-t-14">
              <h4 class="p-b-7">
                <a style="font-family: Cooper Black" class="m-text11">
                  Peduli Keluarga
                </a>
              </h4>
            </div>
          </div></center>
        </div>

        <div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
          <!-- Block3 -->
          <center> <div class="block3">
            <a class="block3-img dis-block hov-img-zoom">
              <img style="width: 100px" src="<?php echo base_url(); ?>assets/front/images/ambulance.png" alt="IMG-BLOG">
            </a>

            <div class="block3-txt p-t-14">
              <h4 class="p-b-7">
                <a style="font-family: Cooper Black" class="m-text11">
                  Cepat Tanggap
                </a>
              </h4>
            </div>
          </div></center>
        </div>
      </div>
    </div>
</section>


	<!-- Footer -->
	 <footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
    <div class="flex-w p-b-90">

      <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
        <h4 class="s-text12 p-b-30">
        <center>  DOKTER </center>
        </h4>

        <div>
          <p class="s-text7 w-size27">
            Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879
          </p>
        </div>
      </div>

      <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
        <h4 class="s-text12 p-b-30">
        <center>  BIDAN </center>
        </h4>

        <div>
          <p class="s-text7 w-size27">
            Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879
          </p>
        </div>
      </div>

      <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
        <h4 class="s-text12 p-b-30">
        <center>  PERAWAT </center>
        </h4>

        <div>
          <p class="s-text7 w-size27">
            Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879
          </p>
        </div>
      </div>
    </div>

    <div class="t-center p-l-15 p-r-15">

      <div class="t-center s-text8 p-t-20">
        Arkamaya Medical © 2019. All rights reserved. <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="<?php echo base_url(); ?>assets/front/https://colorlib.com" target="_blank">Ricka & Fiki</a>
      </div>
    </div>
  </footer>



	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection -->
	<div id="dropDownSelect1"></div>
	<div id="dropDownSelect2"></div>



<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.min.js"></script>
	<script type="text/javascript">
		$(".selection-1").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});

		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect2')
		});
	</script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/slick/slick.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/slick-custom.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/sweetalert/sweetalert.min.js"></script>
	<script type="text/javascript">
		$('.block2-btn-addcart').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to cart !", "success");
			});
		});

		$('.block2-btn-addwishlist').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");
			});
		});
	</script>

<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/noui/nouislider.min.js"></script>
	<script type="text/javascript">
		/*[ No ui ]
	    ===========================================================*/
	    var filterBar = document.getElementById('filter-bar');

	    noUiSlider.create(filterBar, {
	        start: [ 50, 200 ],
	        connect: true,
	        range: {
	            'min': 50,
	            'max': 200
	        }
	    });

	    var skipValues = [
	    document.getElementById('value-lower'),
	    document.getElementById('value-upper')
	    ];

	    filterBar.noUiSlider.on('update', function( values, handle ) {
	        skipValues[handle].innerHTML = Math.round(values[handle]) ;
	    });
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>assets/front/js/main.js"></script>

</body>
</html>