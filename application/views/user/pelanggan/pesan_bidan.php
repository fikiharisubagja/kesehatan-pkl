<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('user/layout/head') ?>
<body class="animsition">

	<!-- Header -->
<?php $this->load->view('user/layout/header_pelanggan') ?>


	<!-- Title Page -->
	<section class="bg-title-page p-t-50 p-b-40 flex-col-c-m" style="background-image: url(<?php echo base_url() ;?>assets/front/images/bidan.jpg);">
		<h2 style="color: grey" class="l-text2 t-center">
			ARKAMAYA MEDICAL
		</h2>
		<p style="color: grey" class="m-text13 t-center">
			Layanan Medical
		</p>
	</section>


	<!-- Content page -->
	<section class="bgwhite p-t-55 p-b-65">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-4 col-lg-3 p-b-50">
				</div>

				<div class="col-sm-6 col-md-8 col-lg-9 p-b-50">
					<!--  -->


					<!-- Product -->
                    
					<div class="row">
						<div class="col-sm-12 col-md-6 col-lg-4 p-b-50">
							<!-- Block2 -->
			
                	<form>
                         <div class="form-group">
		                  <input type="hidden" class="form-control" id="exampleInputEmail1" placeholder="Masukan Nama Kamera"  name="id_user" value=""> 
		                </div>

                        <div class="form-group">
		                  <label for="exampleInputEmail1">Gejala Utama</label>
		                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukan Gejala Utama"  name="gejala" value="">
		                </div>

		                <div class="form-group">
		                  <label for="exampleInputEmail1">Deskripsi gejala</label>
		                  <input type="textarea" class="form-control" id="exampleInputEmail1" placeholder="Masukan Gejala"  name="gejala" value="">
		                </div>

                        <div class="form-group">
		                  <label for="exampleInputEmail1">Alamat Lengkap</label>
		                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukan alamat"  name="alamat" value="<?php  ?>">
		                </div>

		                <div class="form-group">
		                  <label for="exampleInputEmail1">Nama Obat</label>
		                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukan Nama Obat"  name="obat" value="<?php  ?>">
		                </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Jenis Obat</label>
                      <?php $style='class="form-control input-sm"';
                      echo form_dropdown('jenis_obat',$jenis_obat,'',$style);

                      ?>
                    </div>

		                <div class="form-group">
		                  <label for="exampleInputEmail1">Nama Barang</label>
		                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukan Nama Barang"  name="barang" value="<?php  ?>">
		                </div>

                        <div class="form-group">
		                  <label for="exampleInputEmail1">No Telephone</label>
		                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukan No Telephone Aktif"  name="no_hp" value="<?php ?>">
		                </div>

       					 <div class="box-footer">
			                <button onclick="haruslogin()" class="btn btn-primary"><a href="<?php echo base_url('index.php/Tamu/signin'); ?>" style="color: white;">Pesan Bidan</a></button>
			             </div>
						</div>
              
						</form>


						<div class="col-sm-12 col-md-6 col-lg-4 p-b-50">
               
							
					<!-- Pagination -->
					
				</div>
			</div>
		</div>
	</section>


	<!-- Footer -->
	<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
    <div class="flex-w p-b-90">

      <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
        <h4 class="s-text12 p-b-30">
        <center>  DOKTER </center>
        </h4>

        <div>
          <p class="s-text7 w-size27">
            <center> Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
          </p>
        </div>
      </div>

      <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
        <h4 class="s-text12 p-b-30">
        <center>  BIDAN </center>
        </h4>

        <div>
          <p class="s-text7 w-size27">
            <center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
          </p>
        </div>
      </div>

      <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
        <h4 class="s-text12 p-b-30">
        <center>  PERAWAT </center>
        </h4>

        <div>
          <p class="s-text7 w-size27">
            <center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
          </p>
        </div>
      </div>
    </div>

    <div class="t-center p-l-15 p-r-15">

      <div class="t-center s-text8 p-t-20">
        Arkamaya Medical © 2019. All rights reserved. <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="<?php echo base_url(); ?>assets/front/https://colorlib.com" target="_blank">Ricka & Fiki</a>
      </div>
    </div>
  </footer>



  <!-- Back to top -->
  <div class="btn-back-to-top bg0-hov" id="myBtn">
    <span class="symbol-btn-back-to-top">
      <i class="fa fa-angle-double-up" aria-hidden="true"></i>
    </span>
  </div>

  <!-- Container Selection -->
  <div id="dropDownSelect1"></div>
  <div id="dropDownSelect2"></div>



<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/popper.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.min.js"></script>
  <script type="text/javascript">
    $(".selection-1").select2({
      minimumResultsForSearch: 20,
      dropdownParent: $('#dropDownSelect1')
    });

    $(".selection-2").select2({
      minimumResultsForSearch: 20,
      dropdownParent: $('#dropDownSelect2')
    });
  </script>
<!--===============================================================================================-->
  <script src="<?php echo base_url(); ?>assets/front/js/main.js"></script>

</body>
</html>
