<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('user/layout/head') ?>

<body class="animsition">

	<!-- Header -->
<?php $this->load->view('user/layout/header_pelanggan') ?>	
	<!-- Slide1 -->


	<!-- Banner -->

	<!-- New Product -->

	<!-- Banner2 -->
	


	<!-- Blog -->
<section class="bgwhite p-t-60 p-b-25">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-lg-9 p-b-80">
					<div class="p-r-50 p-r-0-lg">
						<div class="p-b-40">
							<div class="blog-detail-img wrap-pic-w">
								<img style="height:500px; width:1000px" src="<?php echo base_url(); ?>assets/front/images/bayibatuk.jpg" alt="IMG-BLOG">
							</div>

							<div class="blog-detail-txt p-t-33">
								<h4 class="p-b-11 m-text24">
									7 Cara Mengatasi Batuk Pada Bayi
								</h4>

								<div class="s-text8 flex-w flex-m p-b-21">
									<span>
										By Admin
										<span class="m-l-3 m-r-6">|</span>
									</span>

									<span>
										28 Dec, 2018
										<span class="m-l-3 m-r-6">|</span>
									</span>

									<span>
										Cooking, Food
										<span class="m-l-3 m-r-6">|</span>
									</span>

									<span>
										8 Comments
									</span>
								</div>

								<p class="p-b-25">
									Ketika mulai memiliki bayi, Anda sebisa mungkin menjaganya agar tidak
mudah terkena penyakit. Anda akan memberikan ASI ekslusif atau susu yang bagus, jika Anda tidak bisa memberinya ASI. Ketika memasuki usia enam bulan, Anda mulai memikirkan asupan makanan yang baik dan sehat
untuk bayi Anda agar imunitasnya terjaga. Namun, sebaik apa pun Anda menjaga kesehatannya, ada suatu waktu bayi dapat terserang sakit, seperti demam, batuk dan flu. Ketika orang dewasa terkena batuk, langkah pertama yang dilakukan adalah mencari toko obat terdekat untuk membeli obat batuk. Ketika batuk dialami oleh bayi Anda, mungkin Anda tidak bisa langsung memberinya obat. Lalu, apa yang harus dilakukan?</p>

<p class="p-b-25"><b>Batuk apa saja yang sering menyerang bayi ?</b></p>

<p class="p-b-25"> Batuk disebarkan oleh virus, dan bisa menjadi gejala adanya infeksi. Berikut ini batuk yang mesti diwaspadai:</p>
<p class="p-b-25"><b>
Batuk croup,</b> menyebabkan kesulitan bernapas, sebab laring dan jalan pernapasan ke paru-paru (trakea) membengkak. Hasilnya bayi akan mengeluarkan batuk seperti gonggongan. Gejalanya akan berupa panas, demam, adanya ingus di dalam hidung. Virus ini biasa menjangkiti anak berusia enam bulan hingga tiga tahun.</p>

<p class="p-b-25"><b>
Batuk rejan,</b> disebabkan oleh infeksi bakteri yang mempengaruhi paru-paru dan saluran udara. Batuk ini akan membuat bayi Anda terengah-engah. Biasanya gejala disertai dengan flu dan demam ringan. Setelah beberapa minggu, jika belum sembuh, akan menjadi batuk kering. Anda bisa memberi vaksin terhadap jenis batuk ini.</p>

<p class="p-b-25">
<b>Brochiolitis, </b> adalah infeksi yang biasanya dialami pada tahun pertamanya. Batuk jenis ini disebabkan oleh cuaca yang dingin. Hal ini terjadi karena saluran udara kecil ke paru-paru terinfeksi dan berlendir. Bayi menjadi kesulitan bernapas. Gejala yang muncul berupa adanya ingus di dalam hidung, batuk kering, kehilangan selera makan. Lama-lama akan mengakibatkan pilek, infeksi telinga, batuk croup, dan pneumonia.</p>

<p class="p-b-25">Selain itu, batuk pada bayi biasanya juga diikuti oleh gejala-gejala berikut:</p>
<p class="p-b-25"> - Demam </p>
<p class="p-b-25"> - Sakit tenggorokan </p>
<p class="p-b-25"> - Hidung tersumbat </p>
<p class="p-b-25"> - Mata memerah </p>
<p class="p-b-25"> - Kehilangan nafsu makan </p>
<p class="p-b-25"> - Adanya pembengkakan getah bening di bawah ketiak, leher, dan belakang kepalanya.</p>

<p class="p-b-25"><b>Bagaimana cara mengatasi batuk pada bayi?</b></p>

<p class="p-b-25"> Sebaiknya Anda tidak panik ketika bayi Anda terserang batuk, selalu perhatikan gejalanya dan coba beberapa cara berikut ini:</p>

<p class="p-b-25"><b>1. Meningkatkan cairan tubuh</b></p>

<p class="p-b-25">Cairan tambahan dapat memudahkannya untuk batuk dan bisa mengurangi lendir di hidung sehingga ia juga bisa mudah bernapas. Anda bisa memberinya air putih, susu, jus. Anda juga bisa memberinya sup ayam hangat, atau cokelat panas, yang dapat meringankan sakit tenggorokannya. Pastikan memberikannya dalam level hangat, bukan panas. Namun, hal ini hanya bisa dilakukan untuk bayi di atas usia enam bulan. Sebaiknya untuk bayi di bawah enam bulan, pemberian ASI ekstra sangat dianjurkan, karena ASI dipercaya dapat meningkatkan imunitas bayi. Selain itu, Anda juga bisa memberinya susu formula.</p>

<p class="p-b-25"><b>2. Berikan sedikit madu</b></p>

<p class="p-b-25"> Madu mengandung antioksidan, antibakteri yang baik untuk kesehatan. Selain itu madu juga mengandung vitamin C yang baik untuk sistem imun tubuh. Memberikan sedikit madu dapat meringankan batuk pada bayi. Berikan bayi Anda ½ sendok teh madu sebelum ia tidur. Namun, pengobatan madu ini hanya bisa dilakukan untuk bayi di atas satu tahun, Anda belum bisa memberikannya pada usia di bawahnya karena malah akan membuatnya sakit.</p>

<p class="p-b-25"><b>3. Menaikkan kepala bayi</b></p>

<p class="p-b-25">Ketika Anda merasa sulit bernapas atau mengalami hidung tersumbat, Anda akan mencoba tidur dengan kepala sedikit dinaikkan. Hal ini juga bisa dicoba pada bayi Anda, taruh bantal yang tidak terlalu tebal atau handuk yang sudah dilipat, di atas matras di mana kepala bayi Anda akan dibaringkan. Ini akan membantunya memudahkan bernapas.</p>

<p class="p-b-25"><b>4. Pilih makanan yang meringankan batuk</b></p>

<p class="p-b-25">Untuk bayi berusia enam bulan atau di bawahnya sebaiknya cukup fokuskan pada pemberian ASI dan susu formula. Jika bayi Anda berusia mendekati setahun ke atas, Anda bisa memilih makanan yang lembut untuk bayi Anda, seperti pudding, yogurt, dan saus apel. Jika mereka suka makanan yang hangat, Anda bisa memberikannya kaldu ayam atau pudding yang baru saja dibuat.</p>

<p class="p-b-25"><b>5. Waktu istirahat yang cukup</b></p>

<p class="p-b-25">Pastikan bayi Anda mendapat istirahat yang cukup. Batuk membuatnya menjadi kehilangan nafsu makan, bisa menyebabkannya gelisah dan sulit beristirahat. Coba menidurkannya ketika waktunya beristirahat; jika ia mudah tertidur di gendongan Anda, sebaiknya Anda tidak membaringkannya hingga ia tertidur. Jika ia mudah tidur di ranjangnya, Anda bisa baringkan di ranjangnya.</p>

<p class="p-b-25"><b>6. Berikan obat penurun demam</b></p>

<p class="p-b-25">Anda juga bisa memberikan paracetamol untuk bayi, jika bayi Anda berusia 37 pekan dan beratnya lebih dari 4 kg. Anda juga bisa memberikan ibuprofen untuk bayi, jika usianya lebih dari tiga bulan dan beratnya paling tidak mencapai 5 kg.</p>

<p class="p-b-25"><b>7. Memberikan uap panas</b></p>
<p class="p-b-25">Uap panas dapat meringankan hidung tersumbat dan batuknya. Anda bisa memasak air panas, lalu taruh di ember kecil atau baskom, dekatkan dengan bayi Anda, tapi pastikan bayi Anda tidak terkena air panas tersebut. Anda juga bisa duduk di kamar mandi dengan bayi Anda, dan biarkan pancuran air hangat mengalir. Uap panas akan melancarkan saluran udara pernapasannya.</p>
<p class="p-b-25"><b>Haruskah saya membawanya ke dokter?</b></p>
<p class="p-b-25">- Batuknya tak kunjung reda setelah lima hari</p>
<p class="p-b-25">- Batuk bayi Anda semakin memburuk, Anda bisa perhatikan dari suaranya</p>
<p class="p-b-25">- Jika bayi Anda di bawah tiga bulan, temperaturnya mencapai 38 derajat C. Jika usianya di bawah enam bulan, temperaturnya mencapai 39 derajat C. Saat itu, Anda harus membawanya ke dokter</p>
<p class="p-b-25">- Memiliki permasalahan pernapasannya</p>
<p class="p-b-25">- Dahak yang keluar berwarna hijau, cokelat dan kuning</p>
							</div>
						</div>

						<!-- Leave a comment -->
						<form class="leave-comment">
							<h4 class="m-text25 p-b-14">
								Leave a Comment
							</h4>

							<p class="s-text8 p-b-40">
								Your email address will not be published. Required fields are marked *
							</p>

							<textarea class="dis-block s-text7 size18 bo12 p-l-18 p-r-18 p-t-13 m-b-20" name="comment" placeholder="Comment..."></textarea>

							<div class="bo12 of-hidden size19 m-b-20">
								<input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="name" placeholder="Name *">
							</div>

							<div class="bo12 of-hidden size19 m-b-20">
								<input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="email" placeholder="Email *">
							</div>

							<div class="bo12 of-hidden size19 m-b-30">
								<input class="sizefull s-text7 p-l-18 p-r-18" type="text" name="website" placeholder="Website">
							</div>

							<div class="w-size24">
								<!-- Button -->
								<button class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
									Post Comment
								</button>
							</div>
						</form>
					</div>
				</div>

				
			</div>
		</div>
	</section>

	<!-- Instagram -->
	<section class="instagram p-t-20">
		<div class="sec-title p-b-52 p-l-15 p-r-15">
			
		</div>

		<div class="flex-w">
			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-03.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-07.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-09.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-13.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>

			<!-- Block4 -->
			<div class="block4 wrap-pic-w">
				<img src="<?php echo base_url(); ?>assets/front/images/gallery-15.jpg" alt="IMG-INSTAGRAM">

				<a href="<?php echo base_url(); ?>assets/front/#" class="block4-overlay sizefull ab-t-l trans-0-4">
					<span class="block4-overlay-heart s-text9 flex-m trans-0-4 p-l-40 p-t-25">
						<i class="icon_heart_alt fs-20 p-r-12" aria-hidden="true"></i>
						<span class="p-t-2">39</span>
					</span>

					<div class="block4-overlay-txt trans-0-4 p-l-40 p-r-25 p-b-30">
						<p class="s-text10 m-b-15 h-size1 of-hidden">
							Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit amet enim orci. Nam eget metus elit.
						</p>

						<span class="s-text9">
							Photo by @nancyward
						</span>
					</div>
				</a>
			</div>
		</div>
	</section>

	<!-- Shipping -->

	<!-- Footer -->
	<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
		<div class="flex-w p-b-90">
			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<center>DOKTER</center>
				</h4>

				<div>
					<p class="s-text7 w-size27">
						<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
					</p>

				</div>
			</div>

			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<center>BIDAN</center>
				</h4>

				<div>
					<p class="s-text7 w-size27">
						<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
					</p>

				</div>
			</div>

			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<center>PERAWAT</center>
				</h4>

				<div>
					<p class="s-text7 w-size27">
						<center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
					</p>

				</div>
			</div>
		</div>

		<div class="t-center p-l-15 p-r-15">

			 <div class="t-center s-text8 p-t-20">
        Arkamaya Medical © 2019. All rights reserved. <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="<?php echo base_url(); ?>assets/front/https://colorlib.com" target="_blank">Ricka & Fiki</a>
      </div>
    </div>
	</footer>



	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection1 -->
	<div id="dropDownSelect1"></div>



<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.min.js"></script>
	<script type="text/javascript">
		$(".selection-1").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/slick/slick.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/js/slick-custom.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/sweetalert/sweetalert.min.js"></script>
	<script type="text/javascript">
		$('.block2-btn-addcart').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to cart !", "success");
			});
		});

		$('.block2-btn-addwishlist').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");
			});
		});
	</script>

<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>assets/front/js/main.js"></script>

</body>
</html>
