<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('user/layout/head') ?>
<body class="animsition">

	<!-- Header -->
<?php $this->load->view('user/layout/header_pelanggan') ?>

<!--  <style type="text/css">
    #map {
      height: 480px;
      width: 100%;
      border: solid thin #333;
      margin-top: 20px;
    }
 
    #map img { 
      max-width: none;
    }
 
    #mapCanvas label { 
      width: auto; display:inline; 
    } 
  </style>
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
  <script type="text/javascript">
    var map;
    var geocoder;
    var bounds = new google.maps.LatLngBounds();
    var markersArray = [];
      
    // setting marker untuk marker asal dan tujuan
    var destinationIcon = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=D|FF0000|000000";
    var originIcon = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=O|FFFF00|000000";
 
    // tentukan terlebih dahulu letak petanya 
    function initialize() {
      var opts = {
        center: new google.maps.LatLng(-7.25009,112.744331),
        zoom: 5,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      map = new google.maps.Map(document.getElementById('map'), opts);
      geocoder = new google.maps.Geocoder();
 
      // setting agar texfield pada kolom asal dan juga tujuan dapat memanggil fungsi autocomplete
      var asal = new google.maps.places.Autocomplete((document.getElementById('origins')),{ types: ['geocode'] });
      var tujuan = new google.maps.places.Autocomplete((document.getElementById('destinations')),{ types: ['geocode'] });
    }
 
    /*      
    menghitung jarak dari data yg dikirim dari form
    disini saya setting untuk mode DRIVING dan menggunakan jalan raya atau juga tol,
    jika ingin mengganti konfigurasinya, silahkan ganti false dengan true
    */
    function calculateDistances() {
      var service = new google.maps.DistanceMatrixService();
      service.getDistanceMatrix(
      { 
        origins: [document.getElementById("origins").value],
        destinations: [document.getElementById("destinations").value],
        travelMode: google.maps.TravelMode.DRIVING, 
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false
      }, callback);
    }
      
    // responde dari Googlemaps Distance Matrix akan diolah dan di kirim ke output HTML
    function callback(response, status) {
      if (status != google.maps.DistanceMatrixStatus.OK) {
        alert('Error was: ' + status);
      } else {
        var origins = response.originAddresses;
        var destinations = response.destinationAddresses;
        deleteOverlays();
 
        for (var i = 0; i < origins.length; i++) {
          var results = response.rows[i].elements;
          addMarker(origins[i], false);
          for (var j = 0; j < results.length; j++) {
            addMarker(destinations[j], true);
          }
    
          /*          
            disini perhitungan tarif, pertama hilangkan dulu 'km'
            dan ubah tanda desimal koma dengan titik. 
          */
          var str = results[0].distance.text;
          var distance = str.replace(' km', '');
          var distance = distance.replace(',','.');
 
          /*          
            jumlah kilometer dikalikan dengan 500 
            setelah itu hasilnya kita konversikan kedalam format kurs rupiah
          */
          var tarif = "Rp."+ formatNumber(distance * 500)+",-"; 
          document.getElementById("billing").value = tarif;
          document.getElementById("distance").value = results[0].distance.text;
    
        }
      }
    }
    // fungsi sederhana untuk mengkonversi bilangan bulat menjadi format kurs rupiah
    function formatNumber (num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
    }
 
    // menampilkan marker untuk origin dan juga destination
    function addMarker(location, isDestination) {
      var icon;
      if (isDestination) {
        icon = destinationIcon;
      } else {
        icon = originIcon;
      }
      geocoder.geocode({'address': location}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          bounds.extend(results[0].geometry.location);
          map.fitBounds(bounds);
          var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location,
            icon: icon
          });
          markersArray.push(marker);
        } else {
          alert("Terjadi kesalahan: "
            + status);
        }
      });
    }
      
    // menghapus koordinat marker sebelumnya dan menggantinya dengan koordinat yang baru
    function deleteOverlays() {
      if (markersArray) {
        for (i in markersArray) {
          markersArray[i].setMap(null);
        }
        markersArray.length = 0;
      }
    }
  </script> -->



  <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCTzjb-g5lJmButPqyNn9y6Q1x8d3JPGyo"></script>
<script type="text/javascript">
function init(){
var service = new google.maps.DirectionsService;
var view = new google.maps.DirectionsRenderer;
 
var info_window = new google.maps.InfoWindow();
var zoom = 5;
 
var pos = new google.maps.LatLng(-3.050444,116.323242);
var options = {
'center': pos,
'zoom': zoom,
'mapTypeId': google.maps.MapTypeId.ROADMAP
};
 
var map = new google.maps.Map(document.getElementById('maps'), options);
view.setMap(map);
info_window = new google.maps.InfoWindow({
'content': 'loading...'
});
 
var result = function(){
lihat(service, view);
}
 
document.getElementById('lihat').addEventListener('click', result)
}
 
function lihat(service, view){
var start = document.getElementById('start').value;
var end = document.getElementById('end').value;
 
var request = {
origin: start,
destination: end,
travelMode: google.maps.TravelMode.DRIVING
};
 
service.route(request, function(response, status){
if(status == google.maps.DirectionsStatus.OK){
view.setDirections(response);
}else{
window.alert('Directions request failed due to ' + status);
}
});
}
 
google.maps.event.addDomListener(window, 'load', init);
</script>
	<!-- Title Page -->
	<!-- Title Page -->
	<section class="bg-title-page p-t-50 p-b-40 flex-col-c-m" style="background-image: url(<?php echo base_url() ;?>assets/front/images/banner-dokter.jpg);">
		<h2 style="color: grey" class="l-text2 t-center">
			ARKAMAYA MEDICAL
		</h2>
		<p style="color: grey" class="m-text13 t-center">
			Layanan medical
		</p>
	</section>


	<!-- Content page -->
	<section class="bgwhite p-t-55 p-b-65">
		<div class="container">
			<div class="row">
					<!--  -->


					<!-- Product -->
                    
<div id="maps" style="width: 1200px; height: 400px;"></div>
<input type="text" id="start" size="50" placeholder="Lokasi sekarang">
<input type="text" id="end" size="50" placeholder="Tujuan">
<button id="lihat">lihat</button><br><br><br>

<div class="block1-wrapbtn w-size2">
              <!-- Button -->
              <a href="<?php echo base_url('index.php/Pelanggan/pesan_perawat'); ?>" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4">
              Pesan Perawat
              </a>
            </div>               
							
					<!-- Pagination -->
					
				</div>
			</div>
		</div>
	</section>
  





	<!-- Footer -->
	<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
    <div class="flex-w p-b-90">

      <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
        <h4 class="s-text12 p-b-30">
        <center>  DOKTER </center>
        </h4>

        <div>
          <p class="s-text7 w-size27">
            <center> Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
          </p>
        </div>
      </div>

      <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
        <h4 class="s-text12 p-b-30">
        <center>  BIDAN </center>
        </h4>

        <div>
          <p class="s-text7 w-size27">
            <center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
          </p>
        </div>
      </div>

      <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
        <h4 class="s-text12 p-b-30">
        <center>  PERAWAT </center>
        </h4>

        <div>
          <p class="s-text7 w-size27">
            <center>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</center>
          </p>
        </div>
      </div>
    </div>

    <div class="t-center p-l-15 p-r-15">

      <div class="t-center s-text8 p-t-20">
        Arkamaya Medical © 2019. All rights reserved. <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="<?php echo base_url(); ?>assets/front/https://colorlib.com" target="_blank">Ricka & Fiki</a>
      </div>
    </div>
  </footer>



  <!-- Back to top -->
  <div class="btn-back-to-top bg0-hov" id="myBtn">
    <span class="symbol-btn-back-to-top">
      <i class="fa fa-angle-double-up" aria-hidden="true"></i>
    </span>
  </div>

  <!-- Container Selection -->
  <div id="dropDownSelect1"></div>
  <div id="dropDownSelect2"></div>



<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/popper.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front/vendor/select2/select2.min.js"></script>
  <script type="text/javascript">
    $(".selection-1").select2({
      minimumResultsForSearch: 20,
      dropdownParent: $('#dropDownSelect1')
    });

    $(".selection-2").select2({
      minimumResultsForSearch: 20,
      dropdownParent: $('#dropDownSelect2')
    });
  </script>
<!--===============================================================================================-->
  <script src="<?php echo base_url(); ?>assets/front/js/main.js"></script>

</body>
</html>
