<!DOCTYPE html>
<html>
<?php $this->load->view('admin/layout/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
   
  <?php
  if( !empty($jml) ) {
        foreach($jml as $j){
            $jumlah_transaksi[] = $j->jumlah;
            $tanggal[] = $j->tanggal;
        }
      }elseif (empty($result)) {
       $jumlah_transaksi[] = 0;
        $tanggal[] = 0;
      } 
    ?>

  <?php
  foreach($bln->result_array() as $b){
  }
  ?>

<div class="wrapper">
<?php $this->load->view('admin/layout/header') ?>
    <!-- Left side column. contains the logo and sidebar -->
     <?php $this->load->view('admin/layout/leftbar') ?> 

     <!-- Content Wrapper. Contains page content -->
     <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>

          <?php foreach($profile->result_array() as $a) { ?>
          Hallo <?php echo $a['username'];?>! 
          <?php } ?>

          <?php echo $this->session->flashdata('login');?>
          <small>Enjoy your work time !!!</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->

        <div class="row">

          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
              <?php foreach ($jumlah_order->result_array() as $a) { ?>
                <div class="inner">
                  <h3><?php echo $a['jumlah'];?><sup style="font-size: 20px"></sup></h3>
                  <p>Jumlah Transaksi Hari ini</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>

                <div style="color: black;">
                  <button data-toggle="modal" data-target="#popup<?php echo $a['id_laporan'];?>"> 
                    Lihat Transaksi
                  </button>
                </div>
              <?php } ?>

              </div>
          </div>

          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-orange">
              <?php foreach ($jumlah_pemasukan->result_array() as $a) { ?>
              <div class="inner">
                <h3>Rp. <?php echo $a['jumlah'];?><sup style="font-size: 20px"></sup></h3>

                <p>Total pemasukan hari ini</p>
                <?php } ?>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <?php foreach ($jumlah_user->result_array() as $a) { ?>
              <div class="inner">
                <h3><?php echo $a['jumlah'];?></h3>

                <p>Jumlah pengguna</p>
              </div>
              <?php } ?>
              <div class="icon">
                <i class="ion ion-person"></i>
              </div>
              <a href="<?php echo base_url('index.php/admin/Adminn/sendMail')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->

          

          <!-- ./col -->
        </div>

          <!-- ./col -->
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-6 connectedSortable">

          <!-- AREA CHART -->         

         <!-- quick email widget -->
         <div class="box box-info">
          <div class="box-header">
            <i class="fa fa-envelope"></i>

            <h3 class="box-title">Quick Email</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
              <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
              title="Remove">
              <i class="fa fa-times"></i></button>
            </div>
            <!-- /. tools -->
          </div>
          <div class="box-body">
            <form action="#" method="post">
              <div class="form-group">
                <input type="email" class="form-control" name="emailto" placeholder="Email to:">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" placeholder="Subject">
              </div>
              <div>
                <textarea class="textarea" placeholder="Message"
                style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </div>
            </form>
          </div>
          <div class="box-footer clearfix">
            <button type="button" class="pull-right btn btn-default" id="sendEmail">Send
              <i class="fa fa-arrow-circle-right"></i></button>
            </div>
          </div>



        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
  <section class="col-lg-6 connectedSortable">

          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Chart Jumlah transaksi harian</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
               <canvas id="canvas" width="1000" height="280"></canvas>
             </div>
           </div>
           <!-- /.box-body -->
         </div>
         <!-- /.box -->

          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Bar Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div id="bar-chart" style="height: 300px;"></div>
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->
               <?php
               foreach ($jumlah_order->result_array() as $dp) {
                ?> 
                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup<?php echo $dp['id_laporan'];?>" class="modal fade">
                <?php } ?>
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                        <h4 class="modal-title">Transaksi Hari ini</h4>
                      </div>
                      <table id="example1" class="table table-bordered table-striped">
                        <tr>
                          <th>Nama Pasien</th>
                          <th>Nama Medis</th>
                          <th>Tanggal</th>
                        </tr>
                        <tr>
                          <?php
                          $a = 1;
                          foreach ($tampil_order->result_array() as $dp) {
                          $no = $a++;
                            ?>
                        <td><?php echo $no;?></td>
                        <td><?php echo $dp['id_laporan'];?></td>
                        <td><?php echo $dp['tanggal'];?></td>
                        </tr>

                        <?php } ?>
                      </table>


                    </div>
                  </div>
                </div>

          </section>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
        <!-- /.row -->
      </div>
    </div>
    <!-- /.box -->

  </section>
  <!-- right col -->
</div>
<!-- /.row (main row) -->

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('admin/layout/footer') ?>
<!-- Control Sidebar -->
<?php $this->load->view('admin/layout/aside') ?>
</div>
<!-- ./wrapper -->



<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/back/bower_components') ?>/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('assets/back/bower_components') ?>/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/back/bower_components') ?>/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('assets/back/bower_components') ?>/raphael/raphael.min.js"></script>
<script src="<?php echo base_url('assets/back/bower_components') ?>/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/back/bower_components') ?>/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('assets/back/plugins') ?>/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url('assets/back/plugins') ?>/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/back/bower_components') ?>/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('assets/back/bower_components') ?>/moment/min/moment.min.js"></script>
<script src="<?php echo base_url('assets/back/bower_components') ?>/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url('assets/back/bower_components') ?>/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('assets/back/plugins') ?>/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('assets/back/bower_components') ?>/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/back/bower_components') ?>/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/back/dist') ?>/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('assets/back/dist') ?>/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/back/dist') ?>/js/demo.js"></script>
<!-- FLOT CHARTS -->
<script src="<?php echo base_url('assets/back');?>/bower_components/Flot/jquery.flot.js"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="<?php echo base_url('assets/back');?>/bower_components/Flot/jquery.flot.resize.js"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="<?php echo base_url('assets/back');?>/bower_components/Flot/jquery.flot.pie.js"></script>
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<script src="<?php echo base_url('assets/back');?>/bower_components/Flot/jquery.flot.categories.js"></script>

<script type="text/javascript" src="<?php echo base_url().'assets/back/bower_components/chart.js/Chart.min.js'?>"></script>
<script>

  var lineChartData = {
    labels : <?php echo json_encode($tanggal);?>,
    datasets : [

    {
      fillColor: "rgba(60,141,188,0.9)",
      strokeColor: "rgba(60,141,188,0.8)",
      pointColor: "#3b8bba",
      pointStrokeColor: "#fff",
      pointHighlightFill: "#fff",
      pointHighlightStroke: "rgba(152,235,239,1)",
      data : <?php echo json_encode($jumlah_transaksi);?>
    }
    ]                 
  }

  var myLine = new Chart(document.getElementById("canvas").getContext("2d")).Line(lineChartData);

        //DONUT CHART


      </script>
<!-- Chart -->

<script>

  $(function () {
    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $('#areaChart').get(0).getContext('2d')
    // This will get the first returned node in the jQuery collection.
    var areaChart       = new Chart(areaChartCanvas)

    var areaChartData = {
      labels  : <?php echo json_encode($tanggal1);?>,
      datasets: [
        {
          label               : 'Digital Goods',
          fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : <?php echo json_encode($jumlah_transaksi1);?>
        }
      ]
    }

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale               : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)

      scaleShowVerticalLines  : true,
      //Boolean - Whether the line is curved between points
      bezierCurve             : true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension      : 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot                : true,
      //Number - Radius of each point dot in pixels
      pointDotRadius          : 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth     : 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius : 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke           : true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth      : 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill             : true,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio     : true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive              : true
    }

    //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions)

  
  })
</script>
    
<script type="text/javascript">
    /*
     * BAR CHART
     * ---------
     */

    var bar_data = {
      label : 'Jumlah transaksi bulanan',
      data : [['January', <?php echo $b['jan'];?>], ['February',<?php echo $b['feb'];?>], ['March', <?php echo $b['mar'];?>], ['April', <?php echo $b['apr'];?>], ['May', <?php echo $b['mei'];?>], ['June', <?php echo $b['jun'];?>], ['Juli', <?php echo $b['jul'];?>], ['Agu', <?php echo $b['agu'];?>], ['Sept', <?php echo $b['sep'];?>], ['Okt', <?php echo $b['okt'];?>], ['Nov', <?php echo $b['nov'];?>], ['Des', <?php echo $b['des'];?>]],
      color: '#3c8dbc'
    }
    $.plot('#bar-chart', [bar_data], {
      grid  : {
        borderWidth: 1,
        borderColor: '#f3f3f3',
        tickColor  : '#f3f3f3'
      },
      series: {
        bars: {
          show    : true,
          barWidth: 0.5,
          align   : 'center'
        }
      },
      xaxis : {
        mode      : 'categories',
        tickLength: 0
      }
    })
    /* END BAR CHART */
</script>
</body>
</html>
