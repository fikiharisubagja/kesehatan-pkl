<!DOCTYPE html>
<html>
<?php $this->load->view('admin/layout/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('admin/layout/header') ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/layout/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Obat
        <small>Arkamaya Medical</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
                      <button type="button" class="btn btn-app" data-toggle="modal" data-target="#modal-default">
                         <i class="fa fa-edit"></i> Tambah Data
                      </button>
              
              <a href="<?php echo base_url('index.php/admin/Adminn/excel_obat')?>" class="btn btn-app">
               <i class="fa fa-inbox"></i> Cetak Excel
              </a>

              <a href="<?php echo base_url('index.php/admin/Adminn/pdf_obat/')?>" class="btn btn-app">
               <i class="fa fa-inbox"></i> Cetak PDF
              </a>



            <div class="box-body table-responsive">
             
             <?php echo $this->session->flashdata('pesan'); ?>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Obat</th>
                   <th>Harga Satuan</th>
                  <th>Aksi</th>
                  
                </tr>
                </thead>
                <tbody>
                 <?php
                 $a = 1;
                foreach ($data_obat->result_array() as $dp) {
                $no = $a++;
                ?>
                 <tr>
                    <td><?php echo $no;?></td>
                    <td><?php echo $dp['nama_obat'];?></td>
                    <td><?php echo $dp['harga_satuan_obat'];?></td>
                    	<td>
												
                          <button data-toggle="modal" data-target="#edit-data<?php echo $dp['id_obat'];?>" class="btn btn-success">Ubah</button>
							
													<a href="<?php echo base_url()."index.php/admin/Adminn/do_delete_obat/".$dp['id_obat'];?>" onClick="return confirm('Anda yakin akan menghapus data ini???');" class="btn btn-danger btn-md">Delete	
													</a>
												

												
											</td>
                </tr>
                <?php
                    }
                ?>
                </tbody>
                              
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
<?php
               
foreach ($data_obat->result_array() as $dp) {
?> 
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="edit-data<?php echo $dp['id_obat'];?>" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Ubah Data</h4>
              </div>
              <form class="form-horizontal" action="<?php echo base_url('index.php/admin/Adminn/do_edit_obat')?>" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-body">
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Id</label>
                            <div class="col-lg-10">
                              <input type="hidden" id="id_obat" name="id_obat" value="<?php echo $dp['id_obat']; ?>">
                                <input type="text" class="form-control" value="<?php echo $dp['id_obat']; ?>" id="id_obat" name="id_obat" placeholder="Tuliskan Id">
                            </div>
                            <label class="col-lg-2 col-sm-2 control-label">Nama</label>
                            <div class="col-lg-10">
                            <input type="text" class="form-control" value="<?php echo $dp['nama_obat']; ?>" id="nama_obat" name="nama_obat" placeholder="Tuliskan Nama">
                            </div>
                            <label class="col-lg-2 col-sm-2 control-label">Harga</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" value="<?php echo $dp['harga_satuan_obat']; ?>" id="harga_satuan_obat" name="harga_satuan_obat" placeholder="Tuliskan Harga">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" type="submit"> Simpan&nbsp;</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"> Batal</button>
                    </div>
                  </form>
              </div>
          </div>
      </div>
  <?php } ?>
  
      <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
            <?php echo form_open_multipart('admin/Adminn/do_insert_obat');?>
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah obat</h4>
              </div>
              <div class="modal-body">
                <div class="box-body">
              
                <div class="box-body">

                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Obat</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukan Nama Obat"  name="nama_obat" required="">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Harga Satuan</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukan Harga Satuan Obat"  name="harga_satuan_obat" required="">
                </div>

                </div>
              </div>
              <div class="modal-footer">
                <button type="cancel" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Tambah Data</button>
              </div>
              </form>


            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $this->load->view('admin/layout/footer') ?>
<?php $this->load->view('admin/layout/aside') ?>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/back/bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url ('assets/back/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url ('assets/back/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url ('assets/back/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url ('assets/back/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url ('assets/back/bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url ('assets/back/dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url ('assets/back/dist/js/demo.js') ?> "></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
