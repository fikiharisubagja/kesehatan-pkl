<?php
//memanggil library FPDF
// intance object dan memberikan pengaturan halaman PDF
$pdf = new FPDF('l','mm','A5');
// membuat halaman baru
$pdf->AddPage();
// Setting jenis font yang akan digunakan
$pdf->SetFont('Arial','B',16);
// mencetak string
$pdf->Cell(190,7,'ARKAMAYA MEDICAL',0,1);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(190,7,'DATA OBAT',0,1);

// memberikan space kebawah agar tidak terlalu rapat
$pdf->Cell(10,7,'',0,1);

$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,6,'ID Obat',1,0);
$pdf->Cell(85,6,'Nama Obat',1,0);
$pdf->Cell(27,6,'Kandungan',1,0);
$pdf->Cell(27,6,'Indikasi',1,0);
$pdf->Cell(27,6,'Dosis',1,0);
$pdf->Cell(27,6,'Stok',1,1);

$pdf->SetFont('Arial','',10);

include 'koneksi.php';
$obat = mysqli_query($connect, "select * from obat");
while ($row = mysqli_feth_array($obat)){
	$pdf->Cell(20,6,$row['id_obat'],1,0);
	$pdf->Cell(85,6,$row['nama_obat'],1,0);
	$pdf->Cell(27,6,$row['kandungan'],1,0);
	$pdf->Cell(27,6,$row['indikasi'],1,0);
	$pdf->Cell(27,6,$row['dosis'],1,0);
	$pdf->Cell(27,6,$row['stok'],1,1);
}

$pdf->Output();
?>