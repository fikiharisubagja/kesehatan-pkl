<!DOCTYPE html>
<html>
<?php $this->load->view('admin/layout/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php $this->load->view('admin/layout/header') ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->load->view('admin/layout/leftbar') ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
        Total Transaksi
         
        </h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('assets/back/');?>#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="<?php echo base_url('assets/back/');?>#">Examples</a></li>
          <li class="active">Invoice</li>
        </ol>
      </section>
      <!-- Main content -->
      <section class="invoice">

        <div class="row">
          <div class="input-group date" data-provide="datepicker">
          <form class="form-horizontal" action="<?php echo base_url('index.php/admin/Adminn/printTotal')?>" method="post">
           <input type="date" name="start_date" id="end_date">
           <input type="date" name="end_date" id="end_date">
           <button type="submit">Submit</button>
          </form>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

      </section>
      <!-- /.content -->
      <div class="clearfix"></div>
    </div>  <!-- /.content-wrapper -->
    

<?php $this->load->view('admin/layout/footer') ?>
<?php $this->load->view('admin/layout/aside') ?>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/back/bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url ('assets/back/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url ('assets/back/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url ('assets/back/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url ('assets/back/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url ('assets/back/bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url ('assets/back/dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url ('assets/back/dist/js/demo.js') ?> "></script>
<!-- page script -->
<script src="<?php echo base_url ('assets/back/bower_components/datepicker/js/bootstrap-datepicker.js') ?>"></script>
<script src="<?php echo base_url ('assets/back/bower_components/datepicker/dist/js/bootstrap-datepicker.js') ?>"></script>
<script src="<?php echo base_url ('assets/back/bower_components/datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
<script type="text/javascript" language="javascript" >
$(document).ready(function(){
 
 $('.input-daterange').datepicker({
  todayBtn:'linked',
  format: "yyyy-mm-dd",
  autoclose: true
 });

 fetch_data('no');

 function fetch_data(is_date_search, start_date='', end_date='')
 {
  var dataTable = $('#order_data').DataTable({
   "processing" : true,
   "serverSide" : true,
   "order" : [],
   "ajax" : {
    url:"fetch.php",
    type:"POST",
    data:{
     is_date_search:is_date_search, start_date:start_date, end_date:end_date
    }
   }
  });
 }

 $('#search').click(function(){
  var start_date = $('#start_date').val();
  var end_date = $('#end_date').val();
  if(start_date != '' && end_date !='')
  {
   $('#order_data').DataTable().destroy();
   fetch_data('yes', start_date, end_date);
  }
  else
  {
   alert("Both Date is Required");
  }
 }); 
 
});
</script>
<script type="text/javascript">
  function logout(){
    var txt;
    if(confirm("Yakin logout?"))
    {
      txt="Yes";
    }else{
      txt="No";
    }
    document.getElemetById("demo").innerHTML = txt;
  }
</script>

</body>
</html>