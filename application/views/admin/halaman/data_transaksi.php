<!DOCTYPE html>
<html>
<?php $this->load->view('admin/layout/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('admin/layout/header') ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/layout/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Transaksi
        <small>Arkamaya Medical</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
              
                <button type="submit" class="btn btn-app" >
               <i class="fa fa-inbox"></i> Export
              </button>
            <div class="box-body table-responsive">             
             
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Pasien</th>
                  <th>Nama Dokter</th>
                  <th>Nama Obat</th>
                  <th>Jumlah Obat</th>
                  <th>Nama Tambahan</th>
                  <th>Alamat</th>
                  <th>alamat detail</th>
                  <th>jenis medis</th>
                  <th>dekripsi gejala</th>
                  <th>Harga medis</th>
                  <th>Status</th>
                  <th>Total bayar</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                 <?php
                 $a = 1;
                foreach ($data_transaksi->result_array() as $dp) {
                $no = $a++;
                ?>
                 <tr>
                    <td><?php echo $no;?></td>
                    <td><?php echo $dp['id_user'];?></td>
                    <td><?php echo $dp['id_medis'];?></td>
                    <td><?php echo $dp['id_obat'];?></td>
                    <td><?php echo $dp['jumlah_obat'];?></td>
                    <td><?php echo $dp['id_tambah'];?></td>
                    <td><?php echo $dp['alamat'];?></td>
                    <td><?php echo $dp['alamat_detail'];?></td>
                    <td><?php echo $dp['jenis_medis'];?></td>
                    <td><?php echo $dp['des_gejala'];?></td>
                    
                    <td>
                    <?php if ($dp['harga_bidan'] > 0) {
                      echo $dp['harga_bidan'];
                    }elseif($dp['harga_dokter'] > 0){
                      echo $dp['harga_dokter'] ;
                    }elseif ($dp['harga_perawat'] > 0) {
                     echo $dp['harga_perawat'];
                    }else{
                      echo "";
                    }

                    ?>
                      
                    </td>

                    <td><?php switch ($dp['status']) {
  
                          case 'sedang mencari medis':
                            echo '<span class="label label-danger">mencari medis</a>';
                            break;
                          case 'diproses':
                            echo '<span class="label label-warning">diproses</span>';
                            break;
                          case 'selesai':
                            echo '<span class="label label-success">selesai</span>';
                            break;
                     
                        } 

                        ?>
                       </td>
                    <td><?php echo $dp['total_bayar'];?></td>										
                     <td>                               
													
                         
													<a href="<?php echo base_url()."index.php/admin/Adminn/do_delete_transaksi/".$dp['id_transaksi'];?>" onClick="return confirm('Anda yakin akan menghapus data ini???');" class="btn btn-danger btn-md">Delete	
													</a>
												

												
											</td>
                </tr>
                <?php
               
                    }
                ?>
                </tbody>
                              
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $this->load->view('admin/layout/footer') ?>

  <!-- Control Sidebar -->
 <?php $this->load->view('admin/layout/aside') ?>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/back/bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url ('assets/back/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url ('assets/back/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url ('assets/back/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url ('assets/back/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url ('assets/back/bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url ('assets/back/dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url ('assets/back/dist/js/demo.js') ?> "></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
