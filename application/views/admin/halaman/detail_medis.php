<!DOCTYPE html>
<html>
<?php $this->load->view('admin/layout/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php $this->load->view('admin/layout/header') ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->load->view('admin/layout/leftbar') ?>
 <script type="text/javascript">
      function konfirmasi() {
        return confirm('Apakah anda yakin?');
      }
    </script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">User profile</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
    <?php echo $this->session->flashdata('notif'); ?>

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <?php foreach($detail->result_array() as $a) { ?>
          <div class="box box-primary">
            <div class="box-body box-profile">
            <?php $image = $a['photo_medis'];
            if (empty($image)) $image = "user.png";
            ?>
              <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url('libs/medis');?>/<?php echo $image?>" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo $a['nama_lengkap'];?> ( <?php echo $a['usia'];?> thn)</h3>

              <p class="text-muted text-center"><?php echo $a['username'];?> || <?php echo $a['status_medis'];?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Menerima orderan</b> <a class="pull-right">1,322</a>
                </li>
                <li class="list-group-item">
                  <b>Menolak orderan</b> <a class="pull-right">543</a>
                </li>
                <li class="list-group-item">
                  <b>Performa</b> <a class="pull-right">60%</a>
                </li>
                <li class="list-group-item">
                  <b>Pendapatan</b> <a class="pull-right">Rp. </a>
                </li>
              </ul>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <?php } ?>

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Identitas</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Bagian Tugas</strong>

              <p class="text-muted">
                <?php echo $a['bagian_tugas'];?>
              </p>

              <strong><i class="fa fa-phone margin-r-5"></i> No Telp</strong>

              <p class="text-muted"><?php echo $a['no_hp'];?></p>
              
              <strong><i class="fa fa-envelope-o margin-r-5"></i> Email</strong>

              <p class="text-muted" name=""><?php echo $a['email'];?></p>
              
              <strong><i class="fa fa-map-marker margin-r-5"></i> Alamat</strong>

              <p class="text-muted"><?php echo $a['alamat'];?></p>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#dokumen" data-toggle="tab">Dokumen</a></li>
              <li><a href="#verifikasi" data-toggle="tab">Verifikasi</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="dokumen">
                <!-- Post -->
                <div class="post">
                <b>
                <h3>Dokumen Medis</h3><br>
                 <div class="user-block">SIP</div>
                  <!-- /.user-block -->
                  <a href="<?php echo base_url(); ?>libs/medis/<?php echo $a['sip'];?>">
                  <?php echo $a['sip'];?>
                  </a>
                  <hr>

                  <div class="user-block">STR</div>
                  <!-- /.user-block -->
                  <a href="<?php echo base_url(); ?>libs/medis/<?php echo $a['str'];?>">
                  <?php echo $a['str'];?>
                  </a>
                  <hr>

                  <div class="user-block">Ijazah</div>
                  <!-- /.user-block -->
                  <a href="<?php echo base_url(); ?>libs/medis/<?php echo $a['ijazah'];?>">
                  <?php echo $a['ijazah'];?>
                  </a>
                  <hr>

                  <div class="user-block">STB</div>
                  <!-- /.user-block -->
                  <a href="<?php echo base_url(); ?>libs/medis/<?php echo $a['stb'];?>">
                  <?php echo $a['stb'];?>
                  </a>
                  <hr>
                  
                  <div class="user-block">KTP</div>
                  <!-- /.user-block -->
                  <a href="<?php echo base_url(); ?>libs/medis/<?php echo $a['ktp'];?>">
                  <?php echo $a['ktp'];?>
                  </a>
                </div>
                </b>
                <!-- /.post -->

                <!-- /.post -->
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="verifikasi">
                <div class="form-group">

                  <b><h3>Verifikasi Medis</h3></b> 

                  <div class="timeline-body">
                    <br><h4>Status verifikasi</h4>
                  </div>
                  <div class="timeline-footer">

                    <?php switch ($a['status_register']) {

                      case 'belum terverifikasi':
                      echo '<h4><div class="label label-danger lb-sm" ">medis belum terverifikasi</div></h4>
                      <h5><a href="/kesehatan/index.php/admin/Adminn/do_verifikasi_medis/'.$a['id_medis'].'" onClick="return konfirmasi();"> Verifikasi medis</a></h5>
                      ';
                      break;
                      case 'terverifikasi':
                      echo '<div class="label label-success">terverifikasi</div>';
                      break;

                    } 

                    ?>
                  </div>
                  <!-- END timeline item -->
                  <!-- timeline item -->
                </div>
              </div>
              <!-- /.tab-pane -->

              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('admin/layout/footer') ?>
<?php $this->load->view('admin/layout/aside') ?>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/back/bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url ('assets/back/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url ('assets/back/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url ('assets/back/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url ('assets/back/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url ('assets/back/bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url ('assets/back/dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url ('assets/back/dist/js/demo.js') ?> "></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

</body>
</html>