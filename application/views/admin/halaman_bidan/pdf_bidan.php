<?php
//memanggil library FPDF
// intance object dan memberikan pengaturan halaman PDF
$pdf = new FPDF('l','mm','A5');
// membuat halaman baru
$pdf->AddPage();
// Setting jenis font yang akan digunakan
$pdf->SetFont('Arial','B',16);
// mencetak string
$pdf->Cell(190,7,'ARKAMAYA MEDICAL',0,1);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(190,7,'DATA BIDAN',0,1);

// memberikan space kebawah agar tidak terlalu rapat
$pdf->Cell(10,7,'',0,1);

$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,6,'ID Bidan',1,0);
$pdf->Cell(85,6,'Nama Bidan',1,0);
$pdf->Cell(27,6,'Tempat Tanggal Lahir',1,0);
$pdf->Cell(27,6,'Alamat',1,0);
$pdf->Cell(27,6,'No Telepon',1,1);

$pdf->SetFont('Arial','',10);

include 'koneksi.php';
$bidan = mysqli_query($connect, "select * from bidan");
while ($row = mysqli_feth_array($bidan)){
	$pdf->Cell(20,6,$row['id_bidan'],1,0);
	$pdf->Cell(85,6,$row['nama'],1,0);
	$pdf->Cell(27,6,$row['tempat_tanggal_lahir'],1,0);
	$pdf->Cell(27,6,$row['alamat'],1,0);
	$pdf->Cell(27,6,$row['no_hp'],1,1);
}

$pdf->Output();
?>