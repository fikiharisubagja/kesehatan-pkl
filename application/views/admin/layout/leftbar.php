<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/back/dist') ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Arkamaya Medical</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li > <a href="<?php echo base_url('index.php/admin/Adminn') ?>"><i class="fa fa-dashboard"></i> <span>Home</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Data Medis</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="<?php echo base_url('index.php/admin/Adminn/data_dokter') ?> "><i class="fa fa-circle-o text-red"></i> Data Dokter</a></li>
             <li><a href="<?php echo base_url('index.php/admin/Adminn/data_perawat') ?>"><i class="fa fa-circle-o text-yellow"></i></i> Data Perawat</a></li>
             <li><a href="<?php echo base_url('index.php/admin/Adminn/data_bidan') ?>"><i class="fa fa-circle-o text-aqua"></i> Data Bidan</a></li>   
          </ul>
        </li>

         <li>
          <a href="<?php echo base_url('index.php/admin/Adminn/data_pasien') ?>">
            <i class="fa fa-table"></i> <span>Data Pasien</span>
          
          </a>
          
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Data Barang</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="<?php echo base_url('index.php/admin/Adminn/data_obat') ?>"><i class="fa fa-circle-o text-purple"></i> Data Obat</a></li>
             <li><a href="<?php echo base_url('index.php/admin/Adminn/data_tambahan') ?>"><i class="fa fa-circle-o text-purple"></i> Data Tambah Layanan</a></li>
          </ul>
        </li>

        <li>
           <a href="<?php echo base_url('index.php/admin/Adminn/data_review') ?>"><i class="fa fa-table"></i> <span>Data Review</span></a>
        </li> 

        <li class="">
          <a href="<?php echo base_url('index.php/admin/Adminn/data_transaksi') ?> ">
            <i class="fa fa-table"></i> <span>Data Transaksi</span>
          </a>
        </li>       
        
        <li>
          <a href="<?php echo base_url('index.php/admin/Adminn/totalTransaksi') ?>">
            <i class="fa fa-fw fa-dollar"></i> <span>Laporan Transaksi</span>
          </a>
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>