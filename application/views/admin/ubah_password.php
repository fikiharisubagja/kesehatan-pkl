<!DOCTYPE html>
<html>
<?php $this->load->view('admin/layout/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('admin/layout/header') ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/layout/leftbar') ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">User profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-9">

          <!-- Profile Image -->
                  <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
  
            <div class="tab-content">
             
              
              <!-- /.tab-pane -->
                <?php echo $this->session->flashdata('success'); ?>
                 <?php echo form_open_multipart('admin/Adminn/do_edit_admin');?>
                 <div class="form-horizontal">

                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Password baru</label>

                    <div class="col-sm-10">
                      <input type="password" id="password" name="password" class="form-control" minlength="8" data-toggle="password" placeholder="Masukan password baru">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Konfirmasi Password</label>

                    <div class="col-sm-10">
                      <input type="password" id="confirm_password" name="password" class="form-control" data-toggle="password" placeholder="Masukan password baru">
                       <span id='message'></span>
                    </div>
                  </div>
    
                     
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                  </div>
                  </div>
                </form>
           
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>

<?php $this->load->view('admin/layout/leftbar') ?>

  <!-- Control Sidebar -->

<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/back/bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url ('assets/back/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url ('assets/back/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url ('assets/back/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- SlimScmroll -->
<script src="<?php echo base_url ('assets/back/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url ('assets/back/bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url ('assets/back/dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url ('assets/back/dist/js/demo.js') ?> "></script>
<!-- page script -->
<script type="text/javascript">

$('#password, #confirm_password').on('keyup', function () {
  if ($('#password').val() == $('#confirm_password').val()) {
    $('#message').html('Password sama').css('color', 'green');
  } else 
    $('#message').html('Password Tidak Sama').css('color', 'red');
});
</script>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-show-password/1.0.3/bootstrap-show-password.min.js"></script>

</body>
</html>