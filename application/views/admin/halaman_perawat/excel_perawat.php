
<html>
	<head><title>ARKAMAYA MEDICAL</title></head>
	<center>
<body>
        
        			 
  <section> 
<h3><center>LAPORAN DATA PERAWAT</center></h3>
<h3><center>APLIKASI MEDICAL</center></h3>
<p style="text-align: left;">Tanggal <?php echo date('d-m-y');?></p>
  <table border="1" style="border-collapse:collapse;">
    <thead>
      	<tr style="background:#CCC;" bordercolor="#000000">
        <th width="10%">No</th>
        <th width="10%">Id</th>
        <th width="20%">Nama</th>
        <th width="20%">Email</th>
        <th width="20%">No hp</th>
        <th width="20%">Jenis Kelamin</th>
        <th width="20%">Alamat</th>
        <th width="20%">Usia</th>
        <th width="20%">Bagian Tugas</th>      
      </tr>
    </thead>
    <tbody>
	<?php
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=Laporan Data Perawat Arkamaya Medical.xls");
		$no=+1;
		foreach($data_perawat->result_array() as $dp)
		{
	?>
      <tr>
        <td><?php echo $no; ?></td>
        <td><?php echo $dp['id_medis']; ?></td>
        <td><?php echo $dp['nama_lengkap']; ?></td>
        <td><?php echo $dp['email']; ?></td>
        <td><?php echo $dp['no_hp']; ?></td>
        <td><?php echo $dp['jenis_kelamin']; ?></td>
        <td><?php echo $dp['alamat_lengkap']; ?></td>
        <td><?php echo $dp['usia']; ?></td>
        <td><?php echo $dp['bagian_tugas']; ?></td>
      </tr>
	 <?php
	 		$no++;
	 	}
	 ?>
    </tbody>
  </table>

</section>
								
	</body>
	</center>
	</html>