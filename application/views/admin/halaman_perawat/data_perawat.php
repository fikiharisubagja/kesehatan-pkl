<!DOCTYPE html>
<html>
<?php $this->load->view('admin/layout/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('admin/layout/header') ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/layout/leftbar') ?>
    <script type="text/javascript">
      function konfirmasi() {
        return confirm('Apakah anda yakin?');
      }
    </script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Perawat
        <small>Arkamaya Medical</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            
              
              <a href="<?php echo base_url('index.php/admin/Adminn/excel_perawat')?>" class="btn btn-app">
               <i class="fa fa-inbox"></i> Export
              </a>
            <div class="box-body table-responsive">
             <?php echo $this->session->flashdata('notif');?>
             
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id Perawat</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Alamat</th>
                  <th>No Telepon</th>                  
                  <th>Status</th>
                  <th>Status register</th>
                  <th>Aksi</th>
                  
                </tr>
                </thead>
                <tbody>
                 <?php
               $a = 1;
                foreach ($data_perawat->result_array() as $dp) {
                  $no = $a++;
                ?>
                 <tr>
                    <td><?php echo $dp['id_medis'];?></td>
                    <td><?php echo $dp['nama_lengkap'];?></td>
                    <td><?php echo $dp['email'];?></td>
                    <td><?php echo $dp['alamat_lengkap'];?></td>
                    <td><?php echo $dp['no_hp'];?></td>

                    <td><?php echo $dp['status'];?></td>
                    <td><?php switch ($dp['status_register']) {

                      case 'belum terverifikasi':
                      echo '<div class="label label-danger">medis belum terverifikasi</div>';
                      break;
                      case 'terverifikasi':
                      echo '<div class="label label-success">terverifikasi</div>';
                      break;

                    } 

                    ?>
                  </td>

                  <td>

                   <button data-toggle="modal" data-target="#edit-data<?php echo $dp['id_medis'];?>" class="btn btn-success">Ubah</button>

                   <a href="<?php echo base_url()."index.php/admin/Adminn/do_delete_medis/".$dp['id_medis'];?>" onClick="return confirm('Anda yakin akan menghapus data ini???');" class="btn btn-danger btn-md">Delete 
                   </a>
                   <a href="<?php echo base_url()."index.php/admin/Adminn/detail_medis/".$dp['id_medis'];?>" class="btn btn-md">
                     Lihat Detail
                   </a>  

                      </td>
                </tr>
                <?php
                 
                    }
                ?>
                </tbody>                              
              </table>

            </div>


            <!-- /.box-body -->
<?php
               
foreach ($data_perawat->result_array() as $dp) {
?> 
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="edit-data<?php echo $dp['id_medis'];?>" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Ubah Data</h4>
              </div>
              <form class="form-horizontal" action="<?php echo base_url('index.php/admin/Adminn/ubah')?>" method="post" enctype="multipart/form-data" role="form">
                <div class="modal-body">
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Nama</label>
                            <div class="col-lg-10">
                              <input type="hidden" id="id_medis" name="id_medis" value="<?php echo $dp['id_medis']; ?>">
                                <input type="text" class="form-control" value="<?php echo $dp['nama_lengkap']; ?>" id="nama_lengkap" name="nama_lengkap" placeholder="Tuliskan Nama">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" type="submit"> Simpan&nbsp;</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"> Batal</button>
                    </div>
                  </form>
              </div>
          </div>
      </div>
  <?php } ?>

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $this->load->view('admin/layout/footer') ?>
<?php $this->load->view('admin/layout/aside') ?>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/back/bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url ('assets/back/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url ('assets/back/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url ('assets/back/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url ('assets/back/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url ('assets/back/bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url ('assets/back/dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url ('assets/back/dist/js/demo.js') ?> "></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  });

</script>
</body>
</html>
