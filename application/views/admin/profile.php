<!DOCTYPE html>
<html>
<?php $this->load->view('admin/layout/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php $this->load->view('admin/layout/header') ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->load->view('admin/layout/leftbar') ?>
 <script type="text/javascript">
      function konfirmasi() {
        return confirm('Apakah anda yakin?');
      }
    </script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">User profile</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
    <?php echo $this->session->flashdata('notif'); ?>

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <?php foreach($pf->result_array() as $a) { ?>
          <div class="box box-primary">
            <div class="box-body box-profile">
            <?php $image = $a['photo'];
            if (empty($image)) $image = "user.png";
            ?>
              <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url('libs/medis');?>/<?php echo $image?>" alt="User profile picture">

              <h3 class="profile-username text-center">
              <?php echo $a['nama_lengkap'];?>
              </h3>


            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
               <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Admin</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
             <?php echo form_open_multipart('admin/Adminn/do_edit_profile');?>

              <div class="box-body">
              <div class="form-group">
                  <input type="hidden" class="form-control" id="exampleInputEmail1" placeholder="Masukan Nama Dokter"  name="id_user" value="<?php echo $a['id_user']; ?>">
                </div>
                
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Admin</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukan Nama Admin"  name="nama_lengkap" value="<?php echo $a['nama_lengkap']; ?>">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Password</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukan Nama Admin"  name="nama_lengkap" value="<?php echo $a['password']; ?>">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Masukan Email Admin"  name="email" value="<?php echo $a['email']; ?>">
                </div>   
                <div class="form-group">
                  <label for="exampleInputEmail1">No Telepon</label>
                  <input type="number" class="form-control" id="exampleInputEmail1" placeholder="Masukan No Telepon Admin"  name="no_hp" value="<?php echo $a['no_hp']; ?>">
                </div>                         
                
                <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              
          
            </form>

             
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
          <?php } ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('admin/layout/footer') ?>
<?php $this->load->view('admin/layout/aside') ?>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/back/bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url ('assets/back/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url ('assets/back/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url ('assets/back/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url ('assets/back/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url ('assets/back/bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url ('assets/back/dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url ('assets/back/dist/js/demo.js') ?> "></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

</body>
</html>