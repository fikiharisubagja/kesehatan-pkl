<!DOCTYPE html>
<html>
<?php $this->load->view('admin/layout/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view('admin/layout/header') ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/layout/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Pemesanan
        <small>Arkamaya Medical</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
           
                <a href="<?php echo base_url('index.php/admin/Adminn/excel_laporan')?>" class="btn btn-app">
               <i class="fa fa-inbox"></i> Cetak Excel
              </a>
            
            <div class="box-body table-responsive">             
             
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id Laporan</th>
                  <th>Id Transaksi</th>
                  <th>Tanggal</th>
                  
                </tr>
                </thead>
                <tbody>
                 <?php
               
                foreach ($data_laporan->result_array() as $dp) {
                ?>
                 <tr>
                    <td><?php echo $dp['id_laporan'];?></td>
                    <td><?php echo $dp['id_transaksi'];?></td>
                    <td><?php echo $dp['tanggal'];?></td>
                    
                </tr>
                <?php
               
                    }
                ?>
                </tbody>
                              
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
<?php $this->load->view('admin/layout/footer') ?>
<?php $this->load->view('admin/layout/aside') ?>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/back/bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url ('assets/back/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url ('assets/back/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url ('assets/back/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url ('assets/back/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url ('assets/back/bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url ('assets/back/dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url ('assets/back/dist/js/demo.js') ?> "></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
